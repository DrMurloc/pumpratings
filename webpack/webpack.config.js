var path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = function(env, argv) {
  return {
    devtool: "#eval-source-map",
    entry: {
      pump: ["babel-polyfill", "./pump/main.js"]
    },
    output: {
      path: path.resolve(
        __dirname,
        "../src/PumpRatings.WebApplication/wwwroot/js"
      ),
      filename: "[name].js"
    },
    resolve: {
      extensions: [".js", ".vue", ".json"],
      alias: {
        vue$: "vue/dist/vue.esm.js",
        "@": resolve("src")
      }
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: "vue-loader",
          options: {
            loaders: {
              // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
              // the "scss" and "sass" values for the lang attribute to the right configs here.
              // other preprocessors should work out of the box, no loader config like this necessary.
              scss: "vue-style-loader!css-loader!sass-loader",
              sass: "vue-style-loader!css-loader!sass-loader?indentedSyntax"
            }
            // other vue-loader options go here
          }
        },
        {
          test: /\.js$/,
          loader: "babel-loader",
          include: [resolve("pump")]
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          loader: "file-loader",
          options: {
            name: "[name].[ext]?[hash]"
          }
        },
        {
          test: /\.css$/,
          use: ["css-loader"]
        }
      ]
    },
    performance: {
      hints: false
    }
  };
};
