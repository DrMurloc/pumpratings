import datalayer from "./datalayer";
export default {
  namespaced: true,
  modules: {
    datalayer
  },
  state: {
    releaseNotes: undefined
  },
  getters: {
    getReleaseNotes: state => state.releaseNotes
  },
  mutations: {
    setReleaseNotes(state, releaseNotes) {
      state.releaseNotes = releaseNotes;
    },
    addReleaseNotes(state, releaseNotes) {
      state.releaseNotes.splice(0, 0, releaseNotes);
      while (state.releaseNotes.length > 5) {
        state.releaseNotes.pop();
      }
    }
  },
  actions: {
    addReleaseNotes({ commit, dispatch }, message) {
      return new Promise((resolve, reject) => {
        dispatch("datalayer/addReleaseNotes", message).then(result => {
          commit("addReleaseNotes", {
            user: {
              id: "",
              userName: ""
            },
            notes: message,
            releaseDate: new Date()
          });
          resolve({});
        });
      });
    }
  }
};
