export default {
  namespaced: true,
  state: {
    pageName: ""
  },
  getters: {
    getPageName: state => state.pageName
  },
  mutations: {
    setPageName(state, pageName) {
      state.pageName = pageName;
    }
  }
};
