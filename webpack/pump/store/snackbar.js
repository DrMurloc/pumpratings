export default {
  namespaced: true,
  state: {
    showSnackbar: false,
    snackbarMessage: ""
  },
  getters: {
    getShowSnackbar: state => state.showSnackbar,
    getSnackbarMessage: state => state.snackbarMessage
  },
  mutations: {
    setShowSnackbar(state, showSnackbar) {
      state.showSnackbar = showSnackbar;
    },
    setSnackbarMessage(state, message) {
      state.snackbarMessage = message;
    }
  },
  actions: {
    toast({ commit }, message) {
      commit("setSnackbarMessage", message);
      commit("setShowSnackbar", true);
    }
  }
};
