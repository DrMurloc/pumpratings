import Vue from "vue";
import Vuex from "vuex";
import Enumerable from "linq";
import datalayer from "./datalayer";
import menu from "./menu";
import profile from "./profile";
import snackbar from "./snackbar";
import releaseNotes from "./releaseNotes";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    datalayer,
    menu,
    profile,
    snackbar,
    releaseNotes
  },
  state: {
    songs: undefined,
    mixes: undefined,
    charts: undefined,
    currentMix: undefined,
    currentSong: undefined,
    currentChart: undefined,
    tags: undefined,
    lists: undefined,
    loginProviders: undefined
  },
  getters: {
    getCurrentSong: state => state.currentSong,
    getCurrentChart: state => state.currentChart,
    getCurrentMix: state => state.currentMix,
    getMixes: state => state.mixes,
    getCharts: state => state.charts,
    getSongs: state => state.songs,
    getTags: state => state.tags,
    getLists: state => state.lists,
    getFavoritesList: state =>
      Enumerable.from(state.lists).first(list => list.isFavorites),
    getLoginProviders: state => state.loginProviders
  },
  mutations: {
    setSongs(state, songs) {
      state.songs = songs;
    },
    setMixes(state, mixes) {
      state.mixes = mixes;
    },
    setCharts(state, charts) {
      state.charts = charts;
    },
    setTags(state, tags) {
      state.tags = tags;
    },
    setLists(state, lists) {
      state.lists = lists;
    },
    setCurrentChart(state, chart) {
      state.currentChart = chart;
    },
    setCurrentSong(state, song) {
      state.currentSong = song;
    },
    setCurrentMix(state, mix) {
      state.currentMix = mix;
    },
    addTag(state, newTag) {
      state.tags.push(newTag);
    },
    addChart(state, newChart) {
      var song = Enumerable.from(state.songs).first(
        song => song.id === newChart.songId
      );
      song.charts.push(newChart);
      newChart.song = song;
      state.charts.push(newChart);
    },
    addSong(state, newSong) {
      state.songs.push(newSong);
      var mix = Enumerable.from(state.mixes).first(
        mix => mix.id === newSong.mixId
      );
      newSong.mix = mix;
      mix.songs.push(newSong);
    },
    addMix(state, newMix) {
      state.mixes.push(newMix);
    },
    deleteComment(state, { chartId, userId }) {
      var chart = Enumerable.from(state.charts).first(
        chart => chart.id === chartId
      );
      for (var i = 0; i < chart.comments.length; i++) {
        if (chart.comments[i].user.id === userId) {
          chart.comments.splice(i, 1);
          break;
        }
      }
    },
    addComment(state, { chartId, comment }) {
      Enumerable.from(state.charts)
        .first(chart => chart.id === chartId)
        .comments.splice(0, 0, comment);
    },
    setRatings(state, { chartId, ratings }) {
      Enumerable.from(state.charts).first(
        chart => chart.id === chartId
      ).ratings = ratings;
    },
    setLoginProviders(state, loginProviders) {
      state.loginProviders = loginProviders;
    }
  },
  actions: {
    loadPageData({ commit, getters, dispatch }) {
      return new Promise((resolve, reject) => {
        console.log("Loading page data...");
        dispatch("datalayer/getPageData")
          .then(result => {
            commit("profile/setProfile", result.me);
            commit("setTags", result.tags);
            commit("setCharts", result.charts);
            for (var i = 0; i < result.songs.length; i++) {
              result.songs[i].charts = Enumerable.from(getters.getCharts)
                .where(chart => chart.songId === result.songs[i].id)
                .toArray();
              for (var j = 0; j < result.songs[i].charts.length; j++) {
                result.songs[i].charts[j].song = result.songs[i];
              }
            }
            commit("setSongs", result.songs);
            for (var i = 0; i < result.mixes.length; i++) {
              result.mixes[i].songs = Enumerable.from(getters.getSongs)
                .where(song => song.mixId === result.mixes[i].id)
                .toArray();
              for (var j = 0; j < result.mixes[i].songs.length; j++) {
                result.mixes[i].songs[j].mix = result.mixes[i];
              }
            }
            commit("setMixes", result.mixes);
            commit("setLists", result.me.lists);
            commit("releaseNotes/setReleaseNotes", result.releaseNotes);
            commit("setLoginProviders", result.loginProviders);
            console.log("Loaded!");
            resolve({});
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    setCurrentMix({ getters, commit }, id) {
      return new Promise((resolve, reject) => {
        var mix = Enumerable.from(getters.getMixes).first(mix => mix.id === id);
        commit("setCurrentMix", mix);
        commit("setCurrentSong", undefined);
        commit("setCurrentChart", undefined);
        resolve({ mix });
      });
    },
    setCurrentSong({ getters, commit }, id) {
      return new Promise((resolve, reject) => {
        var song = Enumerable.from(getters.getSongs).first(
          song => song.id === id
        );
        commit("setCurrentSong", song);
        commit("setCurrentMix", song.mix);
        commit("setCurrentChart", undefined);
        resolve({ song });
      });
    },
    setCurrentChart({ getters, commit, dispatch }, id) {
      return new Promise((resolve, reject) => {
        var chart = Enumerable.from(getters.getCharts).first(
          chart => chart.id === id
        );
        commit("setCurrentSong", chart.song);
        commit("setCurrentMix", chart.song.mix);
        if (chart.comments === undefined) {
          dispatch("datalayer/getChartComments", id)
            .then(result => {
              chart.comments = result.chart.comments;
              return dispatch("datalayer/getChartRatings", id);
            })
            .then(result => {
              chart.ratings = result.chart.myRating;
              if (chart.ratings === null) {
                chart.ratings = {
                  rating: null
                };
              }
              commit("setCurrentChart", chart);
              resolve({ chart });
            });
        } else {
          commit("setCurrentChart", chart);
          resolve({ chart });
        }
      });
    },
    addMix({ commit, dispatch }, newMix) {
      return new Promise((resolve, reject) => {
        dispatch("datalayer/addMix", newMix).then(result => {
          newMix.id = result.mixId;
          newMix.songs = [];
          commit("addMix", newMix);
          resolve({});
        });
      });
    },
    addSong({ commit, dispatch, getters }, newSong) {
      return new Promise((resolve, reject) => {
        dispatch("datalayer/addSong", newSong).then(result => {
          var mix = Enumerable.from(getters.getMixes).first(
            mix => mix.id === newSong.mixId
          );
          newSong.id = result.songId;
          newSong.charts = [];
          commit("addSong", newSong);
          resolve({ songId: result.songId });
        });
      });
    },
    addChart({ commit, getters, dispatch }, { songId, newChart }) {
      return new Promise((resolve, reject) => {
        if (newChart.type !== "CoOp") {
          newChart.players = 1;
        }
        dispatch("datalayer/addChart", { songId, newChart }).then(result => {
          newChart.id = result.chartId;
          newChart.songId = songId;
          newChart.averageRating = null;
          newChart.comments = [];
          newChart.tagNames = [];
          newChart.ratings = {
            rating: null
          };
          commit("addChart", newChart);
          resolve({});
        });
      });
    },
    updateChart({ commit, getters, dispatch }, { chartId, chart }) {
      return new Promise((resolve, reject) => {
        dispatch("datalayer/updateChart", { chartId, chart }).then(result => {
          Enumerable.from(getters.getCharts).first(
            curChart => curChart.id === chartId
          ).video = chart.video;
          resolve({});
        });
      });
    },
    updateScore({ dispatch, commit, getters }, { chartId, score }) {
      return new Promise((resolve, reject) => {
        dispatch("datalayer/updateScore", {
          chartId,
          score
        }).then(result => {
          getters.getCurrentChart.averageScore = result.average;
          resolve({});
        });
      });
    },
    updateChartRatings({ dispatch, commit, getters }, { chartId, ratings }) {
      return new Promise((resolve, reject) => {
        dispatch("datalayer/updateChartRatings", {
          chartId,
          ratings
        }).then(result => {
          console.log(result);
          Enumerable.from(getters.getCharts).first(
            chart => chart.id === chartId
          ).averageRating = result.newAverageRating;
          resolve({});
        });
      });
    },
    deleteComment({ commit, dispatch, getters }, { chartId }) {
      return new Promise((resolve, reject) => {
        var userId = getters["profile/getProfile"].id;
        dispatch("datalayer/deleteComment", { chartId }).then(result => {
          commit("deleteComment", { chartId, userId });
          resolve({});
        });
      });
    },
    addComment({ commit, dispatch, getters }, { chartId, message }) {
      return new Promise((resolve, reject) => {
        var userId = getters["profile/getProfile"].id;
        var userName = getters["profile/getProfile"].name;
        dispatch("datalayer/addComment", { chartId, message }).then(result => {
          var comment = {
            user: {
              id: userId,
              name: userName
            },
            message
          };
          commit("addComment", { chartId, comment });
          resolve({});
        });
      });
    },
    createTag({ commit, dispatch }, newTag) {
      return new Promise((resolve, reject) => {
        dispatch("datalayer/createTag", newTag).then(result => {
          commit("addTag", newTag);
          resolve({});
        });
      });
    },
    removeTag({ commit, dispatch, getters }, { chartId, tag }) {
      return new Promise((resolve, reject) => {
        var chart = Enumerable.from(getters.getCharts).first(
          chart => chart.id === chartId
        );
        var newTags = Enumerable.from(chart.tagNames)
          .where(chartTag => chartTag !== tag)
          .toArray();
        dispatch("datalayer/updateChartTags", {
          chartId,
          tagNames: newTags
        }).then(result => {
          chart.tagNames = newTags;
          resolve({});
        });
      });
    },
    addTag({ commit, dispatch, getters }, { chartId, tag }) {
      return new Promise((resolve, reject) => {
        var chart = Enumerable.from(getters.getCharts).first(
          chart => chart.id === chartId
        );
        var newTags = Enumerable.from(chart.tagNames).toArray();
        newTags.push(tag);
        dispatch("datalayer/updateChartTags", {
          chartId,
          tagNames: newTags
        }).then(result => {
          chart.tagNames = newTags;
          resolve({});
        });
      });
    },
    addChartToList({ commit, dispatch, getters }, { chartId, listId }) {
      return new Promise((resolve, reject) => {
        dispatch("datalayer/addChartToList", { chartId, listId })
          .then(result => {
            Enumerable.from(getters.getLists)
              .first(list => list.id === listId)
              .chartIds.push(chartId);
            resolve({});
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    removeChartFromList({ commit, dispatch, getters }, { chartId, listId }) {
      return new Promise((resolve, reject) => {
        dispatch("datalayer/removeChartFromList", { chartId, listId })
          .then(result => {
            var list = Enumerable.from(getters.getLists).first(
              list => list.id === listId
            );
            list.chartIds.splice(list.chartIds.indexOf(chartId), 1);
            resolve({});
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
});
