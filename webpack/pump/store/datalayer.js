import deepClone from "deep-clone";
import Enumerable from "linq";
import axios from "axios";
export default {
  namespaced: true,
  state: {},
  actions: {
    command({ state }, { name, body }) {
      return new Promise((resolve, reject) => {
        axios
          .post("/command/" + name, body)
          .then(response => {
            resolve(response.data);
          })
          .catch(error => {
            console.error(error);
            reject(error);
          });
      });
    },
    query({ state }, { name, query, variables }) {
      return new Promise((resolve, reject) => {
        axios
          .post("/query", {
            name,
            query,
            variables
          })
          .then(result => {
            resolve(result.data.data);
          })
          .catch(error => {
            console.error(error);
            reject(error);
          });
      });
    },
    clearCache({ dispatch }) {
      return dispatch("command", { name: "ClearCache", body: {} });
    },
    addReleaseNotes({ dispatch }, notes) {
      return dispatch("command", {
        name: "CreateReleaseNotes",
        body: {
          notes
        }
      });
    },
    promoteUserToAdmin({ dispatch }, id) {
      return dispatch("command", {
        name: "UpdateUserRole",
        body: {
          userId: id,
          role: "Admin"
        }
      });
    },
    promoteUserToSuperAdmin({ dispatch }, id) {
      return dispatch("command", {
        name: "UpdateUserRole",
        body: {
          userId: id,
          role: "Super Kami Guru"
        }
      });
    },
    updateProfile({ dispatch }, profile) {
      return dispatch("command", {
        name: "UpdateProfile",
        body: {
          name: profile.name,
          currentRole: profile.currentRoleName,
          darkTheme: profile.darkTheme
        }
      });
    },
    addMix({ dispatch }, newMix) {
      return dispatch("command", {
        name: "CreateMix",
        body: {
          name: newMix.name,
          image: newMix.image,
          displayOrder: newMix.displayOrder
        }
      });
    },
    addChart({ dispatch }, { songId, newChart }) {
      return dispatch("command", {
        name: "CreateChart",
        body: {
          songId: songId,
          difficulty: newChart.difficulty,
          type: newChart.typeName,
          video: newChart.video,
          players: newChart.players
        }
      });
    },
    addSong({ dispatch }, newSong) {
      return dispatch("command", {
        name: "CreateSong",
        body: {
          name: newSong.name,
          artist: newSong.artist,
          bpm: newSong.bpm,
          image: newSong.image,
          mixId: newSong.mixId,
          type: newSong.typeName
        }
      });
    },
    updateChart({ dispatch }, { chartId, chart }) {
      return dispatch("command", {
        name: "UpdateChart",
        body: {
          chartId: chartId,
          video: chart.video
        }
      });
    },
    updateChartRatings({ dispatch }, { chartId, ratings }) {
      return dispatch("command", {
        name: "UpdateRating",
        body: {
          chartId,
          rating: ratings.rating
        }
      });
    },
    deleteComment({ dispatch }, { chartId }) {
      return dispatch("command", {
        name: "DeleteComment",
        body: {
          chartId
        }
      });
    },
    addComment({ dispatch }, { chartId, message }) {
      return dispatch("command", {
        name: "CreateComment",
        body: {
          chartId,
          message
        }
      });
    },
    createTag({ dispatch }, tag) {
      return dispatch("command", {
        name: "CreateTag",
        body: {
          name: tag.name,
          color: tag.color
        }
      });
    },
    updateChartTags({ dispatch }, { chartId, tagNames }) {
      return dispatch("command", {
        name: "UpdateChartTags",
        body: {
          chartId,
          tagNames
        }
      });
    },
    addChartToList({ dispatch }, { chartId, listId }) {
      return dispatch("command", {
        name: "AddChartToList",
        body: {
          chartId,
          listId
        }
      });
    },
    removeChartFromList({ dispatch }, { chartId, listId }) {
      return dispatch("command", {
        name: "RemoveChartFromList",
        body: {
          chartId,
          listId
        }
      });
    },
    getPageData({ dispatch }) {
      return dispatch("query", {
        name: "Get Page Data",
        query: `
{
  mixes {
    id
    name
    displayOrder
    image
  }
  songs {
    mixId
    id
    name
    artist
    bpm
    typeName
    image
  }
  charts {
    id
    songId
    difficulty
    averageRating
    players
    typeName
    video
    tagNames
  }
  tags {
    name
    color
  }
  
  me {
    id
    name
    roleName
    darkTheme
    currentRoleName
    lists {
      id
      name
      chartIds
      isFavorites
    }
  }
  releaseNotes(count:5) {
    id
    user {
      name
    }
    notes
    releaseDate
  }
  loginProviders {
    displayName
    scheme
    icon
  }
}`
      });
    },
    getTracking({ dispatch }, count) {
      return dispatch("query", {
        name: "Get Tracking",
        query: `
query($count:Int){
  tracking(count:$count) {
    id
    user {
      id
      name
    }
    summary
    change
    changeDate
  }
}`,
        variables: {
          count
        }
      });
    },
    getProfileById({ dispatch }, id) {
      return dispatch("query", {
        name: "Get User By Id",
        query: `
query($userId:String!) {
  user(id:$userId) {
    id
    name
    roleName
  }
}`,
        variables: {
          userId: id
        }
      });
    },
    getChartRatings({ dispatch }, chartId) {
      return dispatch("query", {
        name: "Get Chart Ratings",
        query: `
query($chartId:String!) {
  chart(id:$chartId) {
    myRating {
      rating
    }
  }
}`,
        variables: {
          chartId
        }
      });
    },
    getChartComments({ dispatch }, chartId) {
      return dispatch("query", {
        name: "Get Chart comments",
        query: `
query($chartId:String!) {
  chart(id:$chartId) {
    comments {
      user {
        id
        name
      }
      message
      createdDate
    }
  }
}`,
        variables: {
          chartId
        }
      });
    }
  }
};
