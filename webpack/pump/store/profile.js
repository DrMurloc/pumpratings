import datalayer from "./datalayer";
import deepClone from "deep-clone";
export default {
  namespaced: true,
  modules: {
    datalayer
  },
  state: {
    profile: undefined,
    editProfile: {}
  },
  mutations: {
    setProfile(state, profile) {
      state.profile = profile;
    },
    resetEditProfile(state) {
      Object.assign(state.editProfile, state.profile);
    }
  },
  getters: {
    getProfile: state => state.profile,
    getEditProfile: state => state.editProfile,
    getIsSuperKamiGuru: state =>
      state.profile.currentRoleName === "Super Kami Guru",
    getIsAdmin: state =>
      state.profile.currentRoleName === "Admin" ||
      state.profile.currentRoleName === "Super Kami Guru",
    getIsUser: state =>
      state.profile.currentRoleName === "User" ||
      state.profile.currentRoleName === "Admin" ||
      state.profile.currentRoleName === "Super Kami Guru"
  },
  actions: {
    updateProfile({ dispatch, commit, getters }) {
      return new Promise((resolve, reject) => {
        var newProfile = deepClone(getters.getProfile);
        newProfile.name = getters.getEditProfile.name;
        newProfile.darkTheme = getters.getEditProfile.darkTheme;
        newProfile.currentRoleName = getters.getEditProfile.currentRoleName;
        dispatch("datalayer/updateProfile", newProfile).then(result => {
          commit("setProfile", newProfile);
          resolve({});
        });
      });
    }
  }
};
