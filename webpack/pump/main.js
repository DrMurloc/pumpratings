import Vue from "vue";
import Vuetify from "vuetify";
import App from "./App";
import { sync } from "vuex-router-sync";

import router from "./router";
import store from "./store";
sync(store, router);
Vue.config.productionTip = false;
Vue.use(Vuetify);
/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  template: "<App/>",
  components: { App }
});
