import Vue from "vue";
import Router from "vue-router";
import store from "../store";
import Enumerable from "linq";
import Mix from "../components/pages/Mix";
import Song from "../components/pages/Song";
import Chart from "../components/pages/Chart";
import Profile from "../components/pages/Profile";
import MixList from "../components/pages/MixList";
import SongList from "../components/pages/SongList";
import ChartSearch from "../components/pages/ChartSearch";
import SuperAdmin from "../components/pages/SuperAdmin";
import Admin from "../components/pages/Admin";
import Home from "../components/pages/Home";
import ListsOverview from "../components/pages/ListsOverview";
import ChartList from "../components/pages/ChartList";
Vue.use(Router);
Vue.use(store);
var router = new Router({
  routes: [
    {
      path: "/",
      name: "default",
      component: Home,
      beforeEnter: (to, from, next) => {
        store.commit("menu/setPageName", "Home");
        next();
      }
    },
    {
      path: "/mixes",
      name: "mixesList",
      component: MixList,
      beforeEnter: (to, from, next) => {
        store.commit("menu/setPageName", "Mixes");
        next();
      }
    },
    {
      path: "/songs",
      name: "songsList",
      component: SongList,
      beforeEnter: (to, from, next) => {
        store.commit("menu/setPageName", "Songs");
        next();
      }
    },
    {
      path: "/charts",
      name: "chartSearch",
      component: ChartSearch,
      beforeEnter: (to, from, next) => {
        store.commit("menu/setPageName", "Charts");
        next();
      }
    },
    {
      path: "/mixes/:mixId",
      name: "mixSongs",
      component: Mix,
      beforeEnter: (to, from, next) => {
        store
          .dispatch("setCurrentMix", to.params.mixId)
          .then(result => {
            store.commit("menu/setPageName", result.mix.name);
            next();
          })
          .catch(error => {
            console.error(error);
            alert("There was an error changing mix");
          });
      }
    },
    {
      path: "/songs/:songId",
      name: "viewSong",
      component: Song,
      beforeEnter: (to, from, next) => {
        store
          .dispatch("setCurrentSong", to.params.songId)
          .then(result => {
            store.commit("menu/setPageName", result.song.name);
            next();
          })
          .catch(error => {
            console.error(error);
            alert("There was an error changing songs");
          });
      }
    },
    {
      path: "/charts/:chartId",
      name: "viewChart",
      component: Chart,
      beforeEnter: (to, from, next) => {
        store
          .dispatch("setCurrentChart", to.params.chartId)
          .then(result => {
            store.commit(
              "menu/setPageName",
              result.chart.song.name + " (" + result.chart.difficulty + ")"
            );
            next();
          })
          .catch(error => {
            console.error(error);
            alert("There was an error selecting the chart");
          });
      }
    },
    {
      path: "/lists",
      name: "listsOverview",
      component: ListsOverview,
      beforeEnter: (to, from, next) => {
        store.commit("menu/setPageName", "Lists");
        next();
      }
    },
    {
      path: "/lists/:listId",
      name: "chartList",
      component: ChartList,
      beforeEnter: (to, from, next) => {
        var list = Enumerable.from(store.getters.getLists).firstOrDefault(
          list => list.id === to.params.listId,
          undefined
        );
        if (list != undefined) {
          store.commit("menu/setPageName", list.name);
          next();
        } else {
          alert("There was an error navigating to the list");
        }
      }
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile,
      beforeEnter: (to, from, next) => {
        store.commit("menu/setPageName", "Profile");
        store.commit("profile/resetEditProfile");
        next();
      }
    },
    {
      path: "/admin",
      name: "admin",
      component: Admin,
      beforeEnter: (to, from, next) => {
        if (store.getters["profile/getIsAdmin"]) {
          next();
        }
      }
    },
    {
      path: "/admin/super",
      name: "superAdmin",
      component: SuperAdmin,
      beforeEnter: (to, from, next) => {
        if (store.getters["profile/getIsSuperKamiGuru"]) {
          next();
        }
      }
    }
  ]
});
router.beforeEach((to, from, next) => {
  if (store.getters["profile/getProfile"] === undefined) {
    store
      .dispatch("loadPageData")
      .then(result => {
        next();
      })
      .catch(error => {
        console.error(error);
        alert("There was an error while loading page data");
      });
  } else {
    next();
  }
});
export default router;
