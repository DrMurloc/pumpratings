﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace PumpRatings.Command.Abstractions.Exceptions
{
  public class BadCommandArgumentsException : Exception
  {
    public BadCommandArgumentsException(string argumentName)
      : base($"Command argument {argumentName} invalid")
    {

    }

    public BadCommandArgumentsException(string argumentName, string commandName)
      : base($"Command argument {argumentName} invalid for command {commandName}")
    {

    }

    public BadCommandArgumentsException(string argumentName, MemberInfo commandType)
      : this(argumentName,commandType.Name)
    {
      
    }
  }
  public sealed class BadCommandArgumentsException<TCommand> : BadCommandArgumentsException
  {
    public BadCommandArgumentsException(string argumentName) : base(argumentName,typeof(TCommand))
    {

    }
  }
}
