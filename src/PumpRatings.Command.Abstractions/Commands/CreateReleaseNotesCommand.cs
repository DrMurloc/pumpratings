﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class CreateReleaseNotesCommand
  {
    public string Notes { get; set; }
  }
}
