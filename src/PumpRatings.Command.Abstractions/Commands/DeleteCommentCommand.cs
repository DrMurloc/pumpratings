﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class DeleteCommentCommand
  {
    public string ChartId { get; set; }
  }
}
