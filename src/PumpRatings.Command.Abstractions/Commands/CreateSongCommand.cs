﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class CreateSongCommand
  {
    public string Name { get; set; }
    public string Artist { get; set; }
    public string Bpm { get; set; }
    public string Image { get; set; }
    public string MixId { get; set; }
    public string Type { get; set; }
  }
}
