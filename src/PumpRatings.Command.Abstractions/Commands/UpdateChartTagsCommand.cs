﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class UpdateChartTagsCommand
  {
    public string ChartId { get; set; }
    public IEnumerable<string> TagNames { get; set; }
  }
}
