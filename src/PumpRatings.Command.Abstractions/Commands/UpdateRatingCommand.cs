﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class UpdateRatingCommand
  {
    public string ChartId { get; set; }
    public double Rating { get; set; }
  }
}
