﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class RemoveChartFromListCommand
  {
    public string ChartId { get; set; }
    public string ListId { get; set; }
  }
}
