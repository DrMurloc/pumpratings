﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class UpdateUserRoleCommand
  {
    public string UserId { get; set; }
    public string Role { get; set; }
  }
}
