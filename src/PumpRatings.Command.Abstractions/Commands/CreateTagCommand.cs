﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class CreateTagCommand
  {
    public string Name { get; set; }
    public string Color { get; set; }
  }
}
