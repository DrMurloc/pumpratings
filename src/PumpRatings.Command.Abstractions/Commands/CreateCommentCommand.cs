﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class CreateCommentCommand
  {
    public string ChartId { get; set; }
    public string Message { get; set; }
  }
}
