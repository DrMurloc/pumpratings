﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class UpdateProfileCommand
  {
    public string Name { get; set; }
    public string CurrentRole { get; set; }
    public bool DarkTheme { get; set; }
  }
}
