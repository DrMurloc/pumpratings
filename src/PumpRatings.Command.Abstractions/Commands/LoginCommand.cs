﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace PumpRatings.Command.Abstractions.Commands
{
  public sealed class LoginCommand
  {
    public string Scheme { get; set; }
    public IEnumerable<Claim> ExternalClaims { get; set; }
  }
}
