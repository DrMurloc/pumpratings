﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PumpRatings.Command.Abstractions
{
  public interface ICommandHandler<in TCommand>
  {
    Task Handle(TCommand command);
  }

  public interface ICommandHandler<in TCommand, TResult>
  {
    Task<TResult> Handle(TCommand command);
  }
}
