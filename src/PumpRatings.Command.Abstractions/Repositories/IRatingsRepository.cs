﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Command.Abstractions.Models;

namespace PumpRatings.Command.Abstractions.Repositories
{
  public interface IRatingsRepository
  {
    Task MergeRating(MergeRatingModel mergeRatingModel);
    Task<double> GetAverageRating(string chartId);
  }
}
