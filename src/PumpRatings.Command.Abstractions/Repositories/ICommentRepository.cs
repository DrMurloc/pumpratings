﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Command.Abstractions.Models;

namespace PumpRatings.Command.Abstractions.Repositories
{
  public interface ICommentRepository
  {
    Task CreateComment(CreateCommentModel record);
    Task DeleteComment(string chartId, string userId);
  }
}
