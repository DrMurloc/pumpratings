﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Command.Abstractions.Models;

namespace PumpRatings.Command.Abstractions.Repositories
{
  public interface IListChartRepository
  {
    Task CreateListChart(string listId, string chartId);
    Task RemoveListChart(string listId, string chartId);
  }
}
