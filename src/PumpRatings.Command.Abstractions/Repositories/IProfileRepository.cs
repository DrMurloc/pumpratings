﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Command.Abstractions.Models;

namespace PumpRatings.Command.Abstractions.Repositories
{
  public interface IProfileRepository
  {
    Task CreateProfile(CreateProfileModel model);
    Task UpdateProfile(UpdateProfileModel model);
    Task UpdateRole(string userId, string role);
    Task<GetProfileModel> GetProfileByExternalId(string loginProvider, string externalId);
    Task<GetProfileModel> GetProfileByUserId(string userId);
  }
}
