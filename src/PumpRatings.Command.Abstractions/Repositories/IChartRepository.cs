﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Command.Abstractions.Models;

namespace PumpRatings.Command.Abstractions.Repositories
{
  public interface IChartRepository
  {
    Task CreateChart(CreateChartModel record);
    Task UpdateChart(UpdateChartModel model);
    Task UpdateCharTags(string chartId, IEnumerable<string> tagNames);
  }
}
