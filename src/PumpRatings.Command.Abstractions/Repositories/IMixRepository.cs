﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Command.Abstractions.Models;

namespace PumpRatings.Command.Abstractions.Repositories
{
  public interface IMixRepository
  {
    Task CreateMix(CreateMixModel mixesRecord);
  }
}
