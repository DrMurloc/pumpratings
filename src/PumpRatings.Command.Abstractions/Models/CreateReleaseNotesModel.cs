﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class CreateReleaseNotesModel
  {
    public string UserId { get; set; }
    public DateTimeOffset ReleaseDate { get; set; }
    public string Notes { get; set; }
  }
}
