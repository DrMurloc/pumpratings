﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class CreateTagModel
  {
    public string Name { get; set; }
    public string Color { get; set; }
  }
}
