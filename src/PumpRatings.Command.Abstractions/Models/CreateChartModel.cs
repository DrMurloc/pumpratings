﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class CreateChartModel
  {
    public string Id { get; set; }
    public int? Difficulty { get; set; }
    public string Type { get; set; }
    public string SongId { get; set; }
    public string Video { get; set; }
    public int Players { get; set; }
  }
}
