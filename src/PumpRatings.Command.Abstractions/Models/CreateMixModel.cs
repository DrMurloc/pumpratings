﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class CreateMixModel
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public string Image { get; set; }
    public int DisplayOrder { get; set; }
  }
}
