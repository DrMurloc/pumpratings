﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class CreateProfileModel
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public string Role { get; set; }
    public string ExternalId { get; set; }
    public string LoginProvider { get; set; }
    public bool DarkTheme { get; set; }
    public string CurrentRole { get; set; }
  }
}
