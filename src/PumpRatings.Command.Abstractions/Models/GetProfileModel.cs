﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class GetProfileModel
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public string Role { get; set; }
    public bool DarkTheme { get; set; }
    public string CurrentRole { get; set; }
  }
}
