﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class UpdateChartModel
  {
    public string ChartId { get; set; }
    public string Video { get; set; }
  }
}
