﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class CreateListModel
  {
    public string UserId { get; set; }
    public string Id { get; set; }
    public bool IsFavorites { get; set; }
    public string Name { get; set; }
  }
}
