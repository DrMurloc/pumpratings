﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class UpdateProfileModel
  {
    public string UserId { get; set; }
    public string CurrentRole { get; set; }
    public string Name { get; set; }
    public bool DarkTheme { get; set; }
  }
}
