﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class MergeRatingModel
  {
    public string ChartId { get; set; }
    public string UserId { get; set; }
    public double Rating { get; set; }
  }
}
