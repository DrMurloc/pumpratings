﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class GetListModel
  {
    public string UserId { get; set; }
    public string Id { get; set; }
  }
}
