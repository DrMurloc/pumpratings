﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Command.Abstractions.Models
{
  public sealed class CreateCommentModel
  {
    public string UserId { get; set; }
    public string ChartId { get; set; }
    public DateTimeOffset CreatedDate { get; set; }
    public string Message { get; set; }
  }
}
