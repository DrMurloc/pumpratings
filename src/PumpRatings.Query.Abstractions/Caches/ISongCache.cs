﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface ISongCache
  {
    Task<IEnumerable<SongNode>> GetAllSongs(Func<Task<IEnumerable<SongNode>>> retrieval);
    Task<IEnumerable<SongNode>> GetSongsByMixId(string mixId, Func<Task<IEnumerable<SongNode>>> retrieval);
    Task<IEnumerable<SongNode>> GetSongsByTypeName(string typeName, Func<Task<IEnumerable<SongNode>>> retrieval);
    Task<SongNode> GetSongById(string songId, Func<Task<IEnumerable<SongNode>>> retrieval);
  }
}
