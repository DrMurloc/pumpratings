﻿using PumpRatings.Query.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface IReleaseNotesCache
  {
    Task<IEnumerable<ReleaseNotesNode>> GetOrSetReleaseNotes(Func<Task<IEnumerable<ReleaseNotesNode>>> retrieval);
  }
}
