﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface IChartTypeCache
  {
    Task<IEnumerable<ChartTypeNode>> GetAllChartTypes(Func<Task<IEnumerable<ChartTypeNode>>> retrieval);
    Task<ChartTypeNode> GetChartTypeByName(string typeName, Func<Task<IEnumerable<ChartTypeNode>>> retrieval);
  }
}
