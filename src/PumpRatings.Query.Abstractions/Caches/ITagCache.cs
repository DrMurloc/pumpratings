﻿using PumpRatings.Query.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface ITagCache
  {
    Task<IEnumerable<TagNode>> GetAllTags(Func<Task<IEnumerable<TagNode>>> retrieval);
    Task<TagNode> GetTagByName(string tagName, Func<Task<IEnumerable<TagNode>>> retrieval);
  }
}
