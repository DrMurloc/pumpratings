﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface IUserCache
  {
    Task<IEnumerable<UserNode>> GetAllUsers(Func<Task<IEnumerable<UserNode>>> retrieval);
    Task<UserNode> GetUserById(string userId, Func<Task<IEnumerable<UserNode>>> retrieval);
    Task<IEnumerable<UserNode>> GetUsersByRole(string role, Func<Task<IEnumerable<UserNode>>> retrieval);
  }
}
