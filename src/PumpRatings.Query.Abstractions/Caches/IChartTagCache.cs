﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface IChartTagCache
  {
    Task<IEnumerable<ChartTagEdge>> GetChartTagsByChartId(string chartId, Func<Task<IEnumerable<ChartTagEdge>>> retrieval);
    Task<IEnumerable<ChartTagEdge>> GetChartTagsByTagName(string tagName, Func<Task<IEnumerable<ChartTagEdge>>> retrieval);
  }
}
