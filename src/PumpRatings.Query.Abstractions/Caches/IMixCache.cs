﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface IMixCache
  {
    Task<IEnumerable<MixNode>> GetAllMixes(Func<Task<IEnumerable<MixNode>>> retrieval);
    Task<MixNode> GetMixById(string mixId, Func<Task<IEnumerable<MixNode>>> retrieval);
  }
}
