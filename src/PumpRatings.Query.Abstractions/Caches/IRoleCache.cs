﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface IRoleCache
  {
    Task<IEnumerable<RoleNode>> GetAllRoles(Func<Task<IEnumerable<RoleNode>>> retrieval);
    Task<RoleNode> GetRoleByName(string roleName, Func<Task<IEnumerable<RoleNode>>> retrieval);
  }
}
