﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface ISongTypeCache
  {
    Task<IEnumerable<SongTypeNode>> GetAllSongTypes(Func<Task<IEnumerable<SongTypeNode>>> retrieval);
    Task<SongTypeNode> GetSongTypeByName(string songTypeName, Func<Task<IEnumerable<SongTypeNode>>> retrieval);
  }
}
