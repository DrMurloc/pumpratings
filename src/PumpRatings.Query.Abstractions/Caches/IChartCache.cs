﻿using PumpRatings.Query.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PumpRatings.Query.Abstractions.Caches
{
  public interface IChartCache
  {
    Task<IEnumerable<ChartNode>> GetAllCharts(Func<Task<IEnumerable<ChartNode>>> retrieval);
    Task<ChartNode> GetChartById(string chartId, Func<Task<IEnumerable<ChartNode>>> retrieval);
    Task<IEnumerable<ChartNode>> GetChartsBySongId(string songId, Func<Task<IEnumerable<ChartNode>>> retrieval);
    Task<IEnumerable<ChartNode>> GetChartsByTypeName(string typeName, Func<Task<IEnumerable<ChartNode>>> retrieval);
  }
}
