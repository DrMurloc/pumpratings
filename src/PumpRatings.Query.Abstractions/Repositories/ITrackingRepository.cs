﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Repositories
{
  public interface ITrackingRepository
  {
    Task<IEnumerable<TrackingNode>> SearchTracking(int page, int count);
    Task<IEnumerable<TrackingNode>> GetTrackingByUserIds(IEnumerable<string> userIds);
  }
}
