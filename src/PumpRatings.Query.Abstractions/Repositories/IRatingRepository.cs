﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Repositories
{
  public interface IRatingRepository
  {
    Task<IEnumerable<RatingNode>> GetRatingsByChartIdsAndUserId(IEnumerable<string> chartIds, string userId);
    Task<IEnumerable<RatingNode>> GetRatingsByChartIds(IEnumerable<string> chartIds);
    Task<IEnumerable<RatingNode>> GetRatingsByUserIds(IEnumerable<string> userIds);
  }
}
