﻿using PumpRatings.Query.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PumpRatings.Query.Abstractions.Repositories
{
  public interface ICommentRepository
  {
    Task<IEnumerable<CommentNode>> GetCommentsByUserIds(IEnumerable<string> userIds);
    Task<IEnumerable<CommentNode>> GetCommentsByChartIds(IEnumerable<string> chartIds);
  }
}
