﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Repositories
{
  public interface IChartRepository
  {
    Task<IEnumerable<ChartNode>> GetAllCharts();
    Task<double> GetChartAverageRating(string chartId);
    Task<IEnumerable<ListChartEdge>> GetChartIdsByList(IEnumerable<string> listIds);
  }
}
