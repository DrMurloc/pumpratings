﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Repositories
{
  public interface IChartTypeRepository
  {
    Task<IEnumerable<ChartTypeNode>> GetAllChartTypes();
  }
}
