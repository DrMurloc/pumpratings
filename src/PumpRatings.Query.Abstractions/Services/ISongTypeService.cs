﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface ISongTypeService
  {
    Task<IEnumerable<SongTypeNode>> SearchForSongTypes();
    Task<SongTypeNode> GetSongTypeByName(string songTypeName);
  }
}
