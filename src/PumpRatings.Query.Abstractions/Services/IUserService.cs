﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface IUserService
  {
    Task<UserNode> GetMe();
    Task<IEnumerable<UserNode>> SearchForUsers();
    Task<UserNode> GetUserById(string userId);
    Task<IEnumerable<UserNode>> GetUsersByRoleName(string roleName);
  }
}
