﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface IReleaseNotesService
  {
    Task<IEnumerable<ReleaseNotesNode>> SearchForReleaseNotes(int page, int count);
    Task<IEnumerable<ReleaseNotesNode>> GetReleaseNotesByUserId(string userId);
  }
}
