﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface ICommentService
  {
    Task<IEnumerable<CommentNode>> GetCommentsByChartId(string chartId);
    Task<IEnumerable<CommentNode>> GetCommentsByUserId(string userId);
  }
}
