﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface ISongService
  {
    Task<IEnumerable<SongNode>> SearchForSongs();
    Task<IEnumerable<SongNode>> GetSongsByMixId(string mixId);
    Task<IEnumerable<SongNode>> GetSongsByTypeName(string songTypeName);
    Task<SongNode> GetSongById(string songId);
  }
}
