﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface IChartService
  {
    Task<IEnumerable<ChartNode>> SearchForCharts();
    Task<IEnumerable<ChartNode>> GetChartsBySongId(string songId);
    Task<IEnumerable<ChartNode>> GetChartsByTagName(string tagName);
    Task<IEnumerable<ChartNode>> GetChartsByTypeName(string chartTypeName);
    Task<IEnumerable<ChartNode>> GetChartsByListId(string listId);
    Task<ChartNode> GetChartById(string chartId);
  }
}
