﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface IRatingService
  {
    Task<IEnumerable<RatingNode>> GetRatingsByChartId(string chartId);
    Task<RatingNode> GetMyRatingByChart(string chartId);
    Task<IEnumerable<RatingNode>> GetRatingsByUserId(string userId);
  }
}
