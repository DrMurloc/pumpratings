﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface ITrackingService
  {
    Task<IEnumerable<TrackingNode>> SearchForTracking(int page, int count);
    Task<IEnumerable<TrackingNode>> GetTrackingByUser(string userId);
  }
}
