﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface ILoginProviderService
  {
    Task<IEnumerable<LoginProviderNode>> SearchForLoginProviders();
  }
}
