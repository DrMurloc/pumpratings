﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface ITagService
  {
    Task<IEnumerable<TagNode>> SearchForTags();
    Task<TagNode> GetTagByName(string tagName);
    Task<IEnumerable<TagNode>> GetTagsByChart(string chartId);
  }
}
