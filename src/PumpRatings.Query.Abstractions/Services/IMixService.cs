﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface IMixService
  {
    Task<IEnumerable<MixNode>> SearchForMixes();
    Task<MixNode> GetMixById(string mixId);
  }
}
