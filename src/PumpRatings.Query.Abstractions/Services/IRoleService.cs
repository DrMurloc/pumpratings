﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Abstractions.Services
{
  public interface IRoleService
  {
    Task<IEnumerable<RoleNode>> SearchForRoles();
    Task<RoleNode> GetRoleByName(string roleName);
  }
}
