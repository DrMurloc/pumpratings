﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class CommentNode
  {
    public string UserId { get; set; }
    public string ChartId { get; set; }
    public DateTimeOffset CreatedDate { get; set; }
    public string Message { get; set; }
  }
}
