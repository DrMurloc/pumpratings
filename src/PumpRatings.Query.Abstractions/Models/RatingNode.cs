﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class RatingNode
  {
    public string ChartId { get; set; }
    public string UserId { get; set; }
    public double Rating { get; set; }
  }
}
