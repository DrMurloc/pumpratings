﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class ChartTagEdge
  {
    public string ChartId { get; set; }
    public string TagName { get; set; }
  }
}
