﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class TrackingNode
  {
    public int Id { get; set; }
    public DateTimeOffset ChangeDate { get; set; }
    public string UserId { get; set; }
    public string Summary { get; set; }
    public string Change { get; set; }
  }
}
