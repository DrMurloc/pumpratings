﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class ListChartEdge
  {
    public string ListId { get; set; }
    public string ChartId { get; set; }
  }
}
