﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class TagNode
  {
    public string Name { get; set; }
    public string Color { get; set; }
  }
}
