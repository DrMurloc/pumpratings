﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class SongTypeNode
  {
    public string Name { get; }
  }
}
