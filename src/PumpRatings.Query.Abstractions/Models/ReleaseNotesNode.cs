﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class ReleaseNotesNode
  {
    public int Id { get; set; }
    public string UserId { get; set; }
    public DateTimeOffset ReleaseDate { get; set; }
    public string Notes { get; set; }
  }
}
