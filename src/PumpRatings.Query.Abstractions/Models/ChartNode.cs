﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class ChartNode
  {
    public string Id { get; set; }
    public string SongId { get; set; }
    public int? Difficulty { get; set; }
    public string TypeName { get; set; }
    public string Video { get; set; }
    public int Players { get; set; }
    public double? AverageRating { get; set; }
  }
}
