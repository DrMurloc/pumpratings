﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class RoleNode
  {
    public string Name { get; set; }
  }
}
