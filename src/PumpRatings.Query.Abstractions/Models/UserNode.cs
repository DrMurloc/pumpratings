﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class UserNode
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public string RoleName { get; set; }
    public string CurrentRoleName { get; set; }
    public bool DarkTheme { get; set; }
  }
}
