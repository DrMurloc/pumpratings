﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class MixNode
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public string Image { get; set; }
    public int DisplayOrder { get; set; }
  }
}
