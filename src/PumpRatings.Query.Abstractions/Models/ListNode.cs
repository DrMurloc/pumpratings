﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class ListNode
  {
    public string Id { get; set; }
    public string UserId { get; set; }
    public string Name { get; set; }
    public bool IsFavorites { get; set; }
  }
}
