﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class SongNode
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public string Artist { get; set; }
    public string Bpm { get; set; }
    public string Image { get; set; }
    public string MixId { get; set; }
    public string TypeName { get; set; }
  }
}
