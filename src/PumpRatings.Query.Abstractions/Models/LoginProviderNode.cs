﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Query.Abstractions.Models
{
  public sealed class LoginProviderNode
  {
    public string DisplayName { get; set; }
    public string Scheme { get; set; }
    public string Icon { get; set; }
  }
}
