﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Models;

namespace PumpRatings.Abstractions.Security
{
  public interface IUserProfileSetter
  {
    Task SetProfile(UserProfile profile);
  }
}
