﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Abstractions.Security
{
  public static class Role
  {
    public const string User = "User";
    public const string Admin = "Admin";
    public const string SuperKamiGuru = "Super Kami Guru";
    public const string Anonymous = "Anonymous";
  }
}
