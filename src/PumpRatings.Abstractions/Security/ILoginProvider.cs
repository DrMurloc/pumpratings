﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace PumpRatings.Abstractions.Security
{
  public interface ILoginProvider
  {
    string Scheme { get; }
    string DisplayName { get; }
    string Icon { get; }
    string GetExternalUserId(IEnumerable<Claim> externalClaims);
  }
}
