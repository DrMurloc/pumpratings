﻿using System;
using System.Collections.Generic;
using System.Text;
using PumpRatings.Abstractions.Models;

namespace PumpRatings.Abstractions.Security
{
  public interface IUserProfileAccessor
  {
    UserProfile UserProfile { get; }
  }
}
