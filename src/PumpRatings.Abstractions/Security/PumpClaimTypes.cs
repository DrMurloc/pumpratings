﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Abstractions.Security
{
  public static class PumpClaimTypes
  {
    public const string DarkTheme = "PumpRatings.DarkTheme";
    public const string CurrentRole = "PumpRatings.CurrentRole";
  }
}
