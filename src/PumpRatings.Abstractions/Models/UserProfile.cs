﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Abstractions.Models
{
  public sealed class UserProfile
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public string Role { get; set; }
    public bool DarkTheme { get; set; }
    public string CurrentRole { get; set; }
  }
}
