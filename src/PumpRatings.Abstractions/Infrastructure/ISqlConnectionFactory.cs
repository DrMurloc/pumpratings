﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace PumpRatings.Abstractions.Infrastructure
{
  public interface ISqlConnectionFactory
  {
    Task<IDbConnection> ConnectAsync();
    IDbConnection Connect();
  }
}
