﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Abstractions.Infrastructure
{
  public interface IServiceResolver
  {
    TService GetService<TService>();
  }
}
