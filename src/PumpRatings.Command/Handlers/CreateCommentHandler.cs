﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class CreateCommentHandler : ICommandHandler<CreateCommentCommand>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly ICommentRepository _repository;
    private readonly IUserProfileAccessor _user;
    public CreateCommentHandler(IEventLauncher eventLauncher,
      ICommentRepository repository,
      IUserProfileAccessor user)
    {
      _eventLauncher = eventLauncher;
      _repository = repository;
      _user = user;
    }
    public async Task Handle(CreateCommentCommand command)
    {
      var profile = _user.UserProfile;
      if (string.IsNullOrWhiteSpace(command.ChartId))
      {
        throw new BadCommandArgumentsException<CreateCommentCommand>(nameof(command.ChartId));
      }

      command.Message = command.Message ?? string.Empty;
      await _repository.CreateComment(new CreateCommentModel
      {
        ChartId = command.ChartId,
        CreatedDate = DateTimeOffset.Now,
        Message = command.Message,
        UserId = profile.Id
      });
      _eventLauncher.Launch(_user.UserProfile,new CommentCreatedEvent
      {
        ChartId = command.ChartId,
        Message = command.Message,
        UserId = profile.Id,
        UserName = profile.Name
      });
    }
  }
}
