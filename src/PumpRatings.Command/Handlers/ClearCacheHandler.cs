﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class ClearCacheHandler : ICommandHandler<ClearCacheCommand>
  {
    private readonly IEventLauncher _launcher;
    private readonly IUserProfileAccessor _user;
    public ClearCacheHandler(IEventLauncher launcher,
      IUserProfileAccessor user)
    {
      _launcher = launcher;
      _user = user;
    }
    public Task Handle(ClearCacheCommand command)
    {
      return Task.Run(()=>
        _launcher.Launch(_user.UserProfile,new ClearCacheEvent())
      );
    }
  }
}
