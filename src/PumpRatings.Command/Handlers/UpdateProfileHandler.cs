﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class UpdateProfileHandler : ICommandHandler<UpdateProfileCommand>
  {
    private readonly IUserProfileAccessor _userProfileAccessor;
    private readonly IUserProfileSetter _userProfileSetter;
    private readonly IProfileRepository _repository;
    private readonly IEventLauncher _eventLauncher;
    public UpdateProfileHandler(IUserProfileAccessor userProfileAccessor,
      IUserProfileSetter userProfileSetter,
      IProfileRepository profileRepository,
      IEventLauncher eventLauncher)
    {
      _userProfileAccessor = userProfileAccessor;
      _userProfileSetter = userProfileSetter;
      _repository = profileRepository;
      _eventLauncher = eventLauncher;
    }
    public async Task Handle(UpdateProfileCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.Name))
      {
        throw new BadCommandArgumentsException<UpdateProfileCommand>(nameof(command.Name));
      }
      var user = _userProfileAccessor.UserProfile;
      if(string.IsNullOrWhiteSpace(command.Name)
       || (user.Role==Role.User && command.CurrentRole!=Role.User)
       || (user.Role==Role.Admin && !(command.CurrentRole==Role.User || command.CurrentRole==Role.Admin)))
      {
        throw new BadCommandArgumentsException<UpdateProfileCommand>(nameof(command.CurrentRole));
      }
      await _repository.UpdateProfile(new UpdateProfileModel
      {
        CurrentRole = command.CurrentRole,
        DarkTheme = command.DarkTheme,
        Name = command.Name,
        UserId = user.Id
      });
      _eventLauncher.Launch(_userProfileAccessor.UserProfile,new ProfileUpdatedEvent
      {
        CurrentRole = command.CurrentRole,
        DarkTheme = command.DarkTheme,
        Name=command.Name,
        UserId = user.Id
      });
      user.CurrentRole = command.CurrentRole;
      user.Name = command.Name;
      user.DarkTheme = command.DarkTheme;
      await _userProfileSetter.SetProfile(user);
    }
  }
}
