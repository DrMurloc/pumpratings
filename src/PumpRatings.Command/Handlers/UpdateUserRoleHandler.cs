﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class UpdateUserRoleHandler : ICommandHandler<UpdateUserRoleCommand>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly IUserProfileAccessor _user;
    private readonly IProfileRepository _repository;
    public UpdateUserRoleHandler(IEventLauncher eventLauncher,
      IUserProfileAccessor user,
      IProfileRepository repository)
    {
      _eventLauncher = eventLauncher;
      _user = user;
      _repository = repository;
    }
    public async Task Handle(UpdateUserRoleCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.UserId))
      {
        throw new BadCommandArgumentsException<UpdateUserRoleCommand>(nameof(command.UserId));
      }
      var targetUser = await _repository.GetProfileByUserId(command.UserId);
      if (targetUser == null)
      {
        throw new BadCommandArgumentsException<UpdateUserRoleCommand>(nameof(command.UserId));
      }

      var role = _user.UserProfile.Role;
      if (role==Role.User
        || (role == Role.Admin && command.Role == Role.SuperKamiGuru)
        || (role == Role.Admin && (targetUser.Role == Role.SuperKamiGuru || targetUser.Role == Role.Admin))
        || (role == Role.SuperKamiGuru && targetUser.Role == Role.SuperKamiGuru))
      {
        throw new BadCommandArgumentsException<UpdateUserRoleCommand>(nameof(command.Role));
      }
      await _repository.UpdateRole(command.UserId, command.Role);
      _eventLauncher.Launch(_user.UserProfile,new UserRoleUpdatedEvent
      {
        Role=command.Role,
        UserId=command.UserId
      });
    }
  }
}
