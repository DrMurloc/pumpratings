﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class UpdateChartTagsHandler : ICommandHandler<UpdateChartTagsCommand>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly IChartRepository _chartRepository;
    private readonly IUserProfileAccessor _user;

    public UpdateChartTagsHandler(IEventLauncher eventLauncher,
      IChartRepository chartRepository,
      IUserProfileAccessor user)
    {
      _eventLauncher = eventLauncher;
      _chartRepository = chartRepository;
      _user = user;
    }

    public async Task Handle(UpdateChartTagsCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.ChartId))
      {
        throw new BadCommandArgumentsException<UpdateChartTagsCommand>(nameof(command.ChartId));
      }
      command.TagNames = command.TagNames.Where(tag => !string.IsNullOrWhiteSpace(tag)).Distinct().ToArray();
      await _chartRepository.UpdateCharTags(command.ChartId, command.TagNames);
      _eventLauncher.Launch(_user.UserProfile,new ChartTagsUpdatedEvent
      {
        ChartId=command.ChartId,
        TagNames=command.TagNames
      });
    }
  }
}
