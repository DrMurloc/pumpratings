﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class CreateReleaseNotesHandler : ICommandHandler<CreateReleaseNotesCommand>
  {
    private readonly IReleaseNotesRepository _repository;
    private readonly IUserProfileAccessor _user;
    private readonly IEventLauncher _eventLauncher;
    public CreateReleaseNotesHandler(IReleaseNotesRepository repository,
      IUserProfileAccessor user,
      IEventLauncher eventLauncher)
    {
      _repository = repository;
      _user = user;
      _eventLauncher = eventLauncher;
    }
    public async Task Handle(CreateReleaseNotesCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.Notes))
      {
        throw new BadCommandArgumentsException<CreateReleaseNotesCommand>(nameof(command.Notes));
      }
      var profile = _user.UserProfile;
      var now = DateTimeOffset.Now;
      await _repository.CreateReleaseNotes(new CreateReleaseNotesModel
      {
        Notes = command.Notes,
        ReleaseDate = now,
        UserId = profile.Id
      });
      _eventLauncher.Launch(_user.UserProfile,new ReleaseNotesCreatedEvent
      {
        Notes = command.Notes,
        ReleaseDate = now,
        UserId = profile.Id,
        UserName = profile.Name
      });
    }
  }
}
