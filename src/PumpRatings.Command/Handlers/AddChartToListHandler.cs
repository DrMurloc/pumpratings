﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class AddChartToListHandler : ICommandHandler<AddChartToListCommand>
  {
    private readonly IListChartRepository _repository;
    private readonly IEventLauncher _eventLauncher;
    private readonly IListRepository _listRepository;
    private readonly IUserProfileAccessor _user;
    public AddChartToListHandler(IListChartRepository repository,
      IEventLauncher eventLauncher,
      IListRepository listRepository,
      IUserProfileAccessor user)
    {
      _repository = repository;
      _eventLauncher = eventLauncher;
      _listRepository = listRepository;
      _user = user;
    }
    public async Task Handle(AddChartToListCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.ChartId))
      {
        throw new BadCommandArgumentsException<AddChartToListCommand>(nameof(command.ChartId));
      }

      if (string.IsNullOrWhiteSpace(command.ListId))
      {
        throw new BadCommandArgumentsException<AddChartToListCommand>(nameof(command.ListId));
      }

      var list = await _listRepository.GetList(command.ListId);
      if (list==null || list.UserId != _user.UserProfile.Id)
      {
        throw new BadCommandArgumentsException<AddChartToListCommand>(nameof(command.ListId));
      }
      await _repository.CreateListChart(command.ListId,command.ChartId);
      _eventLauncher.Launch(_user.UserProfile,new ChartAddedToListEvent
      {
        ChartId=command.ChartId,
        ListId=command.ListId
      });
    }
    
  }
}
