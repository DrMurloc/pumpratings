﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class UpdateRatingHandler : ICommandHandler<UpdateRatingCommand,double>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly IRatingsRepository _repository;
    private readonly IUserProfileAccessor _user;

    public UpdateRatingHandler(IEventLauncher eventLauncher,
      IRatingsRepository repository,
      IUserProfileAccessor user)
    {
      _eventLauncher = eventLauncher;
      _repository = repository;
      _user = user;
    }

    public async Task<double> Handle(UpdateRatingCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.ChartId))
      {
        throw new BadCommandArgumentsException<UpdateRatingCommand>(nameof(command.ChartId));
      }
      if (command.Rating < 0 || command.Rating > 5)
      {
        throw new BadCommandArgumentsException<UpdateRatingCommand>(nameof(command.Rating));
      }
      var profile = _user.UserProfile;
      await _repository.MergeRating(new MergeRatingModel
      {
        ChartId = command.ChartId,
        Rating = command.Rating,
        UserId = profile.Id
      });
      var newAverage = await _repository.GetAverageRating(command.ChartId);
      _eventLauncher.Launch(_user.UserProfile,new RatingUpdatedEvent
      {
        ChartId = command.ChartId,
        Rating=command.Rating,
        UserId=profile.Id,
        NewAverageRating = newAverage
      });
      return newAverage;
    }
  }
}
