﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class DeleteCommentHandler : ICommandHandler<DeleteCommentCommand>
  {
    private readonly IUserProfileAccessor _user;
    private readonly IEventLauncher _eventLauncher;
    private readonly ICommentRepository _repository;
    public DeleteCommentHandler(IUserProfileAccessor user,
      IEventLauncher eventLauncher,
      ICommentRepository repository)
    {
      _user = user;
      _eventLauncher = eventLauncher;
      _repository = repository;
    }
    public async Task Handle(DeleteCommentCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.ChartId))
      {
        throw new BadCommandArgumentsException<DeleteCommentCommand>(nameof(command.ChartId));
      }
      var profile = _user.UserProfile;
      await _repository.DeleteComment(command.ChartId, profile.Id);
      _eventLauncher.Launch(_user.UserProfile,new CommentDeletedEvent
      {
        ChartId=command.ChartId,
        UserId = profile.Id
      });
    }
  }
}
