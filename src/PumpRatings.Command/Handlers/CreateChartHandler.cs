﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class CreateChartHandler : ICommandHandler<CreateChartCommand,string>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly IChartRepository _chartRepository;
    private readonly IUserProfileAccessor _user;
    public CreateChartHandler(IEventLauncher eventLauncher,
      IChartRepository chartRepository,
      IUserProfileAccessor user)
    {
      _eventLauncher = eventLauncher;
      _chartRepository = chartRepository;
      _user = user;
    }
    public async Task<string> Handle(CreateChartCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.SongId))
      {
        throw new BadCommandArgumentsException<CreateChartCommand>(nameof(command.SongId));
      }
      if (command.Difficulty == null && command.Type != "CoOp")
      {
        throw new BadCommandArgumentsException<CreateChartCommand>(nameof(command.Difficulty));
      }
      if (command.Players < 1 ||(command.Players>1 && command.Type!="CoOp"))
      {
        throw new BadCommandArgumentsException<CreateChartCommand>(nameof(command.Players));
      }
      if (!string.IsNullOrEmpty(command.Video) &&
          !command.Video.StartsWith("https://www.youtube.com/embed/", StringComparison.OrdinalIgnoreCase))
      {
        throw new BadCommandArgumentsException<CreateChartCommand>(nameof(command.Video));
      }

      var newId = Guid.NewGuid().ToString();
      await _chartRepository.CreateChart(new CreateChartModel
      {
        Difficulty = command.Difficulty,
        Id = newId,
        Players = command.Players,
        SongId = command.SongId,
        Type = command.Type,
        Video = command.Video
      });
      _eventLauncher.Launch(_user.UserProfile,new ChartCreatedEvent
      {
        Difficulty = command.Difficulty,
        Id = newId,
        Players=command.Players,
        SongId = command.SongId,
        Type=command.Type,
        Video=command.Video
      });
      return newId;
    }
  }
}
