﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class CreateMixHandler : ICommandHandler<CreateMixCommand,string>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly IMixRepository _repository;
    private readonly IUserProfileAccessor _user;
    public CreateMixHandler(IEventLauncher eventLauncher,
      IMixRepository repository,
      IUserProfileAccessor user)
    {
      _eventLauncher = eventLauncher;
      _repository = repository;
      _user = user;
    }
    public async Task<string> Handle(CreateMixCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.Name))
      {
        throw new BadCommandArgumentsException<CreateMixCommand>(nameof(command.Name));
      }

      if (string.IsNullOrWhiteSpace(command.Image)
          || !command.Image.StartsWith("https://"))
      {
        throw new BadCommandArgumentsException<CreateMixCommand>(nameof(command.Image));
      }

      var newId = Guid.NewGuid().ToString();
      await _repository.CreateMix(new CreateMixModel
      {
        DisplayOrder = command.DisplayOrder,
        Id = newId,
        Image = command.Image,
        Name = command.Name
      });
      _eventLauncher.Launch(_user.UserProfile,new MixCreatedEvent
      {
        DisplayOrder = command.DisplayOrder,
        Id=newId,
        Image=command.Image,
        Name=command.Name
      });
      return newId;
    }
  }
}
