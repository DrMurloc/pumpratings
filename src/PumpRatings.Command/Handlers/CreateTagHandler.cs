﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class CreateTagHandler : ICommandHandler<CreateTagCommand>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly ITagRepository _repository;
    private readonly IUserProfileAccessor _user;
    public CreateTagHandler(IEventLauncher eventLauncher,
      ITagRepository repository,
      IUserProfileAccessor user)
    {
      _eventLauncher = eventLauncher;
      _repository = repository;
      _user = user;
    }
    public async Task Handle(CreateTagCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.Name))
      {
        throw new BadCommandArgumentsException<CreateTagCommand>(nameof(command.Name));
      }

      if (string.IsNullOrWhiteSpace(command.Color))
      {
        throw new BadCommandArgumentsException<CreateTagCommand>(nameof(command.Color));
      }

      await _repository.CreateTag(new CreateTagModel
      {
        Color = command.Color,
        Name = command.Name
      });
      _eventLauncher.Launch(_user.UserProfile,new TagCreatedEvent
      {
        Color=command.Color,
        Name=command.Name
      });
    }
  }
}
