﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Models;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class LoginHandler : ICommandHandler<LoginCommand>
  {
    private readonly IProfileRepository _repository;
    private readonly IEventLauncher _eventLauncher;
    private readonly IUserProfileSetter _user;
    private readonly IListRepository _listRepository;
    private readonly IEnumerable<ILoginProvider> _loginProviders;
    public LoginHandler(IEventLauncher eventLauncher,
      IProfileRepository repository,
      IListRepository listRepository,
      IUserProfileSetter user,
      IEnumerable<ILoginProvider> loginProviders)
    {
      _user = user;
      _listRepository = listRepository;
      _repository = repository;
      _eventLauncher = eventLauncher;
      _loginProviders = loginProviders;
    }
    public async Task Handle(LoginCommand command)
    {
      var loginProvider = _loginProviders.FirstOrDefault(provider => provider.Scheme == command.Scheme);
      if(loginProvider==null)
      {
        throw new BadCommandArgumentsException<LoginCommand>(nameof(command.Scheme));
      }

      var externalId = loginProvider.GetExternalUserId(command.ExternalClaims);
      if (externalId == null)
      {
        throw new BadCommandArgumentsException<LoginCommand>(nameof(command.ExternalClaims));
      }

      var user = await _repository.GetProfileByExternalId(command.Scheme, externalId);
      var profile = user==null?null:new UserProfile
      {
        CurrentRole = user.CurrentRole,
        DarkTheme = user.DarkTheme,
        Id = user.Id,
        Name = user.Name,
        Role = user.Role
      };
      if (profile == null)
      {
        profile = new UserProfile
        {
          Id=Guid.NewGuid().ToString(),
          CurrentRole = Role.User,
          Role=Role.User,
          DarkTheme = true,
          Name="Anonymous"
        };

        await _repository.CreateProfile(new CreateProfileModel
        {
          CurrentRole = profile.CurrentRole,
          DarkTheme = profile.DarkTheme,
          ExternalId = externalId,
          Id = profile.Id,
          LoginProvider = command.Scheme,
          Name = profile.Name,
          Role = profile.Role
        });
        _eventLauncher.Launch(profile, new ProfileCreatedEvent
        {
          CurrentRole = profile.Role,
          DarkTheme = profile.DarkTheme,
          Id = profile.Id,
          Name= profile.Name,
          Role= profile.Role
        });
        const bool isFavorites = true;
        const string listName = "Favorites";
        var newListId = Guid.NewGuid().ToString();
        await _listRepository.CreateList(new CreateListModel
        {
          Id = newListId,
          IsFavorites = isFavorites,
          Name = listName,
          UserId = profile.Id
        });
        _eventLauncher.Launch(profile, new ListCreatedEvent
        {
          Id=newListId,
          IsFavorites = isFavorites,
          Name=listName,
          UserId = profile.Id
        });
      }

      await _user.SetProfile(profile);
      _eventLauncher.Launch(profile, new LoggedInEvent
      {
        CurrentRole = user.CurrentRole,
        DarkTheme = user.DarkTheme,
        UserId=user.Id,
        Name=user.Name,
        Role=user.Role
      });
    }
  }
}
