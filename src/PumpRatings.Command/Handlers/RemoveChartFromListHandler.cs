﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class RemoveChartFromListHandler : ICommandHandler<RemoveChartFromListCommand>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly IListChartRepository _repository;
    private readonly IListRepository _listRepository;
    private readonly IUserProfileAccessor _user;
    public RemoveChartFromListHandler(IEventLauncher eventLauncher,
      IListChartRepository repository,
      IListRepository listRepository,
      IUserProfileAccessor user)
    {
      _eventLauncher = eventLauncher;
      _repository = repository;
      _user = user;
      _listRepository = listRepository;
    }
    public async Task Handle(RemoveChartFromListCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.ListId))
      {
        throw new BadCommandArgumentsException<RemoveChartFromListCommand>(nameof(command.ListId));
      }

      if (string.IsNullOrWhiteSpace(command.ChartId))
      {
        throw new BadCommandArgumentsException<RemoveChartFromListCommand>(nameof(command.ChartId));
      }
      var list = await _listRepository.GetList(command.ListId);
      if (list == null || list.UserId != _user.UserProfile.Id)
      {
        throw new BadCommandArgumentsException<AddChartToListCommand>(nameof(command.ListId));
      }
      await _repository.RemoveListChart(command.ListId, command.ChartId);
      _eventLauncher.Launch(_user.UserProfile,new ChartRemovedFromListEvent
      {
        ChartId=command.ChartId,
        ListId=command.ListId
      });
    }
  }
}
