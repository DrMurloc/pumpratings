﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class CreateSongHandler : ICommandHandler<CreateSongCommand,string>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly ISongRepository _songRepository;
    private readonly IUserProfileAccessor _user;
    public CreateSongHandler(IEventLauncher eventLauncher,
      ISongRepository songRepository,
      IUserProfileAccessor user)
    {
      _user = user;
      _eventLauncher = eventLauncher;
      _songRepository = songRepository;
    }
    public async Task<string> Handle(CreateSongCommand command)
    {
      if (string.IsNullOrWhiteSpace(command.Name))
      {
        throw new BadCommandArgumentsException<CreateSongCommand>(nameof(command.Name));
      }

      if (string.IsNullOrWhiteSpace(command.Artist))
      {
        throw new BadCommandArgumentsException<CreateSongCommand>(nameof(command.Artist));
      }

      if (string.IsNullOrWhiteSpace(command.Bpm))
      {
        throw new BadCommandArgumentsException<CreateSongCommand>(nameof(command.Bpm));
      }

      if (string.IsNullOrWhiteSpace(command.Image)
          || !command.Image.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
      {
        throw new BadCommandArgumentsException<CreateSongCommand>(nameof(command.Image));
      }

      if (string.IsNullOrWhiteSpace(command.MixId))
      {
        throw new BadCommandArgumentsException<CreateSongCommand>(nameof(command.MixId));
      }

      if (string.IsNullOrWhiteSpace(command.Type))
      {
        throw new BadCommandArgumentsException<CreateSongCommand>(nameof(command.Type));
      }

      var newId = Guid.NewGuid().ToString();
      await _songRepository.CreateSong(new CreateSongModel
      {
        Artist = command.Artist,
        Bpm = command.Bpm,
        Id = newId,
        Image = command.Image,
        MixId = command.MixId,
        Name = command.Name,
        Type = command.Type
      });

      _eventLauncher.Launch(_user.UserProfile,new SongCreatedEvent
      {
        Artist=command.Artist,
        Bpm=command.Bpm,
        Id=newId,
        Image=command.Image,
        MixId=command.MixId,
        Name=command.Name,
        Type=command.Type
      });
      return newId;
    }
  }
}
