﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Command.Handlers
{
  public sealed class UpdateChartHandler : ICommandHandler<UpdateChartCommand>
  {
    private readonly IEventLauncher _eventLauncher;
    private readonly IChartRepository _chartRepository;
    private readonly IUserProfileAccessor _user;
    public UpdateChartHandler(IEventLauncher eventLauncher,
      IChartRepository chartRepository,
      IUserProfileAccessor user)
    {
      _user = user;
      _eventLauncher = eventLauncher;
      _chartRepository = chartRepository;
    }
    public async Task Handle(UpdateChartCommand command)
    {
      if (!string.IsNullOrEmpty(command.Video) &&
          !command.Video.StartsWith("https://www.youtube.com/embed/", StringComparison.OrdinalIgnoreCase))
      {
        throw new BadCommandArgumentsException<UpdateChartCommand>(nameof(command.Video));
      }

      if (string.IsNullOrWhiteSpace(command.ChartId))
      {
        throw new BadCommandArgumentsException<UpdateChartCommand>(nameof(command.ChartId));
      }
      await _chartRepository.UpdateChart(new UpdateChartModel
      {
        Video = command.Video,
        ChartId = command.ChartId
      });
      _eventLauncher.Launch(_user.UserProfile,new ChartUpdatedEvent
      {
        ChartId=command.ChartId,
        Video=command.Video
      });
    }
  }
}
