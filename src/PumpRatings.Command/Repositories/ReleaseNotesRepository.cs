﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class ReleaseNotesRepository : IReleaseNotesRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;

    public ReleaseNotesRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task CreateReleaseNotes(CreateReleaseNotesModel record)
    {
      using (var scope = _metrics.BeginRepositoryScope<ReleaseNotesRepository>(nameof(CreateReleaseNotes)))
      {
        await _connection.ExecuteAsync(@"
INSERT INTO ReleaseNotes
(UserId, ReleaseDate, Notes)
VALUES
(@UserId, @ReleaseDate, @Notes)", record);
      }
    }
  }
}
