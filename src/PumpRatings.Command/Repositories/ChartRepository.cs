﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class ChartRepository : IChartRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public ChartRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task CreateChart(CreateChartModel record)
    {
      using (var scope = _metrics.BeginRepositoryScope<ChartRepository>(nameof(CreateChart)))
      {
        await _connection.ExecuteAsync(@"
INSERT INTO Charts
(Id, Difficulty, Type, SongId, Video, Players)
VALUES
(@Id, @Difficulty, @Type, @SongId, @Video, @Players)",record);
      }
    }

    public async Task UpdateChart(UpdateChartModel model)
    {
      using (var scope = _metrics.BeginRepositoryScope<ChartRepository>(nameof(UpdateChart)))
      {
        await _connection.ExecuteAsync(@"
UPDATE Charts
SET Video = @Video
WHERE Id = @ChartId",model);
      }
    }

    public async Task UpdateCharTags(string chartId, IEnumerable<string> tagNames)
    {
      using (var scope = _metrics.BeginRepositoryScope<ChartRepository>(nameof(UpdateCharTags)))
      {
        using (var transaction = _connection.BeginTransaction())
        {
          await _connection.ExecuteAsync(@"
DELETE FROM ChartTag
WHERE ChartId = @chartId", new {chartId},transaction);
          await _connection.ExecuteAsync(@"
INSERT INTO ChartTag
(ChartId, Tag)
VALUES
(@chartId, @tagName)", tagNames.Select(tagName => new {chartId, tagName}),transaction);
          transaction.Commit();
        }
      }
    }
  }
}
