﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class CommentRepository : ICommentRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public CommentRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task CreateComment(CreateCommentModel record)
    {
      using (var scope = _metrics.BeginRepositoryScope<CommentRepository>(nameof(CreateComment)))
      {
        await _connection.ExecuteAsync(@"
INSERT INTO Comments
(UserId, ChartId, CreatedDate, Message)
VALUES
(@UserId, @ChartId, @CreatedDate, @Message)", record);
      }
    }

    public async Task DeleteComment(string chartId, string userId)
    {
      using (var scope = _metrics.BeginRepositoryScope<CommentRepository>(nameof(DeleteComment)))
      {
        await _connection.ExecuteAsync(@"
DELETE FROM Comments
WHERE UserId = @userId
  AND ChartId = @chartId", new {userId, chartId});
      }
    }
  }
}
