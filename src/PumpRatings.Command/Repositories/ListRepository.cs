﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class ListRepository : IListRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public ListRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task CreateList(CreateListModel model)
    {
      using (var scope = _metrics.BeginRepositoryScope<ListRepository>(nameof(CreateList)))
      {
        await _connection.ExecuteAsync(@"
INSERT INTO Lists
(Id, UserId, Name, IsFavorite)
VALUES
(@Id, @UserId, @Name, @IsFavorites)", model);
      }
    }

    public async Task<GetListModel> GetList(string listId)
    {
      using (var scope = _metrics.BeginRepositoryScope<ListRepository>(nameof(GetList)))
      {
        var result = await _connection.QueryFirstOrDefaultAsync<GetListModel>(@"
SELECT Id,
  UserId
FROM Lists
WHERE Id = @listId", new {listId});
        scope.ResultCount = result == null ? 0 : 1;
        return result;
      }
    }
  }
}
