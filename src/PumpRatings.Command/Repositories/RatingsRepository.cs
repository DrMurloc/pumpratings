﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class RatingsRepository : IRatingsRepository
  {
    private readonly IMetricsService _metrics;
    private readonly IDbConnection _connection;

    public RatingsRepository(IMetricsService metrics,
      IDbConnection connection)
    {
      _connection = connection;
      _metrics = metrics;
    }

    public async Task MergeRating(MergeRatingModel mergeRatingModel)
    {
      using (var scope = _metrics.BeginRepositoryScope<RatingsRepository>(nameof(MergeRating)))
      {
        await _connection.ExecuteAsync(@"
MERGE INTO Ratings r
USING (SELECT @ChartId ChartId, @UserId UserId, @Rating Rating) AS s ON r.ChartId = s.ChartId AND r.UserId=s.UserId
WHEN MATCHED THEN
  UPDATE SET r.Rating = s.Rating
WHEN NOT MATCHED THEN
  INSERT
  (ChartId, UserId, Rating)
  VALUES
  (s.ChartId, s.UserId, s.Rating);", mergeRatingModel);
      }
    }

    public async Task<double> GetAverageRating(string chartId)
    {
      using (var scope = _metrics.BeginRepositoryScope<RatingsRepository>(nameof(GetAverageRating)))
      {
        var result = await _connection.QueryFirstOrDefaultAsync<double>(@"
SELECT AVG(Rating)
FROM Ratings
WHERE ChartId = @chartId
GROUP BY ChartId", new {chartId});
        scope.ResultCount = 1;
        return result;
      }
    }
  }
}
