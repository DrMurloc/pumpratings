﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class ListChartsRepository : IListChartRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public ListChartsRepository(IDbConnection connection, IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }

    public async Task CreateListChart(string listId, string chartId)
    {
      using (var scope = _metrics.BeginRepositoryScope<ListChartsRepository>(nameof(CreateListChart)))
      {
        await _connection.ExecuteAsync(@"
INSERT INTO ListCharts
(ListId, ChartId)
VALUES
(@listId, @chartId)", new {listId, chartId});
      }
    }

    public async Task RemoveListChart(string listId, string chartId)
    {
      using (var scope = _metrics.BeginRepositoryScope<ListChartsRepository>(nameof(RemoveListChart)))
      {
        await _connection.ExecuteAsync(@"
DELETE FROM ListCharts
WHERE ListId = @listId
  AND ChartId = @chartId", new {listId, chartId});
      }
    }
  }
}
