﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class TagRepository : ITagRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;

    public TagRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }

    public async Task CreateTag(CreateTagModel record)
    {
      using (var scope = _metrics.BeginRepositoryScope<TagRepository>(nameof(CreateTag)))
      {
        await _connection.ExecuteAsync(@"
INSERT INTO Tags
(Name, Color)
VALUES
(@Name, @Color)", record);
      }
    }
  }
}
