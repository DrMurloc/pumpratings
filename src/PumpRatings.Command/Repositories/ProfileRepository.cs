﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class ProfileRepository : IProfileRepository
  {
    private readonly IMetricsService _metrics;
    private readonly IDbConnection _connection;
    public ProfileRepository(IMetricsService metrics,
      IDbConnection connection)
    {
      _metrics = metrics;
      _connection = connection;
    }

    public async Task CreateProfile(CreateProfileModel model)
    {
      using (var scope = _metrics.BeginRepositoryScope<ProfileRepository>(nameof(CreateProfile)))
      {
        await _connection.ExecuteAsync(@"
INSERT INTO Users
(Id, Name, Role, ExternalId, LoginProvider, DarkTheme, CurrentRole)
VALUES
(@Id, @Name, @Role, @ExternalId, @LoginProvider, @DarkTheme, @CurrentRole)",model);
      }
    }

    public async Task UpdateProfile(UpdateProfileModel model)
    {
      using (var scope = _metrics.BeginRepositoryScope<ProfileRepository>(nameof(UpdateProfile)))
      {
        await _connection.ExecuteAsync(@"
UPDATE Users
SET Name = @Name,
  DarkTheme = @DarkTheme,
  CurrentRole = @CurrentRole
WHERE Id = @UserId", model);
      }
    }

    public async Task UpdateRole(string userId, string role)
    {
      using (var scope = _metrics.BeginRepositoryScope<ProfileRepository>(nameof(UpdateProfile)))
      {
        await _connection.ExecuteAsync(@"
UPDATE Users
SET Role = @role,
  CurrentRole = @role
WHERE Id = @userId", new {userId, role});
      }
    }

    public async Task<GetProfileModel> GetProfileByExternalId(string loginProvider, string externalId)
    {
      using (var scope = _metrics.BeginRepositoryScope<ProfileRepository>(nameof(GetProfileByExternalId)))
      {
        var result = await _connection.QueryFirstOrDefaultAsync<GetProfileModel>(@"
SELECT Id,
  Name,
  Role,
  DarkTheme,
  CurrentRole
FROM Users
WHERE LoginProvider = @loginProvider
  AND ExternalId = @externalId", new {loginProvider, externalId});
        scope.ResultCount = result == null ? 0 : 1;
        return result;
      }
    }
    public async Task<GetProfileModel> GetProfileByUserId(string userId)
    {
      using (var scope = _metrics.BeginRepositoryScope<ProfileRepository>(nameof(GetProfileByExternalId)))
      {
        var result = await _connection.QueryFirstOrDefaultAsync<GetProfileModel>(@"
SELECT Id,
  Name,
  Role,
  DarkTheme,
  CurrentRole
FROM Users
WHERE Id = @userId", new { userId });
        scope.ResultCount = result == null ? 0 : 1;
        return result;
      }
    }
  }
}
