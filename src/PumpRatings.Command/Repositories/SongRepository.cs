﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class SongRepository : ISongRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;

    public SongRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task CreateSong(CreateSongModel record)
    {
      using (var scope = _metrics.BeginRepositoryScope<SongRepository>(nameof(CreateSong)))
      {
        await _connection.ExecuteAsync(@"
INSERT INTO Songs
(Id, Name, Artist, Bpm, Image, MixId, Type)
VALUES
(@Id, @Name, @Artist, @Bpm, @Image, @MixId, @Type)", record);
      }
    }
  }
}
