﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Command.Abstractions.Models;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Command.Repositories
{
  public sealed class MixRepository : IMixRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public MixRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task CreateMix(CreateMixModel mixesRecord)
    {
      using (var scope = _metrics.BeginRepositoryScope<MixRepository>(nameof(CreateMix)))
      {
        await _connection.ExecuteAsync(@"
INSERT INTO Mixes
(Id, Name, Image, DisplayOrder)
VALUES
(@Id, @Name, @Image, @DisplayOrder)", mixesRecord);
      }
    }
  }
}
