﻿
using Microsoft.Extensions.DependencyInjection;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Repositories;
using PumpRatings.Command.Handlers;
using PumpRatings.Command.Repositories;

namespace PumpRatings.Command
{
  public static class DependencyInjection
  {
    public static IServiceCollection AddPumpRatingsCommand(this IServiceCollection builder)
    {
      return builder
        .AddDataLayer()
        .AddCommandHandlers();
    }

    private static IServiceCollection AddDataLayer(this IServiceCollection builder)
    {
      return builder.AddTransient<IChartRepository, ChartRepository>()
        .AddTransient<ICommentRepository, CommentRepository>()
        .AddTransient<IMixRepository, MixRepository>()
        .AddTransient<IProfileRepository, ProfileRepository>()
        .AddTransient<IRatingsRepository, RatingsRepository>()
        .AddTransient<IReleaseNotesRepository, ReleaseNotesRepository>()
        .AddTransient<ISongRepository, SongRepository>()
        .AddTransient<ITagRepository, TagRepository>()
        .AddTransient<IListRepository,ListRepository>()
        .AddTransient<IListChartRepository,ListChartsRepository>();
    }
    private static IServiceCollection AddCommandHandlers(this IServiceCollection builder)
    {
      return builder.AddCommandHandler<ClearCacheCommand,ClearCacheHandler>()
        .AddCommandHandler<CreateChartCommand, string, CreateChartHandler>()
        .AddCommandHandler<CreateCommentCommand, CreateCommentHandler>()
        .AddCommandHandler<CreateMixCommand, string, CreateMixHandler>()
        .AddCommandHandler<CreateReleaseNotesCommand, CreateReleaseNotesHandler>()
        .AddCommandHandler<CreateSongCommand, string, CreateSongHandler>()
        .AddCommandHandler<CreateTagCommand, CreateTagHandler>()
        .AddCommandHandler<DeleteCommentCommand, DeleteCommentHandler>()
        .AddCommandHandler<UpdateChartCommand, UpdateChartHandler>()
        .AddCommandHandler<UpdateProfileCommand, UpdateProfileHandler>()
        .AddCommandHandler<UpdateRatingCommand, double, UpdateRatingHandler>()
        .AddCommandHandler<UpdateChartTagsCommand, UpdateChartTagsHandler>()
        .AddCommandHandler<UpdateUserRoleCommand, UpdateUserRoleHandler>()
        .AddCommandHandler<AddChartToListCommand, AddChartToListHandler>()
        .AddCommandHandler<RemoveChartFromListCommand,RemoveChartFromListHandler>()
        .AddCommandHandler<LoginCommand,LoginHandler>();

    }
    private static IServiceCollection AddCommandHandler<TCommandType, THandler>(this IServiceCollection builder)
      where THandler: class, ICommandHandler<TCommandType>
    {
      return builder.AddTransient<ICommandHandler<TCommandType>, THandler>();
    }

    private static IServiceCollection AddCommandHandler<TCommandType, TReturnType, THandler>(
      this IServiceCollection builder)
      where THandler: class, ICommandHandler<TCommandType,TReturnType>
    {
      return builder.AddTransient<ICommandHandler<TCommandType,TReturnType>, THandler>();
    }
  }
}
