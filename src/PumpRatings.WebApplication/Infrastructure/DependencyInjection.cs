﻿using System.Data;
using Microsoft.Extensions.DependencyInjection;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Abstractions.Security;
using PumpRatings.WebApplication.Configuration;

namespace PumpRatings.WebApplication.Infrastructure
{
  public static class DependencyInjection
  {
    public static IServiceCollection AddInfrastructure(this IServiceCollection builder, ApplicationConfiguration configuration)
    {
      return builder.Configure<DataLayerConfiguration>(options=>
      {
        options.Sql = configuration.DataLayer.Sql;
      }).AddSingleton<ISqlConnectionFactory,SqlConnectionFactory>()
        .AddScoped<IDbConnection>(services=>services.GetService<ISqlConnectionFactory>().Connect())
        .AddSingleton<IServiceResolver,HttpContextServiceResolver>()
        .AddScoped<UserProfileManager>()
        .AddScoped<IUserProfileAccessor>(services=>services.GetService<UserProfileManager>())
        .AddScoped<IUserProfileSetter>(services=>services.GetService<UserProfileManager>());
    }
  }
}
