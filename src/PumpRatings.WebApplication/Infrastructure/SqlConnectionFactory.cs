﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.WebApplication.Configuration;

namespace PumpRatings.WebApplication.Infrastructure
{
  public sealed class SqlConnectionFactory : ISqlConnectionFactory
  {
    private readonly string _connectionString;
    public SqlConnectionFactory(IOptions<DataLayerConfiguration> config)
    {
      var sql = config.Value.Sql;
      _connectionString = $"Server={sql.Server};";
      _connectionString += $"Initial Catalog={sql.InitialCatalog};";
      _connectionString += $"User ID={sql.UserId};";
      _connectionString += $"Password={sql.Password};";
      if (!string.IsNullOrWhiteSpace(sql.PersistSecurityInfo))
      {
        _connectionString += $"Persist Security Info={sql.PersistSecurityInfo};";
      }
      _connectionString += $"MultipleActiveResultSets=True;";


      if (!string.IsNullOrWhiteSpace(sql.Encrypt))
      {
        _connectionString += $"Encrypt={sql.Encrypt};";
      }

      if (!string.IsNullOrWhiteSpace(sql.TrustServerCertificate))
      {
        _connectionString += $"TrustServerCertificate={sql.TrustServerCertificate};";
      }

      if (sql.ConnectionTimeout != null)
      {
        _connectionString += $"Connection Timeout={sql.ConnectionTimeout.Value};";
      }
    }
    public async Task<IDbConnection> ConnectAsync()
    {
      var connection = new SqlConnection(_connectionString);
      await connection.OpenAsync();
      return connection;
    }

    public IDbConnection Connect()
    {
      var connection = new SqlConnection(_connectionString);
      connection.Open();
      return connection;
    }
  }
}
