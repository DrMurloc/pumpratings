﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using PumpRatings.Abstractions.Models;
using PumpRatings.Abstractions.Security;

namespace PumpRatings.WebApplication.Infrastructure
{
  public sealed class UserProfileManager : IUserProfileAccessor,
    IUserProfileSetter
  {
    private readonly IHttpContextAccessor _httpContextAccessor;

    public UserProfileManager(IHttpContextAccessor httpContextAccessor)
    {
      _httpContextAccessor = httpContextAccessor;
    }

    private UserProfile _userProfile;
    public UserProfile UserProfile {
      get
      {
        if (_userProfile != null)
        {
          return _userProfile;
        }
        var userPrincipal = _httpContextAccessor.HttpContext.User;
        
        return _userProfile=userPrincipal.Claims.Any() ? new UserProfile
        {
          Id = userPrincipal.FindFirstValue(ClaimTypes.NameIdentifier),
          Name = userPrincipal.FindFirstValue(ClaimTypes.Name),
          Role = userPrincipal.FindFirstValue(ClaimTypes.Role),
          DarkTheme = bool.TryParse(userPrincipal.FindFirstValue(PumpClaimTypes.DarkTheme), out var value) && value,
          CurrentRole = userPrincipal.FindFirstValue(PumpClaimTypes.CurrentRole)
        }:new UserProfile
        {
          Id = "Anonymous",
          Name = "Anonymous",
          CurrentRole = Role.Anonymous,
          Role = Role.Anonymous,
          DarkTheme = true
        };
      }
    }
    public async Task SetProfile(UserProfile profile)
    {
      var principal = new ClaimsPrincipal(new ClaimsIdentity(new[]
      {
        new Claim(ClaimTypes.NameIdentifier,profile.Id),
        new Claim(ClaimTypes.Name,profile.Name),
        new Claim(ClaimTypes.Role,profile.Role),
        new Claim(PumpClaimTypes.DarkTheme,profile.DarkTheme.ToString()),
        new Claim(PumpClaimTypes.CurrentRole,profile.CurrentRole)
      }));
      await _httpContextAccessor.HttpContext.SignOutAsync();
      await _httpContextAccessor.HttpContext.SignInAsync(principal);
      _userProfile = profile;
    }
  }
}
