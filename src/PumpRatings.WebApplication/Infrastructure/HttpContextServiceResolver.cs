﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PumpRatings.Abstractions.Infrastructure;

namespace PumpRatings.WebApplication.Infrastructure
{
  public sealed class HttpContextServiceResolver : IServiceResolver
  {
    private readonly IHttpContextAccessor _httpContextAccessor;
    public HttpContextServiceResolver(IHttpContextAccessor httpContextAccessor)
    {
      _httpContextAccessor = httpContextAccessor;
    }
    public TService GetService<TService>()
    {
      return (TService) _httpContextAccessor.HttpContext.RequestServices.GetService(typeof(TService));
    }
  }
}
