﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.WebApplication.Configuration
{
  public sealed class DataLayerConfiguration
  {
    public SqlConfiguration Sql { get; set; } = new SqlConfiguration();

    public sealed class SqlConfiguration
    {
      public string Server { get; set; }
      public string InitialCatalog { get; set; }
      public string UserId { get; set; }
      public string Password { get; set; }
      public string PersistSecurityInfo { get; set; }
      public string Encrypt { get; set; }
      public string TrustServerCertificate { get; set; }
      public int? ConnectionTimeout { get; set; }
    }
  }
}