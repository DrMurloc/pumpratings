﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.WebApplication.Configuration
{
  public sealed class AuthenticationConfiguration
  {
    public GoogleOauthConfiguration Google { get; set; } = new GoogleOauthConfiguration();
    public TwitterOauthConfiguration Twitter { get; set; } = new TwitterOauthConfiguration();
    public FacebookOauthConfiguration Facebook { get; set; } = new FacebookOauthConfiguration();
    public DiscordOauthConfiguration Discord { get; set; } = new DiscordOauthConfiguration();
    public sealed class GoogleOauthConfiguration
    {
      public string ClientId { get; set; } = string.Empty;
      public string ClientSecret { get; set; } = string.Empty;
    }

    public sealed class TwitterOauthConfiguration
    {
      public string ConsumerKey { get; set; } = string.Empty;
      public string ConsumerSecret { get; set; } = string.Empty;
    }

    public sealed class FacebookOauthConfiguration
    {
      public string AppId { get; set; } = string.Empty;
      public string AppSecret { get; set; } = string.Empty;
    }

    public sealed class DiscordOauthConfiguration
    {
      public string ClientId { get; set; } = string.Empty;
      public string ClientSecret { get; set; } = string.Empty;
    }
  }
}
