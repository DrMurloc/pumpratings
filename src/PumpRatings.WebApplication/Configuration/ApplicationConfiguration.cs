﻿namespace PumpRatings.WebApplication.Configuration
{
  public sealed class ApplicationConfiguration
  {
    public DataLayerConfiguration DataLayer { get; set; } = new DataLayerConfiguration();
    public AuthenticationConfiguration Authentication { get; set; } = new AuthenticationConfiguration();
    public LogDnaConfiguration LogDna { get; set; } = new LogDnaConfiguration();
    public sealed class LogDnaConfiguration
    {
      public string IngestionKey { get; set; } = string.Empty;
      public string HostName { get; set; } = string.Empty;
    }
  }
}
