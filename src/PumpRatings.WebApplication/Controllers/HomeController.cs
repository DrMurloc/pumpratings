﻿using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PumpRatings.Query.Abstractions.Services;
using PumpRatings.WebApplication.Security.Attributes;

namespace PumpRatings.WebApplication.Controllers
{
  [Route("")]
  public class HomeController : Controller
  {
    [HttpGet("")]
    public IActionResult Home() => View("Index");


    [HttpGet("/sitemap.xml")]
    public async Task SitemapXml([FromServices] IMixService mixService,
      [FromServices] ISongService songService,
      [FromServices] IChartService chartService)
    {
      var host = "https://" + Request.Host;
      Response.ContentType = "application/xml";
      var mixes=await mixService.SearchForMixes();
      var songs = await songService.SearchForSongs();
      var charts = await chartService.SearchForCharts();
      using (var xml = XmlWriter.Create(Response.Body, new XmlWriterSettings() {Indent = true}))
      {
        void addUrl(string vueRouterPath)
        {
          xml.WriteStartElement("url");
          xml.WriteElementString("loc",host+"/#"+vueRouterPath);
          xml.WriteEndElement();
        }
        xml.WriteStartDocument();
        xml.WriteStartElement("urlset", "http://www.sitemaps.org/schemas/sitemap/0.9");
        addUrl("/");
        foreach(var mix in mixes)
        {
          addUrl("/mixes/"+mix.Id);
        }

        foreach (var song in songs)
        {
          addUrl("/songs/"+song.Id);
        }

        foreach (var chart in charts)
        {
          addUrl("/charts/"+chart.Id);
        }
        xml.WriteEndElement();
      }
    }
  }
}
