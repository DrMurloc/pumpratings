﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.WebApplication.Security;

namespace PumpRatings.WebApplication.Controllers
{
  [Route("[controller]")]
  public class AccountController : Controller
  {
    [HttpGet("Logout")]
    public async Task<IActionResult> Logout()
    {
      await HttpContext.SignOutAsync();
      return Redirect("/#/profile");
    }
    [HttpGet("Login/{providerScheme}")]
    public IActionResult ExternalLogin([FromRoute] string providerScheme, [FromQuery] string returnUrl)
    {
      if (string.IsNullOrWhiteSpace(returnUrl))
      {
        returnUrl = "/";
      }
      var props = new AuthenticationProperties
      {
        RedirectUri = Url.Action("ExternalCallback",new {scheme=providerScheme}),
        Items =
        {
          { "scheme", providerScheme},
          { "returnUrl", returnUrl }
        }
      };
      return new ChallengeResult(providerScheme, props);
    }

    [HttpGet("ExternalCallback")]
    public async Task<IActionResult> ExternalCallback([FromServices] ICommandHandler<LoginCommand> handler,
      [FromQuery] string scheme)
    {
      try
      {
        var authenticateResult = await HttpContext.AuthenticateAsync(scheme);
        var providerScheme = authenticateResult.Ticket.Properties.Items["scheme"];
        authenticateResult.Ticket.Properties.Items.TryGetValue("returnUrl", out var returnUrl);
        returnUrl = Url.Action("Home", "Home") + "#" + returnUrl;
        var externalClaims = authenticateResult.Principal.Claims;
        await handler.Handle(new LoginCommand
        {
          Scheme = providerScheme,
          ExternalClaims = externalClaims
        });
        return Redirect(returnUrl);
      }
      catch (Exception)
      {
        return RedirectToAction("Home", "Home");

      }
    }
  }
}
