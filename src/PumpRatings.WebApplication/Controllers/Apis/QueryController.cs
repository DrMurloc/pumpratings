﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.DataLoader;
using GraphQL.Types;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PumpRatings.Abstractions.Security;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Services;
using PumpRatings.WebApplication.Security;
using PumpRatings.WebApplication.Security.Attributes;

namespace PumpRatings.WebApplication.Controllers.Apis
{
  [Anonymous]
  [Route("[controller]")]
  public sealed class QueryController : Controller
  {
    private readonly IDocumentExecuter _documentExecuter;
    private readonly ISchema _schema;
    private readonly ILogger<QueryController> _logger;
    private readonly DataLoaderDocumentListener _listener;
    private readonly IMetricsService _metrics;
    private readonly IHostingEnvironment _environment;
    private readonly IUserProfileAccessor _user;
    public QueryController(ISchema schema, IDocumentExecuter documentExecuter,
      ILogger<QueryController> logger,
      IServiceProvider services,
      IHttpContextAccessor httpContext,
      IMetricsService metrics,
      IHostingEnvironment environment,
      IUserProfileAccessor user)
    {
      _environment = environment;
      _metrics = metrics;
      _schema = schema;
      _documentExecuter = documentExecuter;
      _logger = logger;
      _listener = services.GetRequiredService<DataLoaderDocumentListener>();
      _user = user;
    }

    [HttpPost]
    public async Task<IActionResult> Post([FromBody] GraphQlQuery query)
    {

      if (query == null)
      {
        throw new ArgumentNullException(nameof(query));
      }
      var inputs = query.Variables.ToInputs();
      var executionOptions = new ExecutionOptions
      {
        Schema = _schema,
        Query = query.Query,
        Inputs = inputs,
        Listeners = { _listener }
      };
      ExecutionResult result;
      using (var scope = _metrics.BeginQueryScope(query.Query, query.Name))
      {
        var profile = _user.UserProfile;
        scope.UserId = profile.Id;
        scope.UserName = profile.Name;
        result = await _documentExecuter.ExecuteAsync(executionOptions).ConfigureAwait(false);
        if (!(result.Errors?.Count > 0))
        {
          var resultString = JsonConvert.SerializeObject(result);
          scope.ResultLength = resultString.Length;
          return Content(resultString, "application/json");
        }
      }

      foreach (var t in result.Errors)
      {
        _logger.LogError(t, "There was an exception while querying graph ql", new
        {
          query.Name,
          t.Message,
          t.StackTrace,
          query.Query,
          query.Variables
        });
      }

      result.ExposeExceptions = _environment.IsDevelopment();
      return BadRequest(result);


    }

    [AllowAnonymous]
    [HttpGet("loginProviders")]
    public async Task<IActionResult> GetLoginProviders(
      [FromServices] ILoginProviderService loginProviderServices)
    {
      return Json(await loginProviderServices.SearchForLoginProviders());
    }


    public sealed class GraphQlQuery
    {
      public string Name { get; set; }
      public string Query { get; set; }
      public JObject Variables { get; set; }
    }
  }
}
