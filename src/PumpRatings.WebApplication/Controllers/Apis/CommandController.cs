﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Command.Abstractions;
using PumpRatings.Command.Abstractions.Commands;
using PumpRatings.Command.Abstractions.Exceptions;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.WebApplication.Security.Attributes;

namespace PumpRatings.WebApplication.Controllers.Apis
{
  [Route("[controller]")]
  public sealed class CommandController : Controller
  {
    private readonly IServiceResolver _services;
    private readonly IMetricsService _metrics;
    private readonly ILogger<CommandController> _logger;
    public CommandController(IServiceResolver services,
      IMetricsService metrics,
      ILogger<CommandController> logger)
    {
      _services = services;
      _metrics = metrics;
      _logger = logger;
    }

    private async Task<IActionResult> LaunchCommand<TCommand>(TCommand command)
    {
      using (var scope =
        _metrics.BeginQueryScope(JsonConvert.SerializeObject(command, Formatting.Indented), typeof(TCommand).Name))
      {
        try
        {
          await _services.GetService<ICommandHandler<TCommand>>().Handle(command);
          return Ok();
        }
        catch (BadCommandArgumentsException e)
        {
          _logger.LogWarning(e,"Bad command format",new {e.Message,e.StackTrace,command});
          return BadRequest();
        }
      }
    }

    private async Task<IActionResult> LaunchCommand<TCommand, TResult>(TCommand command,
      Func<TResult, object> resultToJson)
    {
      using (var scope =
        _metrics.BeginQueryScope(JsonConvert.SerializeObject(command, Formatting.Indented), nameof(TCommand)))
      {
        try
        {
          var result = await _services.GetService<ICommandHandler<TCommand,TResult>>().Handle(command);
          return Json(resultToJson(result));
        }
        catch (BadCommandArgumentsException e)
        {
          _logger.LogWarning(e, "Bad command format", new { e.Message, e.StackTrace, command });
          return BadRequest();
        }
      }

    }

    [SuperKamiGuru]
    [HttpPost("[action]")]
    public Task<IActionResult> ClearCache()
      => LaunchCommand(new ClearCacheCommand());

    [Admin]
    [HttpPost("[action]")]
    public Task<IActionResult> CreateChart([FromBody] CreateChartCommand command)
      => LaunchCommand<CreateChartCommand,string>(command,chartId => new {chartId});

    [User]
    [HttpPost("[action]")]
    public Task<IActionResult> CreateComment([FromBody] CreateCommentCommand command)
      => LaunchCommand(command);

    [Admin]
    [HttpPost("[action]")]
    public Task<IActionResult> CreateMix([FromBody] CreateMixCommand command)
      => LaunchCommand<CreateMixCommand, string>(command, mixId => new {mixId});

    [SuperKamiGuru]
    [HttpPost("[action]")]
    public Task<IActionResult> CreateReleaseNotes([FromBody] CreateReleaseNotesCommand command)
      => LaunchCommand(command);

    [Admin]
    [HttpPost("[action]")]
    public Task<IActionResult> CreateSong([FromBody] CreateSongCommand command)
      => LaunchCommand<CreateSongCommand, string>(command, songId => new {songId});

    [Admin]
    [HttpPost("[action]")]
    public Task<IActionResult> CreateTag([FromBody] CreateTagCommand command)
      => LaunchCommand(command);

    [User]
    [HttpPost("[action]")]
    public Task<IActionResult> DeleteComment([FromBody] DeleteCommentCommand command)
      => LaunchCommand(command);

    [Admin]
    [HttpPost("[action]")]
    public Task<IActionResult> UpdateChart([FromBody] UpdateChartCommand command)
      => LaunchCommand(command);

    [User]
    [HttpPost("[action]")]
    public Task<IActionResult> UpdateProfile([FromBody] UpdateProfileCommand command)
      => LaunchCommand(command);

    [User]
    [HttpPost("[action]")]
    public Task<IActionResult> UpdateRating([FromBody] UpdateRatingCommand command)
      => LaunchCommand<UpdateRatingCommand,double>(command,newAverageRating=>new {newAverageRating});

    [Admin]
    [HttpPost("[action]")]
    public Task<IActionResult> UpdateChartTags([FromBody] UpdateChartTagsCommand command)
      => LaunchCommand(command);

    [Admin]
    [HttpPost("[action]")]
    public Task<IActionResult> UpdateUserRole([FromBody] UpdateUserRoleCommand command)
      => LaunchCommand(command);

    [User]
    [HttpPost("[action]")]
    public Task<IActionResult> AddChartToList([FromBody] AddChartToListCommand command)
      => LaunchCommand(command);

    [User]
    [HttpPost("[action]")]
    public Task<IActionResult> RemoveChartFromList([FromBody] RemoveChartFromListCommand command)
      => LaunchCommand(command);
  }
}
