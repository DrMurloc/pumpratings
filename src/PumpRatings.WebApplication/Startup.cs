﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PumpRatings.Command;
using PumpRatings.Event;
using PumpRatings.Metrics;
using PumpRatings.Query.NetCore;
using PumpRatings.WebApplication.Configuration;
using PumpRatings.WebApplication.Infrastructure;
using PumpRatings.WebApplication.Security;
using RedBear.LogDNA.Extensions.Logging;

namespace PumpRatings.WebApplication
{
  public class Startup
  {
    private readonly ApplicationConfiguration _config;
    private readonly IHostingEnvironment _environment;
    public Startup(IHostingEnvironment env)
    {
      _environment = env;
      var configBuilder = new ConfigurationBuilder()
        .SetBasePath(env.ContentRootPath)
        .AddJsonFile("appsettings.json", false)
        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
        .AddEnvironmentVariables();
      if (env.IsDevelopment())
      {
        configBuilder.AddUserSecrets<Startup>();
      }
      _config = configBuilder.Build()
        .Get<ApplicationConfiguration>();
    }
    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services
        .AddMvc()
        .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
        .Services
        .AddPumpSecurity(_config)
        .AddPumpRatingEvents()
        .AddPumpRatingsGraphQl()
        .AddPumpRatingsCommand()
        .AddPumpMetrics(builder => { builder.AddLogger(); })
        .AddInfrastructure(_config)
        .AddHttpContextAccessor()
        .AddLogging(options =>
        {
          options.AddFilter((context, logLevel) =>
          {
            if (context.StartsWith("Microsoft"))
            {
              return logLevel >= LogLevel.Warning;
            }
            return logLevel >= LogLevel.Information;
          });
          if (_environment.IsDevelopment())
          {
            options.AddConsole();
          }
          if (!string.IsNullOrWhiteSpace(_config.LogDna.IngestionKey))
          {
            options.AddLogDNA(new LogDNAOptions(_config.LogDna.IngestionKey, LogLevel.Information)
            {
              HostName= _config.LogDna.HostName
            });
          }
        });
    }
    
    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      app.UseStaticFiles()
        .UseMvc()
        .AddPumpRatingsGraphiQl()
        .UseAuthentication()
        .UseCookiePolicy(new CookiePolicyOptions()
        {
          HttpOnly = HttpOnlyPolicy.None,
          Secure = CookieSecurePolicy.None,
          MinimumSameSitePolicy = SameSiteMode.None
        });
    }
  }
}
