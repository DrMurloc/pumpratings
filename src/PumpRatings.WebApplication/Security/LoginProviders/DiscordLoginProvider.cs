﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using PumpRatings.Abstractions.Security;

namespace PumpRatings.WebApplication.Security.LoginProviders
{
  internal class DiscordLoginProvider : ILoginProvider
  {
    public static string ChallengeScheme = "Discord";
    public string Scheme => ChallengeScheme;
    public string DisplayName => Scheme;
    public string Icon => "fab fa-discord";
    public string GetExternalUserId(IEnumerable<Claim> externalClaims)
    {
      return externalClaims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value;
    }
  }
}
