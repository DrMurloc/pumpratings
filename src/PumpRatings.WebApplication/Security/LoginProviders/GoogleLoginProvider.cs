﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using PumpRatings.Abstractions.Security;

namespace PumpRatings.WebApplication.Security.LoginProviders
{
  internal sealed class GoogleLoginProvider : ILoginProvider
  {
    public static string ChallengeScheme = "Google";
    public string Scheme => ChallengeScheme;
    public string DisplayName => Scheme;
    public string Icon => "fab fa-google";
    public string GetExternalUserId(IEnumerable<Claim> externalClaims)
    {
      return externalClaims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value;
    }
  }
}
