﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using PumpRatings.Abstractions.Security;

namespace PumpRatings.WebApplication.Security.LoginProviders
{
  internal sealed class TwitterLoginProvider : ILoginProvider
  {
    public static string ChallengeScheme = "Twitter";
    public string Scheme => ChallengeScheme;
    public string DisplayName => Scheme;
    public string Icon => "images/providers/twitter_icon.png";
    public string GetExternalUserId(IEnumerable<Claim> externalClaims)
    {
      return externalClaims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value;
    }
  }
}
