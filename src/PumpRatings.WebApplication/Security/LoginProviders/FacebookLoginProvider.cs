﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using PumpRatings.Abstractions.Security;

namespace PumpRatings.WebApplication.Security.LoginProviders
{
  internal class FacebookLoginProvider : ILoginProvider
  {
    public static string ChallengeScheme = "Facebook";
    public string Scheme => ChallengeScheme;
    public string DisplayName => Scheme;
    public string Icon => "fab fa-facebook";
    public string GetExternalUserId(IEnumerable<Claim> externalClaims)
    {
      return externalClaims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value;
    }
  }
}
