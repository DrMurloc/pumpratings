﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using PumpRatings.Abstractions.Security;

namespace PumpRatings.WebApplication.Security.Attributes
{
  public sealed class UserAttribute : AuthorizeAttribute
  {
    public UserAttribute()
    {
      Policy = PolicyName;
      AuthenticationSchemes = PumpAuthenticationSchemes.Internal;
    }
    public static string PolicyName => Role.User;

    public static Func<AuthorizationHandlerContext,bool> Authorize = ctx =>AdminAttribute.Authorize(ctx) || ctx.User.FindFirst(ClaimTypes.Role)?.Value == Role.User;
  }
}
