﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using PumpRatings.Abstractions.Security;

namespace PumpRatings.WebApplication.Security.Attributes
{
  public sealed class AnonymousAttribute : AuthorizeAttribute
  {
    public AnonymousAttribute()
    {
      Policy = PolicyName;
      AuthenticationSchemes = PumpAuthenticationSchemes.Internal;
    }
    public static string PolicyName => Role.Anonymous;

    public static Func<AuthorizationHandlerContext, bool> Authorize = ctx => true;

  }
}
