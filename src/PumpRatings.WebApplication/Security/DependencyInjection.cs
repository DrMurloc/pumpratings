﻿using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using PumpRatings.Abstractions.Security;
using PumpRatings.WebApplication.Configuration;
using PumpRatings.WebApplication.Security.Attributes;
using PumpRatings.WebApplication.Security.LoginProviders;

namespace PumpRatings.WebApplication.Security
{
  public static class DependencyInjection
  {
    public static IServiceCollection AddPumpSecurity(this IServiceCollection builder, ApplicationConfiguration config)
    {
      return builder
        .AddAuthorization(options =>
        {
          options.AddPolicy(UserAttribute.PolicyName, policy => policy.RequireAssertion(UserAttribute.Authorize));
          options.AddPolicy(AdminAttribute.PolicyName, policy => policy.RequireAssertion(AdminAttribute.Authorize));
          options.AddPolicy(SuperKamiGuruAttribute.PolicyName,
            policy => policy.RequireAssertion(SuperKamiGuruAttribute.Authorize));
          options.AddPolicy(AnonymousAttribute.PolicyName, policy => policy.RequireAssertion(AnonymousAttribute.Authorize));

        })
        .AddAuthentication(options =>
        {
          options.DefaultAuthenticateScheme = PumpAuthenticationSchemes.Internal;
          options.DefaultSignInScheme = PumpAuthenticationSchemes.Internal;
          options.DefaultChallengeScheme = GoogleLoginProvider.ChallengeScheme;
        })
        .AddCookie(PumpAuthenticationSchemes.Internal, options =>
        {
          options.ExpireTimeSpan = TimeSpan.FromDays(30);
        })
        .AddProviders(config.Authentication)
        .Services;
    }

    private static AuthenticationBuilder AddProviders(this AuthenticationBuilder builder,
      AuthenticationConfiguration config)
    {
      if (!string.IsNullOrEmpty(config.Google.ClientSecret) && !string.IsNullOrEmpty(config.Google.ClientId))
      {
        builder
          .AddGoogle(options =>
          {
            options.ClientSecret = config.Google.ClientSecret;
            options.ClientId = config.Google.ClientId;
          }).Services.AddTransient<ILoginProvider, GoogleLoginProvider>();
      }

      if (!string.IsNullOrWhiteSpace(config.Discord.ClientId) && !string.IsNullOrEmpty(config.Discord.ClientSecret))
      {
        builder.AddDiscord(options =>
        {
          options.ClientId = config.Discord.ClientId;
          options.ClientSecret = config.Discord.ClientSecret;
        }).Services.AddTransient<ILoginProvider, DiscordLoginProvider>();
      }

      if (!string.IsNullOrEmpty(config.Twitter.ConsumerKey) && !string.IsNullOrEmpty(config.Twitter.ConsumerSecret))
      {
        builder.AddTwitter(options =>
        {
          options.ConsumerKey = config.Twitter.ConsumerKey;
          options.ConsumerSecret = config.Twitter.ConsumerSecret;
        }).Services.AddTransient<ILoginProvider, TwitterLoginProvider>();
      }

      if (!string.IsNullOrEmpty(config.Facebook.AppId) && !string.IsNullOrEmpty(config.Facebook.AppSecret))
      {
        builder.AddFacebook(options =>
        {
          options.AppSecret = config.Facebook.AppSecret;
          options.AppId = config.Facebook.AppId;
        }).Services.AddTransient<ILoginProvider, FacebookLoginProvider>();
      }
      return builder;
    }
  }
}
