﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using PumpRatings.Metrics.Abstractions.Scopes;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Metrics.Services
{
  internal sealed class MetricsService : IMetricsService
  {
    private readonly IHttpContextAccessor _contextAccessor;
    private MetricsRequestScope Request =>
      (MetricsRequestScope)_contextAccessor.HttpContext.RequestServices.GetService(typeof(MetricsRequestScope));
    public MetricsService(IHttpContextAccessor contextAccessor)
    {
      _contextAccessor = contextAccessor;
    }
    public MetricsRepositoryScope<T> BeginRepositoryScope<T>(string actionName)
    {
      return Request.AddRepositoryScope(new MetricsRepositoryScope<T>(actionName));
    }

    public MetricsQueryScope BeginQueryScope(string query, string queryName = "")
    {
      return Request.AddQueryScope(new MetricsQueryScope(query, queryName));
    }
  }
}
