﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PumpRatings.Metrics.Abstractions.Scopes;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Metrics.Services
{
  internal sealed class LoggingRecorder : IMetricsRecorder
  {
    private readonly ILogger<IMetricsRecorder> _logger;
    public LoggingRecorder(ILogger<IMetricsRecorder> logger)
    {
      _logger = logger;
    }
    public Task Record(MetricsRequestScope request)
    {
      return Task.Run(() =>
      {
        var queries = request.QueryScopes.ToArray();
        var repositories = request.RepositoryScopes.ToArray();
        var totalQueryTime = queries.Sum(query => query.Duration);
        _logger.LogInformation($"Request Completed - {string.Join(",",queries.Select(query=>query.QueryName??"Nameless"))} ({totalQueryTime} milliseconds, {repositories.Length} repository calls)",
          new
          {
            QueryCount = queries.Length,
            RepositoryCallCount = repositories.Length,
            TotalQueryTime = queries.Sum(query => query.Duration),
            TotalRepositoryCallTime = repositories.Sum(repository => repository.Duration),
            Queries = queries,
            RepositoryCalls = repositories
          });
      });
    }
  }
}
