﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using PumpRatings.Metrics.Abstractions.Scopes;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Metrics.Services;

namespace PumpRatings.Metrics
{
  public static class ServiceCollectionExtensions
  {
    public static IServiceCollection AddPumpMetrics(this IServiceCollection builder,
      Action<MetricsBuilder> configure)
    {
      var metricsBuilder = new MetricsBuilder(builder);
      configure(metricsBuilder);
      return builder.AddScoped<MetricsRequestScope>()
        .AddTransient<IMetricsService, MetricsService>();
    }
  }
}
