﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Metrics.Services;

namespace PumpRatings.Metrics
{
  public sealed class MetricsBuilder
  {
    private readonly IServiceCollection _serviceCollection;
    public MetricsBuilder(IServiceCollection serviceCollection)
    {
      _serviceCollection = serviceCollection;
    }

    public MetricsBuilder AddLogger()
    {
      _serviceCollection.AddScoped<IMetricsRecorder, LoggingRecorder>();
      return this;
    }
  }
}
