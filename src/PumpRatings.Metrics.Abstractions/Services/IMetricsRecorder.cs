﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Metrics.Abstractions.Scopes;

namespace PumpRatings.Metrics.Abstractions.Services
{
  public interface IMetricsRecorder
  {
    Task Record(MetricsRequestScope request);
  }
}
