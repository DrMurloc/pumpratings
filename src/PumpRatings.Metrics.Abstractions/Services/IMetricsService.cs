﻿using System;
using System.Collections.Generic;
using System.Text;
using PumpRatings.Metrics.Abstractions.Scopes;

namespace PumpRatings.Metrics.Abstractions.Services
{
  public interface IMetricsService
  {
    MetricsRepositoryScope<T> BeginRepositoryScope<T>(string actionName);
    MetricsQueryScope BeginQueryScope(string query, string queryName = "");
  }
}
