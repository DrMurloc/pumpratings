﻿using System;
using System.Collections.Generic;
using System.Text;
using PumpRatings.Metrics.Abstractions.Services;

namespace PumpRatings.Metrics.Abstractions.Scopes
{
  public sealed class MetricsRequestScope : IDisposable
  {
    private readonly IEnumerable<IMetricsRecorder> _recorders;
    public MetricsRequestScope(IEnumerable<IMetricsRecorder> recorders)
    {
      _recorders = recorders;
      _repositoryScopes = new List<MetricsRepositoryScope>();
      _queryScopes = new List<MetricsQueryScope>();
    }
    private readonly IList<MetricsRepositoryScope> _repositoryScopes;
    public IEnumerable<MetricsRepositoryScope> RepositoryScopes => _repositoryScopes;
    private readonly IList<MetricsQueryScope> _queryScopes;
    public IEnumerable<MetricsQueryScope> QueryScopes => _queryScopes;


    public MetricsRepositoryScope<T> AddRepositoryScope<T>(MetricsRepositoryScope<T> scope)
    {
      _repositoryScopes.Add(scope);
      return scope;
    }

    public MetricsQueryScope AddQueryScope(MetricsQueryScope scope)
    {
      _queryScopes.Add(scope);
      return scope;
    }

    public void Dispose()
    {
      foreach (var recorder in _recorders)
      {
        recorder.Record(this);
      }
    }
  }
}
