﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Metrics.Abstractions.Scopes
{
  public abstract class MetricsRepositoryScope : IDisposable
  {
    public string Context { get; }
    public string ActionName { get; }
    public double Duration { get; private set; }
    public int? ResultCount { get; set; }
    private readonly DateTimeOffset _startTime;
    protected MetricsRepositoryScope(string context, string actionName)
    {
      Context = context;
      _startTime = DateTimeOffset.Now;
      ActionName = actionName;
    }
    public void Dispose()
    {
      Duration = (DateTimeOffset.Now - _startTime).TotalMilliseconds;
    }
  }

  public sealed class MetricsRepositoryScope<TContext> : MetricsRepositoryScope
  {
    public MetricsRepositoryScope(string actionName) : base(nameof(TContext), actionName)
    {

    }
  }
}
