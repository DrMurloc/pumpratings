﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Metrics.Abstractions.Scopes
{
  public sealed class MetricsQueryScope : IDisposable
  {
    public string Query { get; }
    public string QueryName { get; }
    public double Duration { get; private set; }
    public int? ResultLength { get; set; }
    private readonly DateTimeOffset _startTime;
    public string UserId { get; set; }
    public string UserName { get; set; }
    public MetricsQueryScope(string query, string queryName = "")
    {
      Query = query;
      QueryName = queryName;
      _startTime = DateTimeOffset.Now;
    }
    public void Dispose()
    {
      Duration = (DateTimeOffset.Now - _startTime).TotalMilliseconds;
    }
  }
}
