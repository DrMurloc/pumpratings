﻿using System;
using System.Linq;
using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.GraphTypes;

namespace PumpRatings.Query.QuerySchema
{
  public sealed class EntryPoints : PumpObjectGraphType
  {
    public EntryPoints(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      AddMixEntryPoints();
      AddSongEntryPoints();
      AddChartEntryPoints();
      AddReleaseNotesEntryPoints();
      AddTrackingEntryPoints();
      AddTagEntryPoints();
      AddUserEntryPoints();
      AddChartTypeEntryPoints();
      AddSongTypeEntryPoints();
      AddRoleEntryPoints();
      AddLoginProviderEntryPoints();
    }

    private void AddLoginProviderEntryPoints()
    {
      FieldAsync<ListGraphType<LoginProviderGraphType>>("loginProviders",
        resolve: async context => await LoginProviderService.SearchForLoginProviders());
    }
    private void AddMixEntryPoints()
    {
      FieldAsync<MixGraphType>("mix",
        arguments: new QueryArguments(new QueryArgument<StringGraphType> {Name = "id", DefaultValue = null}),
        resolve: async context =>
        {
          var id = context.GetArgument<string>("id");
          if (id == null)
          {
            throw new ArgumentNullException(nameof(id));
          }
          return await MixService.GetMixById(id);
        });
      FieldAsync<ListGraphType<MixGraphType>>("mixes",
        resolve: async context => await MixService.SearchForMixes());
    }

    private void AddSongEntryPoints()
    {
      FieldAsync<SongGraphType>("song",
        arguments: new QueryArguments(new QueryArgument<StringGraphType> {Name = "id", DefaultValue = null}),
        resolve: async context =>
        {
          var id = context.GetArgument<string>("id");
          if (id == null)
          {
            throw new ArgumentNullException(nameof(id));
          }

          return await SongService.GetSongById(id);
        });
      FieldAsync<ListGraphType<SongGraphType>>("songs",
        resolve: async context => await SongService.SearchForSongs());
    }

    private void AddChartEntryPoints()
    {
      FieldAsync<ChartGraphType>("chart",
        arguments: new QueryArguments(new QueryArgument<StringGraphType> {Name = "id"}),
        resolve: async context =>
        {
          var id = context.GetArgument<string>("id");
          if (id == null)
          {
            throw new ArgumentNullException(nameof(id));
          }

          return await ChartService.GetChartById(id);
        });
      FieldAsync<ListGraphType<ChartGraphType>>("charts",
        resolve: async context => await ChartService.SearchForCharts());
    }

    private void AddReleaseNotesEntryPoints()
    {
      FieldAsync<ListGraphType<ReleaseNotesGraphType>>("releaseNotes",
        arguments: new QueryArguments(
          new QueryArgument<IntGraphType> {Name = "page", DefaultValue = 1},
          new QueryArgument<IntGraphType> {Name = "count", DefaultValue = 5}
        ),
        resolve: async context => await ReleaseNotesService.SearchForReleaseNotes(
          page:context.GetArgument<int>("page"),
          count:context.GetArgument<int>("count")));
    }

    private void AddTrackingEntryPoints()
    {
      FieldAsync<ListGraphType<TrackingGraphType>>("tracking",
        arguments: new QueryArguments(
          new QueryArgument<IntGraphType> {Name = "page", DefaultValue = 1},
          new QueryArgument<IntGraphType> {Name = "count", DefaultValue = 5}
        ),
        resolve: async context => await TrackingService.SearchForTracking(
          page: context.GetArgument<int>("page"),
          count: context.GetArgument<int>("count")));
    }

    public void AddTagEntryPoints()
    {
      FieldAsync<TagGraphType>("tag",
        arguments: new QueryArguments(
          new QueryArgument<StringGraphType> {Name = "name", DefaultValue = null}),
        resolve: async context =>
        {
          var name = context.GetArgument<string>("name");
          if (name == null)
          {
            throw new ArgumentNullException(nameof(name));
          }
          return await TagService.GetTagByName(name);
        });
      FieldAsync<ListGraphType<TagGraphType>>("tags",
        resolve: async context => await TagService.SearchForTags());
    }

    public void AddUserEntryPoints()
    {
      FieldAsync<UserGraphType>("user",
        arguments: new QueryArguments(
          new QueryArgument<StringGraphType> {Name = "id", DefaultValue = null}),
        resolve: async context =>
        {
          var id = context.GetArgument<string>("id");
          if (id == null)
          {
            throw new ArgumentNullException(nameof(id));
          }

          return await UserService.GetUserById(id);
        });
      FieldAsync<UserGraphType>("me",
        resolve: async context => await UserService.GetMe());
      FieldAsync<ListGraphType<UserGraphType>>("users",
        resolve: async context => await UserService.SearchForUsers());
    }

    public void AddChartTypeEntryPoints()
    {
      FieldAsync<ChartTypeGraphType>("chartType",
        arguments: new QueryArguments(
          new QueryArgument<StringGraphType> {Name = "name", DefaultValue = null}),
        resolve: async context =>
        {
          var name = context.GetArgument<string>("name");
          if (name == null)
          {
            throw new ArgumentNullException(nameof(name));
          }

          return await ChartTypeService.GetChartTypeByName(name);
        });
      FieldAsync<ListGraphType<ChartTypeGraphType>>("chartTypes",
        resolve: async context => await ChartTypeService.SearchForChartTypes());
      FieldAsync<ListGraphType<StringGraphType>>("chartTypeNames",
        resolve: async context => (await ChartTypeService.SearchForChartTypes()).Select(chartType => chartType.Name));
    }
    public void AddSongTypeEntryPoints()
    {
      FieldAsync<SongTypeGraphType>("songType",
        arguments: new QueryArguments(
          new QueryArgument<StringGraphType> { Name = "name", DefaultValue = null }),
        resolve: async context =>
        {
          var name = context.GetArgument<string>("name");
          if (name == null)
          {
            throw new ArgumentNullException(nameof(name));
          }

          return await SongTypeService.GetSongTypeByName(name);
        });
      FieldAsync<ListGraphType<SongTypeGraphType>>("songTypes",
        resolve: async context => await SongTypeService.SearchForSongTypes());
      FieldAsync<ListGraphType<StringGraphType>>("songTypeNames",
        resolve: async context => (await SongTypeService.SearchForSongTypes()).Select(songType => songType.Name));
    }

    public void AddRoleEntryPoints()
    {
      FieldAsync<RoleGraphType>("role",
        arguments: new QueryArguments(
          new QueryArgument<StringGraphType> {Name = "name", DefaultValue  =null}),
        resolve: async context =>
        {
          var name = context.GetArgument<string>("name");
          if (name == null)
          {
            throw new ArgumentNullException(nameof(name));
          }

          return await RoleService.GetRoleByName(name);
        });
      FieldAsync<ListGraphType<RoleGraphType>>("roles",
        resolve: async context => await RoleService.SearchForRoles());
    }
  }
}
