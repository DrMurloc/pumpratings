﻿using GraphQL;
using GraphQL.Types;
namespace PumpRatings.Query.QuerySchema
{
  public sealed class PumpItUpSchema : Schema
  {
    public PumpItUpSchema(IDependencyResolver resolver) : base(resolver)
    {
      Query = resolver.Resolve<EntryPoints>();
    }
  }
}
