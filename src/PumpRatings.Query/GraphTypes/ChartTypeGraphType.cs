﻿using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class ChartTypeGraphType : PumpObjectGraphType<ChartTypeNode>
  {
    public ChartTypeGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(chartType => chartType.Name);
      FieldAsync<ListGraphType<ChartGraphType>>("charts",
        resolve: async context => await ChartService.GetChartsByTypeName(context.Source.Name));
    }
  }
}
