﻿using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class UserGraphType : PumpObjectGraphType<UserNode>
  {
    public UserGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(user => user.Id);
      Field(user => user.Name);
      Field(user => user.RoleName);
      Field(user => user.DarkTheme);
      Field(user => user.CurrentRoleName);
      FieldAsync<ListGraphType<ChartListGraphType>>("lists",
        resolve: async context => await ListService.GetListsByUserId(context.Source.Id));
      FieldAsync<ListGraphType<CommentGraphType>>("comments",
        resolve: async context => await CommentService.GetCommentsByUserId(context.Source.Id));
      FieldAsync<RoleGraphType>("role",
        resolve: async context => await RoleService.GetRoleByName(context.Source.RoleName));
      FieldAsync<RoleGraphType>("currentRole",
        resolve: async context => await RoleService.GetRoleByName(context.Source.CurrentRoleName));
      FieldAsync<ListGraphType<RatingGraphType>>("ratings",
        resolve: async context => await RatingService.GetRatingsByUserId(context.Source.Id));
      FieldAsync<ListGraphType<ReleaseNotesGraphType>>("releaseNotes",
        resolve: async context => await ReleaseNotesService.GetReleaseNotesByUserId(context.Source.Id));
      FieldAsync<ListGraphType<TrackingGraphType>>("tracking",
        resolve: async context => await TrackingService.GetTrackingByUser(context.Source.Id));
    }
  }
}
