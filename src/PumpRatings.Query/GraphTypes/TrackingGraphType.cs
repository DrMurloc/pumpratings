﻿using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class TrackingGraphType : PumpObjectGraphType<TrackingNode>
  {
    public TrackingGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(tracking => tracking.Id);
      Field(tracking => tracking.Change);
      Field(tracking => tracking.ChangeDate);
      Field(tracking => tracking.Summary);
      Field(tracking => tracking.UserId);
      FieldAsync<UserGraphType>("user",
        resolve: async context => await UserService.GetUserById(context.Source.UserId));
    }
  }
}
