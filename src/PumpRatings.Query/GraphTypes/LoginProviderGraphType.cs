﻿using System;
using System.Collections.Generic;
using System.Text;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class LoginProviderGraphType : PumpObjectGraphType<LoginProviderNode>
  {
    public LoginProviderGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(provider => provider.Scheme);
      Field(provider => provider.DisplayName);
      Field(provider => provider.Icon);
    }
  }
}
