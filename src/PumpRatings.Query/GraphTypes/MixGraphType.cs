﻿using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;
namespace PumpRatings.Query.GraphTypes
{
  public sealed class MixGraphType : PumpObjectGraphType<MixNode>
  {
    public MixGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(mix => mix.Id);
      Field(mix => mix.DisplayOrder);
      Field(mix => mix.Image);
      Field(mix => mix.Name);
      FieldAsync<ListGraphType<SongGraphType>>("songs",
        resolve: async context => await SongService.GetSongsByMixId(context.Source.Id));
    }
  }
}
