﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class ChartListGraphType : PumpObjectGraphType<ListNode>
  {
    public ChartListGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field("id", list => list.Id);
      Field("name", list => list.Name);
      Field("userId", list => list.UserId);
      Field("isFavorites", list => list.IsFavorites);
      FieldAsync<UserGraphType>("user",
        resolve: async context => await UserService.GetUserById(context.Source.UserId));
      FieldAsync<ListGraphType<ChartGraphType>>("charts",
        resolve: async context => await ChartService.GetChartsByListId(context.Source.Id));
      FieldAsync<ListGraphType<StringGraphType>>("chartIds",
        resolve: async context => (await ChartService.GetChartsByListId(context.Source.Id)).Select(chart => chart.Id));
    }
  }
}
