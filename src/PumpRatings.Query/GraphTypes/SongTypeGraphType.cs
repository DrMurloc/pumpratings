﻿using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class SongTypeGraphType : PumpObjectGraphType<SongGraphType>
  {
    public SongTypeGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(type => type.Name);
      FieldAsync<ListGraphType<SongGraphType>>("songs",
        resolve: async context => await SongService.GetSongsByTypeName(context.Source.Name));
    }
  }
}
