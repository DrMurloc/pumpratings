﻿
using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class SongGraphType : PumpObjectGraphType<SongNode>
  {
    public SongGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(song => song.Id);
      Field(song => song.Name);
      Field(song => song.Artist);
      Field(song => song.Bpm);
      Field(song => song.Image);
      Field(song => song.TypeName);
      Field(song => song.MixId);
      FieldAsync<MixGraphType>("mix",
        resolve: async context => await MixService.GetMixById(context.Source.MixId));
      FieldAsync<ListGraphType<ChartGraphType>>("charts",
        resolve: async context => await ChartService.GetChartsBySongId(context.Source.Id));
    }
  }
}
