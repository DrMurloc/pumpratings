﻿using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class CommentGraphType : PumpObjectGraphType<CommentNode>
  {
    public CommentGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(comment => comment.ChartId);
      Field(comment => comment.UserId);
      Field(comment => comment.Message);
      Field(comment => comment.CreatedDate);
      FieldAsync<ChartGraphType>("chart",
        resolve: async context => await ChartService.GetChartById(context.Source.ChartId));
      FieldAsync<UserGraphType>("user",
        resolve: async context => await UserService.GetUserById(context.Source.UserId));
    }
  }
}
