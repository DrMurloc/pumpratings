﻿using System;
using System.Collections.Generic;
using System.Text;
using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.GraphTypes
{
  public abstract class PumpObjectGraphType : ObjectGraphType
  {
    private readonly IServiceResolver _serviceResolver;
    protected IChartService ChartService => _serviceResolver.GetService<IChartService>();
    protected IChartTypeService ChartTypeService => _serviceResolver.GetService<IChartTypeService>();
    protected ICommentService CommentService => _serviceResolver.GetService<ICommentService>();
    protected IMixService MixService => _serviceResolver.GetService<IMixService>();
    protected IRatingService RatingService => _serviceResolver.GetService<IRatingService>();
    protected IReleaseNotesService ReleaseNotesService => _serviceResolver.GetService<IReleaseNotesService>();
    protected IRoleService RoleService => _serviceResolver.GetService<IRoleService>();
    protected ISongService SongService => _serviceResolver.GetService<ISongService>();
    protected ISongTypeService SongTypeService => _serviceResolver.GetService<ISongTypeService>();
    protected ITagService TagService => _serviceResolver.GetService<ITagService>();
    protected ITrackingService TrackingService => _serviceResolver.GetService<ITrackingService>();
    protected IUserService UserService => _serviceResolver.GetService<IUserService>();
    protected ILoginProviderService LoginProviderService => _serviceResolver.GetService<ILoginProviderService>();
    protected PumpObjectGraphType(IServiceResolver serviceResolver)
    {
      _serviceResolver = serviceResolver;

    }
  }
  public abstract class PumpObjectGraphType<T> : ObjectGraphType<T>
  {
    private readonly IServiceResolver _serviceResolver;
    protected IChartService ChartService => _serviceResolver.GetService<IChartService>();
    protected IChartTypeService ChartTypeService => _serviceResolver.GetService<IChartTypeService>();
    protected ICommentService CommentService => _serviceResolver.GetService<ICommentService>();
    protected IMixService MixService => _serviceResolver.GetService<IMixService>();
    protected IRatingService RatingService => _serviceResolver.GetService<IRatingService>();
    protected IReleaseNotesService ReleaseNotesService => _serviceResolver.GetService<IReleaseNotesService>();
    protected IRoleService RoleService => _serviceResolver.GetService<IRoleService>();
    protected ISongService SongService => _serviceResolver.GetService<ISongService>();
    protected ISongTypeService SongTypeService => _serviceResolver.GetService<ISongTypeService>();
    protected ITagService TagService => _serviceResolver.GetService<ITagService>();
    protected ITrackingService TrackingService => _serviceResolver.GetService<ITrackingService>();
    protected IUserService UserService => _serviceResolver.GetService<IUserService>();
    protected ILoginProviderService LoginProviderService => _serviceResolver.GetService<ILoginProviderService>();
    protected IListService ListService => _serviceResolver.GetService<IListService>();
    protected PumpObjectGraphType(IServiceResolver serviceResolver)
    {
      _serviceResolver = serviceResolver;

    }
  }
}
