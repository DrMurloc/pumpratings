﻿using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class ReleaseNotesGraphType : PumpObjectGraphType<ReleaseNotesNode>
  {
    public ReleaseNotesGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(notes => notes.Id);
      Field(notes => notes.Notes);
      Field(notes => notes.ReleaseDate);
      Field(notes => notes.UserId);
      FieldAsync<UserGraphType>("user",
        resolve: async context => await UserService.GetUserById(context.Source.UserId));
    }
  }
}
