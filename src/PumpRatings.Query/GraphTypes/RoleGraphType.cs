﻿using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class RoleGraphType : PumpObjectGraphType<RoleNode>
  {
    public RoleGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(role => role.Name);
      FieldAsync<ListGraphType<UserGraphType>>("users",
        resolve: async context => await UserService.GetUsersByRoleName(context.Source.Name));
    }
  }
}
