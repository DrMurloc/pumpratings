﻿using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class TagGraphType : PumpObjectGraphType<TagNode>
  {
    public TagGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(tag => tag.Name);
      Field(tag => tag.Color);
      FieldAsync<ListGraphType<ChartGraphType>>("charts",
        resolve: async context => await ChartService.GetChartsByTagName(context.Source.Name));
    }
  }
}
