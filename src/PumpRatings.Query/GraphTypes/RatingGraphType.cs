﻿
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class RatingGraphType : PumpObjectGraphType<RatingNode>
  {
    public RatingGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(rating => rating.ChartId);
      Field(rating => rating.UserId);
      Field(rating => rating.Rating);
      FieldAsync<UserGraphType>("user",
        resolve: async context => await UserService.GetUserById(context.Source.UserId));
      FieldAsync<ChartGraphType>("chart",
        resolve: async context => await ChartService.GetChartById(context.Source.ChartId));
    }
  }
}
