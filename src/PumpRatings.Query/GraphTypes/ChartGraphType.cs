﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphQL.Types;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.GraphTypes
{
  public sealed class ChartGraphType : PumpObjectGraphType<ChartNode>
  {
    public ChartGraphType(IServiceResolver serviceResolver) : base(serviceResolver)
    {
      Field(chart => chart.Id);
      Field<FloatGraphType>("averageRating",
        resolve:context => context.Source.AverageRating);
      Field<FloatGraphType>("difficulty",
        resolve:context=>context.Source.Difficulty);
      Field(chart => chart.Players);
      Field(chart => chart.SongId);
      Field(chart => chart.TypeName);
      Field<StringGraphType>("video",
        resolve:context => context.Source.Video);
      FieldAsync<SongGraphType>("song",
        resolve: async context => await SongService.GetSongById(context.Source.SongId));
      FieldAsync<ListGraphType<TagGraphType>>("tags",
        resolve: async context => await TagService.GetTagsByChart(context.Source.Id));
      FieldAsync<ListGraphType<StringGraphType>>("tagNames",
        resolve: async context => (await TagService.GetTagsByChart(context.Source.Id)).Select(tag => tag.Name));
      FieldAsync<ListGraphType<CommentGraphType>>("comments",
        resolve: async context => (await CommentService.GetCommentsByChartId(context.Source.Id)));
      FieldAsync<ChartGraphType>("type",
        resolve: async context => await ChartTypeService.GetChartTypeByName(context.Source.TypeName));
      FieldAsync<ListGraphType<RatingGraphType>>("ratings",
        resolve: async context => await RatingService.GetRatingsByChartId(context.Source.Id));
      FieldAsync<RatingGraphType>("myRating",
        resolve: async context => await RatingService.GetMyRatingByChart(context.Source.Id));
    }
  }
}
