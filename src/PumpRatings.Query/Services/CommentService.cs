﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQL.DataLoader;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class CommentService : ICommentService
  {
    private readonly ICommentRepository _repository;
    private readonly IDataLoaderContextAccessor _dataLoaderContextAccessor;
    private DataLoaderContext DataLoader => _dataLoaderContextAccessor.Context;
    public CommentService(ICommentRepository repository,
      IDataLoaderContextAccessor dataLoaderContextAccessor)
    {
      _repository = repository;
      _dataLoaderContextAccessor = dataLoaderContextAccessor;
    }
    public Task<IEnumerable<CommentNode>> GetCommentsByChartId(string chartId)
    {
      var loader = DataLoader.GetOrAddBatchLoader<string, IEnumerable<CommentNode>>(
        nameof(GetCommentsByChartId),
        BuildChartToCommentsEdges);
      return loader.LoadAsync(chartId);
    }

    private async Task<IDictionary<string, IEnumerable<CommentNode>>> BuildChartToCommentsEdges(
      IEnumerable<string> chartIds)
    {
      var uniqueChartIds = chartIds.Distinct().ToArray();
      var edges = await _repository.GetCommentsByChartIds(uniqueChartIds);
      return uniqueChartIds.ToDictionary(chartId => chartId,
        chartId => (IEnumerable<CommentNode>) edges.Where(edge => edge.ChartId == chartId).ToArray());
    }

    public Task<IEnumerable<CommentNode>> GetCommentsByUserId(string userId)
    {
      var loader = DataLoader.GetOrAddBatchLoader<string, IEnumerable<CommentNode>>(
        nameof(GetCommentsByUserId),
        BuildUserToCommentsEdges);
      return loader.LoadAsync(userId);
    }

    private async Task<IDictionary<string, IEnumerable<CommentNode>>> BuildUserToCommentsEdges(
      IEnumerable<string> userIds)
    {
      var uniqueUserIds = userIds.Distinct().ToArray();
      var edges = await _repository.GetCommentsByUserIds(uniqueUserIds);
      return uniqueUserIds.ToDictionary(userId => userId,
        userId => (IEnumerable<CommentNode>) edges.Where(edge => edge.UserId == userId).ToArray());
    }
  }
}
