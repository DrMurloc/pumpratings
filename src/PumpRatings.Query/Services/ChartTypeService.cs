﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class ChartTypeService : IChartTypeService
  {
    private readonly IChartTypeCache _cache;
    private readonly IChartTypeRepository _repository;
    public ChartTypeService(IChartTypeCache cache,
      IChartTypeRepository repository)
    {
      _cache = cache;
      _repository = repository;
    }
    public Task<IEnumerable<ChartTypeNode>> SearchForChartTypes()
    {
      return _cache.GetAllChartTypes(_repository.GetAllChartTypes);
    }

    public Task<ChartTypeNode> GetChartTypeByName(string chartTypeName)
    {
      return _cache.GetChartTypeByName(chartTypeName,_repository.GetAllChartTypes);
    }
  }
}
