﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class ReleaseNotesService : IReleaseNotesService
  {
    private readonly IReleaseNotesCache _cache;
    private readonly IReleaseNotesRepository _repository;
    public ReleaseNotesService(IReleaseNotesCache cache,
      IReleaseNotesRepository repository)
    {
      _cache = cache;
      _repository = repository;
    }
    public async Task<IEnumerable<ReleaseNotesNode>> SearchForReleaseNotes(int page, int count)
    {
      return (await _cache.GetOrSetReleaseNotes(_repository.GetAllReleaseNotes))
        .OrderByDescending(notes=>notes.ReleaseDate)
        .Skip((page - 1) * count)
        .Take(count)
        .ToArray();
    }

    public async Task<IEnumerable<ReleaseNotesNode>> GetReleaseNotesByUserId(string userId)
    {
      return (await _cache.GetOrSetReleaseNotes(_repository.GetAllReleaseNotes))
        .Where(releaseNotes => releaseNotes.UserId == userId)
        .ToArray();
    }
  }
}
