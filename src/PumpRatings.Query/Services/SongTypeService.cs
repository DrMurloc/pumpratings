﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class SongTypeService : ISongTypeService
  {
    private readonly ISongTypeCache _cache;
    private readonly ISongTypeRepository _repository;
    public SongTypeService(ISongTypeCache cache,
      ISongTypeRepository repository)
    {
      _cache = cache;
      _repository = repository;
    }
    public Task<IEnumerable<SongTypeNode>> SearchForSongTypes()
    {
      return _cache.GetAllSongTypes(_repository.GetAllSongTypes);
    }

    public Task<SongTypeNode> GetSongTypeByName(string songTypeName)
    {
      return _cache.GetSongTypeByName(songTypeName, _repository.GetAllSongTypes);
    }
  }
}
