﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class TagService : ITagService
  {
    private readonly ITagCache _cache;
    private readonly ITagRepository _repository;
    private readonly IChartTagCache _chartTagCache;
    private readonly IChartTagRepository _chartTagRepository;
    public TagService(ITagCache cache,
      ITagRepository repository,
      IChartTagCache chartTagCache,
      IChartTagRepository chartTagRepository)
    {
      _cache = cache;
      _repository = repository;
      _chartTagCache = chartTagCache;
      _chartTagRepository = chartTagRepository;
    }
    public Task<IEnumerable<TagNode>> SearchForTags()
    {
      return _cache.GetAllTags(_repository.GetAllTags);
    }

    public Task<TagNode> GetTagByName(string tagName)
    {
      return _cache.GetTagByName(tagName, _repository.GetAllTags);
    }

    public async Task<IEnumerable<TagNode>> GetTagsByChart(string chartId)
    {
      return (await _chartTagCache.GetChartTagsByChartId(chartId, _chartTagRepository.GetAllChartTags))
        .Select(chartTag => _cache.GetTagByName(chartTag.TagName,_repository.GetAllTags).Result);
    }
  }
}
