﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class MixService : IMixService
  {
    private readonly IMixCache _cache;
    private readonly IMixRepository _repository;
    public MixService(IMixCache cache,
      IMixRepository repository)
    {
      _cache = cache;
      _repository = repository;
    }
    public Task<IEnumerable<MixNode>> SearchForMixes()
    {
      return _cache.GetAllMixes(_repository.GetAllMixes);
    }

    public Task<MixNode> GetMixById(string mixId)
    {
      return _cache.GetMixById(mixId,_repository.GetAllMixes);
    }
  }
}
