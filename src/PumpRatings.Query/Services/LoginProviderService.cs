﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class LoginProviderService : ILoginProviderService
  {
    private readonly IEnumerable<ILoginProvider> _loginProviders;
    public LoginProviderService(IEnumerable<ILoginProvider> loginProviders)
    {
      _loginProviders = loginProviders;
    }
    public Task<IEnumerable<LoginProviderNode>> SearchForLoginProviders()
    {
      return Task.FromResult(_loginProviders.Select(provider => new LoginProviderNode
      {
        DisplayName = provider.DisplayName,
        Scheme = provider.Scheme,
        Icon=provider.Icon
      }));
    }
  }
}
