﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Security;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class UserService : IUserService
  {
    private readonly IUserCache _cache;
    private readonly IUserRepository _repository;
    private readonly IUserProfileAccessor _userProfileAccessor;
    public UserService(IUserCache cache,
      IUserRepository repository,
      IUserProfileAccessor userProfileAccessor)
    {
      _cache = cache;
      _repository = repository;
      _userProfileAccessor = userProfileAccessor;
    }

    public Task<UserNode> GetMe()
    {
      var profile = _userProfileAccessor.UserProfile;
      return Task.FromResult(new UserNode
      {
        CurrentRoleName = profile.CurrentRole,
        DarkTheme = profile.DarkTheme,
        Id = profile.Id,
        Name = profile.Name,
        RoleName = profile.Role
      });
    }

    public Task<IEnumerable<UserNode>> SearchForUsers()
    {
      return _cache.GetAllUsers(_repository.GetAllUsers);
    }

    public Task<UserNode> GetUserById(string userId)
    {
      return _cache.GetUserById(userId,_repository.GetAllUsers);
    }

    public Task<IEnumerable<UserNode>> GetUsersByRoleName(string roleName)
    {
      return _cache.GetUsersByRole(roleName,_repository.GetAllUsers);
    }
  }
}
