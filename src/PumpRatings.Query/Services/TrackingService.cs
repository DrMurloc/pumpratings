﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQL.DataLoader;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class TrackingService : ITrackingService
  {
    private readonly ITrackingRepository _repository;
    private readonly IDataLoaderContextAccessor _dataLoaderContextAccessor;
    private DataLoaderContext DataLoader => _dataLoaderContextAccessor.Context;
    public TrackingService(ITrackingRepository repository,
      IDataLoaderContextAccessor dataLoaderContextAccessor)
    {
      _repository = repository;
      _dataLoaderContextAccessor = dataLoaderContextAccessor;
    }
    public Task<IEnumerable<TrackingNode>> SearchForTracking(int page, int count)
    {
      return _repository.SearchTracking(page, count);
    }

    public Task<IEnumerable<TrackingNode>> GetTrackingByUser(string userId)
    {
      var loader = DataLoader.GetOrAddBatchLoader<string, IEnumerable<TrackingNode>>(
        nameof(GetTrackingByUser),
        BuildUserTrackingDictionary);
      return loader.LoadAsync(userId);
    }

    private async Task<IDictionary<string, IEnumerable<TrackingNode>>> BuildUserTrackingDictionary(
      IEnumerable<string> userIds)
    {
      var uniqueUserIds = userIds.Distinct().ToArray();
      var edges = await _repository.GetTrackingByUserIds(uniqueUserIds);
      return uniqueUserIds.ToDictionary(userId => userId,
        userId => (IEnumerable<TrackingNode>) edges.Where(edge => edge.UserId == userId).ToArray());
    }
  }
}
