﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQL.DataLoader;
using PumpRatings.Abstractions.Security;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class RatingService : IRatingService
  {
    private readonly IUserProfileAccessor _user;
    private readonly IRatingRepository _repository;
    private readonly IDataLoaderContextAccessor _dataLoaderContextAccessor;
    private DataLoaderContext DataLoader => _dataLoaderContextAccessor.Context;

    public RatingService(IUserProfileAccessor user,
      IRatingRepository repository,
      IDataLoaderContextAccessor dataLoaderContextAccessor)
    {
      _user = user;
      _repository = repository;
      _dataLoaderContextAccessor = dataLoaderContextAccessor;
    }

    public Task<IEnumerable<RatingNode>> GetRatingsByChartId(string chartId)
    {
      var loader = DataLoader.GetOrAddBatchLoader<string, IEnumerable<RatingNode>>(
        nameof(GetRatingsByChartId),
        BuildChartToRatingEdges);
      return loader.LoadAsync(chartId);
    }

    private async Task<IDictionary<string, IEnumerable<RatingNode>>> BuildChartToRatingEdges(
      IEnumerable<string> chartIds)
    {
      var uniqueChartIds = chartIds.Distinct().ToArray();
      var edges = await _repository.GetRatingsByChartIds(uniqueChartIds);
      return uniqueChartIds.ToDictionary(chartId => chartId,
        chartId => (IEnumerable<RatingNode>) edges.Where(edge => edge.ChartId == chartId).ToArray());
    }
    public Task<RatingNode> GetMyRatingByChart(string chartId)
    {
      var loader = DataLoader.GetOrAddBatchLoader<string, RatingNode>(
        nameof(GetMyRatingByChart),
        BuildChartUserToRatingEdges);
      return loader.LoadAsync(chartId);
    }
    private async Task<IDictionary<string,RatingNode>> BuildChartUserToRatingEdges(
      IEnumerable<string> chartIds)
    {
      var uniqueChartIds = chartIds.Distinct().ToArray();
      var userId = _user.UserProfile.Id;
      var edges = await _repository.GetRatingsByChartIdsAndUserId(uniqueChartIds, userId);
      return uniqueChartIds.ToDictionary(chartId => chartId,
        chartId => edges.FirstOrDefault(edge => edge.ChartId == chartId));
    }
    public Task<IEnumerable<RatingNode>> GetRatingsByUserId(string userId)
    {
      var loader = DataLoader.GetOrAddBatchLoader<string, IEnumerable<RatingNode>>(
        nameof(GetRatingsByUserId),
        BuildUserToRatingEdges);
      return loader.LoadAsync(userId);
    }

    private async Task<IDictionary<string, IEnumerable<RatingNode>>> BuildUserToRatingEdges(
      IEnumerable<string> userIds)
    {
      var uniqueUserIds = userIds.Distinct().ToArray();
      var edges = await _repository.GetRatingsByUserIds(uniqueUserIds);
      return uniqueUserIds.ToDictionary(userId => userId,
        userId => (IEnumerable<RatingNode>) edges.Where(edge => edge.UserId == userId).ToArray());
    }
  }
}
