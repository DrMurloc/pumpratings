﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class RoleService : IRoleService
  {
    private readonly IRoleCache _cache;
    private readonly IRoleRepository _repository;
    public RoleService(IRoleCache cache,
      IRoleRepository repository)
    {
      _cache = cache;
      _repository = repository;
    }
    public Task<IEnumerable<RoleNode>> SearchForRoles()
    {
      return _cache.GetAllRoles(_repository.GetAllRoles);
    }

    public Task<RoleNode> GetRoleByName(string roleName)
    {
      return _cache.GetRoleByName(roleName,_repository.GetAllRoles);
    }
  }
}
