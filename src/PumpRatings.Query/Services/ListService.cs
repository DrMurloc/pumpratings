﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQL.DataLoader;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public class ListService : IListService
  {
    private readonly IListRepository _repository;
    private readonly IDataLoaderContextAccessor _dataLoaderContextAccessor;
    private DataLoaderContext DataLoader => _dataLoaderContextAccessor.Context;
    public ListService(IListRepository repository,
      IDataLoaderContextAccessor dataLoaderContextAccessor)
    {
      _repository = repository;
      _dataLoaderContextAccessor = dataLoaderContextAccessor;
    }
    public Task<IEnumerable<ListNode>> GetListsByUserId(string userId)
    {
      var loader =
        DataLoader.GetOrAddBatchLoader<string, IEnumerable<ListNode>>(nameof(GetListsByUserId),
          BuildUserToListsDictionary);
      return loader.LoadAsync(userId);
    }

    private async Task<IDictionary<string, IEnumerable<ListNode>>> BuildUserToListsDictionary(
      IEnumerable<string> userIds)
    {
      var uniqueUserIds = userIds.Distinct().ToArray();
      var lists = await _repository.GetListsByUserIds(uniqueUserIds);
      return uniqueUserIds.ToDictionary(userId => userId,
        userId => (IEnumerable<ListNode>)lists.Where(list => list.UserId == userId).ToArray());
    }
  }
}
