﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class SongService : ISongService
  {
    private readonly ISongCache _cache;
    private readonly ISongRepository _repository;
    public SongService(ISongCache cache,
      ISongRepository repository)
    {
      _cache = cache;
      _repository = repository;
    }
    public Task<IEnumerable<SongNode>> SearchForSongs()
    {
      return _cache.GetAllSongs(_repository.GetAllSongs);
    }

    public Task<IEnumerable<SongNode>> GetSongsByMixId(string mixId)
    {
      return _cache.GetSongsByMixId(mixId,_repository.GetAllSongs);
    }

    public Task<IEnumerable<SongNode>> GetSongsByTypeName(string songTypeName)
    {
      return _cache.GetSongsByTypeName(songTypeName,_repository.GetAllSongs);
    }

    public Task<SongNode> GetSongById(string songId)
    {
      return _cache.GetSongById(songId,_repository.GetAllSongs);
    }
  }
}
