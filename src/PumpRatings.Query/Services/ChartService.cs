﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQL.DataLoader;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;

namespace PumpRatings.Query.Services
{
  public sealed class ChartService : IChartService
  {
    private readonly IChartRepository _repository;
    private readonly IChartCache _cache;
    private readonly IChartTagCache _chartTagCache;
    private readonly IChartTagRepository _chartTagRepository;

    private readonly IDataLoaderContextAccessor _dataLoaderContextAccessor;
    private DataLoaderContext DataLoader => _dataLoaderContextAccessor.Context;
    public ChartService(IChartRepository repository,
      IChartCache cache,
      IChartTagCache chartTagCache,
      IChartTagRepository chartTagRepository,
      IDataLoaderContextAccessor dataLoaderContextAccessor)
    {
      _repository = repository;
      _cache = cache;
      _chartTagCache = chartTagCache;
      _chartTagRepository = chartTagRepository;
      _dataLoaderContextAccessor = dataLoaderContextAccessor;

    }
    public Task<IEnumerable<ChartNode>> SearchForCharts()
    {
      return _cache.GetAllCharts(_repository.GetAllCharts);
    }

    public async Task<IEnumerable<ChartNode>> GetChartsBySongId(string songId)
    {
      return (await _cache.GetChartsBySongId(songId,_repository.GetAllCharts))
        .Where(chart => chart.SongId == songId)
        .ToArray();
    }

    public async Task<IEnumerable<ChartNode>> GetChartsByTagName(string tagName)
    {
      return (await _chartTagCache.GetChartTagsByTagName(tagName, _chartTagRepository.GetAllChartTags))
        .Select(edge => _cache.GetChartById(edge.ChartId,_repository.GetAllCharts).Result)
        .ToArray();
    }

    public async Task<IEnumerable<ChartNode>> GetChartsByTypeName(string chartTypeName)
    {
      return (await _cache.GetChartsByTypeName(chartTypeName,_repository.GetAllCharts))
        .Where(chart => chart.TypeName == chartTypeName)
        .ToArray();
    }
    public Task<ChartNode> GetChartById(string chartId)
    {
      return _cache.GetChartById(chartId,_repository.GetAllCharts);
    }
    public Task<IEnumerable<ChartNode>> GetChartsByListId(string listId)
    {
      var loader = DataLoader.GetOrAddBatchLoader<string, IEnumerable<ChartNode>>(nameof(GetChartsByListId),
        BuildListToChartsDictionary);
      return loader.LoadAsync(listId);
    }

    private async Task<IDictionary<string, IEnumerable<ChartNode>>> BuildListToChartsDictionary(
      IEnumerable<string> listIds)
    {
      var uniqueListIds = listIds.Distinct().ToArray();
      var listCharts = await _repository.GetChartIdsByList(uniqueListIds);
      return uniqueListIds.ToDictionary(listId => listId,
        listId => (IEnumerable<ChartNode>)listCharts.Where(listChart => listChart.ListId == listId)
          .Select(listChart => _cache.GetChartById(listChart.ChartId,_repository.GetAllCharts).Result)
          .ToArray());
    }
  }
}
