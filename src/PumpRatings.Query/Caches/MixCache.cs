﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Caches
{
  public sealed class MixCache : IMixCache,
    IEventHandler<ClearCacheEvent>,
    IEventHandler<MixCreatedEvent>
  {
    private IDictionary<string, MixNode> _mixIdToMixes;
    private Task _buildDictionariesTask;

    private async Task BuildDictionariesTask(Task<IEnumerable<MixNode>> retrieval)
    {
      _mixIdToMixes = new Dictionary<string, MixNode>(StringComparer.OrdinalIgnoreCase);
      var mixes = await retrieval;
      foreach (var mix in mixes)
      {
        _mixIdToMixes[mix.Id] = mix;
      }
    }

    private Task BuildDictionaries(Func<Task<IEnumerable<MixNode>>> retrieval)
    {
      return _buildDictionariesTask ?? (_buildDictionariesTask = BuildDictionariesTask(retrieval()));
    }
    public async Task<IEnumerable<MixNode>> GetAllMixes(Func<Task<IEnumerable<MixNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _mixIdToMixes.Values;
    }

    public async Task<MixNode> GetMixById(string mixId, Func<Task<IEnumerable<MixNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _mixIdToMixes.TryGetValue(mixId, out var mix) ? mix : null;
    }

    public Task Handle(ClearCacheEvent inEvent)
    {
      _buildDictionariesTask = null;
      return Task.CompletedTask;
    }

    public async Task Handle(MixCreatedEvent inEvent)
    {
      if (_buildDictionariesTask == null)
      {
        return;
      }
      await _buildDictionariesTask;
      _mixIdToMixes[inEvent.Id] = new MixNode
      {
        DisplayOrder = inEvent.DisplayOrder,
        Id = inEvent.Id,
        Image = inEvent.Image,
        Name = inEvent.Name
      };
    }
  }
}
