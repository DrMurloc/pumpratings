﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Caches
{
  public sealed class ChartTagCache : IChartTagCache,
    IEventHandler<ClearCacheEvent>,
    IEventHandler<ChartTagsUpdatedEvent>
  {
    private IDictionary<string, IEnumerable<ChartTagEdge>> _chartToTags;
    
    private IDictionary<string, IEnumerable<ChartTagEdge>> _tagToCharts;
    private Task<IEnumerable<ChartTagEdge>> _retrieval;
    private Task _buildDictionariesTask;
    
    private async Task BuildDictionaries(
      Func<Task<IEnumerable<ChartTagEdge>>> retrieval)
    {
      _retrieval = retrieval();
      var edges = (await _retrieval).ToArray();
      var distinctChartIds = edges.Select(edge => edge.ChartId).Distinct().ToArray();
      _chartToTags = distinctChartIds.ToDictionary(chartId => chartId,
        chartId => (IEnumerable<ChartTagEdge>) edges.Where(edge => edge.ChartId == chartId).ToArray());

      var distinctTagNames = edges.Select(edge => edge.TagName).Distinct().ToArray();
      _tagToCharts = distinctTagNames.ToDictionary(tagName => tagName,
        tagName => (IEnumerable<ChartTagEdge>) edges.Where(edge => edge.TagName == tagName).ToArray());

    }
    public async Task<IEnumerable<ChartTagEdge>> GetChartTagsByChartId(string chartId, Func<Task<IEnumerable<ChartTagEdge>>> retrieval)
    {
      if (_buildDictionariesTask == null)
      {
        _buildDictionariesTask = BuildDictionaries(retrieval);
      }

      await _buildDictionariesTask;
      return _chartToTags.TryGetValue(chartId, out var value) ? value : new ChartTagEdge[] { };
    }

    public async Task<IEnumerable<ChartTagEdge>> GetChartTagsByTagName(string tagName, Func<Task<IEnumerable<ChartTagEdge>>> retrieval)
    {
      if (_buildDictionariesTask == null)
      {
        _buildDictionariesTask = BuildDictionaries(retrieval);
      }

      await _buildDictionariesTask;
      return _tagToCharts.TryGetValue(tagName, out var value) ? value : new ChartTagEdge[] { };
    }

    public Task Handle(ClearCacheEvent inEvent)
    {
      _buildDictionariesTask = null;
      return Task.CompletedTask;
    }

    public async Task Handle(ChartTagsUpdatedEvent inEvent)
    {
      if (_buildDictionariesTask == null)
      {
        return;
      }

      await _buildDictionariesTask;
      if (!_chartToTags.ContainsKey(inEvent.ChartId))
      {
        _buildDictionariesTask = null;
        return;
      }

      var oldTags = _chartToTags[inEvent.ChartId];
      foreach (var tag in oldTags)
      {
        _tagToCharts[tag.TagName] =
          _tagToCharts[tag.TagName].Where(chartTag => chartTag.ChartId != inEvent.ChartId).ToArray();
      }

      foreach (var tag in inEvent.TagNames)
      {
        _tagToCharts[tag] = _tagToCharts[tag].Append(new ChartTagEdge
        {
          ChartId = inEvent.ChartId,
          TagName = tag
        }).ToArray();
      }

      _chartToTags[inEvent.ChartId] = inEvent.TagNames.Select(tagName=>new ChartTagEdge
      {
        ChartId=inEvent.ChartId,
        TagName=tagName
      }).ToArray();
    }
  }
}
