﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Caches
{
  public sealed class SongCache : ISongCache,
    IEventHandler<ClearCacheEvent>,
    IEventHandler<SongCreatedEvent>
  {
    private IDictionary<string, SongNode> _songIdToSongs;
    private IDictionary<string, List<SongNode>> _mixIdToSongs;
    private IDictionary<string, List<SongNode>> _typeToSongs;
    private Task _buildDictionariesTask;

    private async Task BuildDictionariesTask(Task<IEnumerable<SongNode>> retrieval)
    {
      _songIdToSongs=new Dictionary<string, SongNode>(StringComparer.OrdinalIgnoreCase);
      _mixIdToSongs=new Dictionary<string, List<SongNode>>(StringComparer.OrdinalIgnoreCase);
      _typeToSongs=new Dictionary<string, List<SongNode>>(StringComparer.OrdinalIgnoreCase);
      var songs = await retrieval;
      foreach (var song in songs)
      {
        _songIdToSongs[song.Id] = song;
        if (!_mixIdToSongs.ContainsKey(song.MixId))
        {
          _mixIdToSongs[song.MixId]=new List<SongNode>();
        }
        _mixIdToSongs[song.MixId].Add(song);
        if (!_typeToSongs.ContainsKey(song.TypeName))
        {
          _typeToSongs[song.TypeName] = new List<SongNode>();
        }
        _typeToSongs[song.TypeName].Add(song);
      }
    }

    private Task BuildDictionaries(Func<Task<IEnumerable<SongNode>>> retrieval)
    {
      return _buildDictionariesTask ?? (_buildDictionariesTask = BuildDictionariesTask(retrieval()));
    }
    public async Task<IEnumerable<SongNode>> GetAllSongs(Func<Task<IEnumerable<SongNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _songIdToSongs.Values;
    }

    public async Task<IEnumerable<SongNode>> GetSongsByMixId(string mixId, Func<Task<IEnumerable<SongNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _mixIdToSongs.TryGetValue(mixId, out var songs) ? songs : new List<SongNode>();
    }

    public async Task<IEnumerable<SongNode>> GetSongsByTypeName(string typeName, Func<Task<IEnumerable<SongNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _typeToSongs.TryGetValue(typeName, out var songs) ? songs : new List<SongNode>();
    }

    public async Task<SongNode> GetSongById(string songId, Func<Task<IEnumerable<SongNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _songIdToSongs.TryGetValue(songId, out var song) ? song : null;
    }
    public Task Handle(ClearCacheEvent inEvent)
    {
      _buildDictionariesTask = null;
      return Task.CompletedTask;
    }

    public async Task Handle(SongCreatedEvent inEvent)
    {
      if (_buildDictionariesTask == null)
      {
        return;
      }

      await _buildDictionariesTask;
      var node = new SongNode
      {
        Artist = inEvent.Artist,
        Bpm = inEvent.Bpm,
        Id = inEvent.Id,
        Image = inEvent.Image,
        MixId = inEvent.MixId,
        Name = inEvent.Name,
        TypeName = inEvent.Type
      };
      _songIdToSongs[node.Id] = node;
      if (!_mixIdToSongs.ContainsKey(node.MixId))
      {
        _mixIdToSongs[node.MixId]=new List<SongNode>();
      }
      _mixIdToSongs[node.MixId].Add(node);
      if (!_typeToSongs.ContainsKey(node.TypeName))
      {
        _typeToSongs[node.TypeName]=new List<SongNode>();
      }
      _typeToSongs[node.TypeName].Add(node);
    }
  }
}
