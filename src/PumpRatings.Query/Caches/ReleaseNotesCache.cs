﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Caches
{
  public sealed class ReleaseNotesCache : IReleaseNotesCache,
    IEventHandler<ClearCacheEvent>,
    IEventHandler<ReleaseNotesCreatedEvent>
  {
    private Task<IEnumerable<ReleaseNotesNode>> _retrieval;
    public Task<IEnumerable<ReleaseNotesNode>> GetOrSetReleaseNotes(Func<Task<IEnumerable<ReleaseNotesNode>>> retrieval)
    {
      return _retrieval ?? (_retrieval = retrieval());
    }

    public Task Handle(ClearCacheEvent inEvent)
    {
      _retrieval = null;
      return Task.CompletedTask;
    }

    public Task Handle(ReleaseNotesCreatedEvent inEvent)
    {
      _retrieval = null;
      return Task.CompletedTask;
    }
  }
}
