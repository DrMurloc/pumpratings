﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Caches
{
  public class ChartTypeCache : IChartTypeCache,
    IEventHandler<ClearCacheEvent>
  {
    private IDictionary<string, ChartTypeNode> _nameToChartType;
    private Task _buildDictionariesTask;

    private async Task BuildDictionariesTask(Task<IEnumerable<ChartTypeNode>> retrieval)
    {
      _nameToChartType=new Dictionary<string, ChartTypeNode>(StringComparer.OrdinalIgnoreCase);
      var types = await retrieval;
      foreach (var type in types)
      {
        _nameToChartType[type.Name] = type;
      }
    }

    private Task BuildDictionaries(Func<Task<IEnumerable<ChartTypeNode>>> retrieval)
    {
      return _buildDictionariesTask ?? (_buildDictionariesTask = BuildDictionariesTask(retrieval()));
    }

    public async Task<IEnumerable<ChartTypeNode>> GetAllChartTypes(Func<Task<IEnumerable<ChartTypeNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _nameToChartType.Values;
    }

    public async Task<ChartTypeNode> GetChartTypeByName(string typeName, Func<Task<IEnumerable<ChartTypeNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _nameToChartType.TryGetValue(typeName, out var type) ? type : null;
    }
    public Task Handle(ClearCacheEvent inEvent)
    {
      _buildDictionariesTask = null;
      return Task.CompletedTask;
    }
  }
}
