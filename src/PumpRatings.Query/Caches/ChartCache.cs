﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Caches
{
  public sealed class ChartCache : IChartCache,
    IEventHandler<ClearCacheEvent>,
    IEventHandler<ChartCreatedEvent>,
    IEventHandler<ChartUpdatedEvent>,
    IEventHandler<RatingUpdatedEvent>
  {
    private IDictionary<string, ChartNode> _chartIdToCharts;
    private IDictionary<string, List<ChartNode>> _songIdToCharts;
    private IDictionary<string, List<ChartNode>> _typeToCharts;

    private Task _buildDictionariesTask;

    private async Task BuildDictionariesTask(Task<IEnumerable<ChartNode>> retrieval)
    {
      _chartIdToCharts=new Dictionary<string, ChartNode>(StringComparer.OrdinalIgnoreCase);
      _songIdToCharts=new Dictionary<string, List<ChartNode>>(StringComparer.OrdinalIgnoreCase);
      _typeToCharts=new Dictionary<string, List<ChartNode>>(StringComparer.OrdinalIgnoreCase);
      var charts = await retrieval;
      foreach (var chart in charts)
      {
        _chartIdToCharts[chart.Id] = chart;
        if (!_songIdToCharts.ContainsKey(chart.SongId))
        {
          _songIdToCharts[chart.SongId] = new List<ChartNode>();
        }
        _songIdToCharts[chart.SongId].Add(chart);
        if (!_typeToCharts.ContainsKey(chart.TypeName))
        {
          _typeToCharts[chart.TypeName] = new List<ChartNode>();
        }
        _typeToCharts[chart.TypeName].Add(chart);
      }
    }
    private Task BuildDictionaries(Func<Task<IEnumerable<ChartNode>>> retrieval)
    {
      return _buildDictionariesTask ?? (_buildDictionariesTask = BuildDictionariesTask(retrieval()));
    }

    public async Task<IEnumerable<ChartNode>> GetAllCharts(Func<Task<IEnumerable<ChartNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _chartIdToCharts.Values;
    }

    public async Task<ChartNode> GetChartById(string chartId, Func<Task<IEnumerable<ChartNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _chartIdToCharts.TryGetValue(chartId,out var chart)?chart:null;
    }

    public async Task<IEnumerable<ChartNode>> GetChartsBySongId(string songId, Func<Task<IEnumerable<ChartNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _songIdToCharts.TryGetValue(songId,out var charts)?charts:new List<ChartNode>();
    }

    public async Task<IEnumerable<ChartNode>> GetChartsByTypeName(string typeName, Func<Task<IEnumerable<ChartNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _typeToCharts.TryGetValue(typeName, out var charts) ? charts : new List<ChartNode>();
    }

    public Task Handle(ClearCacheEvent inEvent)
    {
      _buildDictionariesTask = null;
      return Task.CompletedTask;
    }

    public async Task Handle(ChartCreatedEvent inEvent)
    {
      if (_buildDictionariesTask == null)
      {
        return;
      }
      await _buildDictionariesTask;
      var node = new ChartNode
      {
        AverageRating = null,
        Difficulty = inEvent.Difficulty,
        Id=inEvent.Id,
        Players=inEvent.Players,
        SongId = inEvent.SongId,
        TypeName = inEvent.Type,
        Video=inEvent.Video
      };
      _chartIdToCharts[node.Id] = node;
      if (!_songIdToCharts.ContainsKey(node.SongId))
      {
        _songIdToCharts[node.SongId] = new List<ChartNode>();
      }
      _songIdToCharts[node.SongId].Add(node);
      if (!_typeToCharts.ContainsKey(node.TypeName))
      {
        _songIdToCharts[node.TypeName] = new List<ChartNode>();
      }
      _typeToCharts[node.TypeName].Add(node);
    }

    public async Task Handle(ChartUpdatedEvent inEvent)
    {
      if (_buildDictionariesTask == null)
      {
        return;
      }
      await _buildDictionariesTask;
      if (!_chartIdToCharts.ContainsKey(inEvent.ChartId))
      {
        _buildDictionariesTask = null;
        return;
      }

      _chartIdToCharts[inEvent.ChartId].Video = inEvent.Video;
    }

    public async Task Handle(RatingUpdatedEvent inEvent)
    {
      if (_buildDictionariesTask == null)
      {
        return;
      }
      await _buildDictionariesTask;
      if (!_chartIdToCharts.ContainsKey(inEvent.ChartId))
      {
        _buildDictionariesTask = null;
        return;
      }
      _chartIdToCharts[inEvent.ChartId].AverageRating = inEvent.NewAverageRating;
    }
  }
}
