﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Caches
{
  public sealed class RoleCache : IRoleCache,
    IEventHandler<ClearCacheEvent>
  {
    private IDictionary<string, RoleNode> _nameToRole;
    private Task _buildDictionariesTask;

    private async Task BuildDictionariesTask(Task<IEnumerable<RoleNode>> retrieval)
    {
      _nameToRole=new Dictionary<string, RoleNode>(StringComparer.OrdinalIgnoreCase);
      var roles = await retrieval;
      foreach (var role in roles)
      {
        _nameToRole[role.Name] = role;
      }
    }

    private Task BuildDictionaries(Func<Task<IEnumerable<RoleNode>>> retrieval)
    {
      return _buildDictionariesTask ?? (_buildDictionariesTask = BuildDictionariesTask(retrieval()));
    }
    public async Task<IEnumerable<RoleNode>> GetAllRoles(Func<Task<IEnumerable<RoleNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _nameToRole.Values;
    }

    public async Task<RoleNode> GetRoleByName(string roleName, Func<Task<IEnumerable<RoleNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _nameToRole.TryGetValue(roleName, out var role) ? role : null;
    }

    public Task Handle(ClearCacheEvent inEvent)
    {
      _buildDictionariesTask = null;
      return Task.CompletedTask;
    }
  }
}
