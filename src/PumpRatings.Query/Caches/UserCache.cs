﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Caches
{
  public sealed class UserCache : IUserCache,
    IEventHandler<ClearCacheEvent>,
    IEventHandler<ProfileUpdatedEvent>,
    IEventHandler<ProfileCreatedEvent>
  {
    private IDictionary<string, UserNode> _idToUsers;
    private IDictionary<string, List<UserNode>> _roleToUsers;
    private Task _buildDictionariesTask;

    private async Task BuildDictionariesTask(Task<IEnumerable<UserNode>> retrieval)
    {
      _idToUsers=new Dictionary<string, UserNode>(StringComparer.OrdinalIgnoreCase);
      _roleToUsers=new Dictionary<string, List<UserNode>>(StringComparer.OrdinalIgnoreCase);
      var users = await retrieval;
      foreach(var user in users)
      {
        _idToUsers[user.Id] = user;
        if (!_roleToUsers.ContainsKey(user.RoleName))
        {
          _roleToUsers[user.RoleName]=new List<UserNode>();
        }
        _roleToUsers[user.RoleName].Add(user);
      }
    }

    private Task BuildDictionaries(Func<Task<IEnumerable<UserNode>>> retrieval)
    {
      return _buildDictionariesTask ?? (_buildDictionariesTask = BuildDictionariesTask(retrieval()));
    }
    public async Task<IEnumerable<UserNode>> GetAllUsers(Func<Task<IEnumerable<UserNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _idToUsers.Values;
    }

    public async Task<UserNode> GetUserById(string userId, Func<Task<IEnumerable<UserNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _idToUsers.TryGetValue(userId, out var user) ? user : null;
    }

    public async Task<IEnumerable<UserNode>> GetUsersByRole(string role, Func<Task<IEnumerable<UserNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _roleToUsers.TryGetValue(role, out var users) ? users : new List<UserNode>();
    }

    public Task Handle(ClearCacheEvent inEvent)
    {
      _buildDictionariesTask = null;
      return Task.CompletedTask;
    }

    public async Task Handle(ProfileUpdatedEvent inEvent)
    {
      if (_buildDictionariesTask == null)
      {
        return;
      }

      await _buildDictionariesTask;
      if (!_idToUsers.ContainsKey(inEvent.UserId))
      {
        _buildDictionariesTask = null;
        return;
      }

      var user = _idToUsers[inEvent.UserId];
      user.Name = inEvent.Name;
    }

    public async Task Handle(ProfileCreatedEvent inEvent)
    {
      if (_buildDictionariesTask == null)
      {
        return;
      }

      await _buildDictionariesTask;
      var newUser = new UserNode
      {
        Id=inEvent.Id,
        Name = inEvent.Name,
        RoleName=inEvent.Role,
        CurrentRoleName = inEvent.CurrentRole,
        DarkTheme = inEvent.DarkTheme
      };
      _idToUsers[newUser.Id] = newUser;
      if (!_roleToUsers.ContainsKey(newUser.RoleName))
      {
        _roleToUsers[newUser.RoleName]=new List<UserNode>();
      }
      _roleToUsers[newUser.RoleName].Add(newUser);
    }
  }
}
