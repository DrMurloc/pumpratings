﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Caches
{
  public sealed class SongTypeCache : ISongTypeCache,
    IEventHandler<ClearCacheEvent>
  {
    private IDictionary<string, SongTypeNode> _nameToSongType;
    private Task _buildDictionariesTask;

    private async Task BuildDictionariesTask(Task<IEnumerable<SongTypeNode>> retrieval)
    {
      _nameToSongType = new Dictionary<string, SongTypeNode>(StringComparer.OrdinalIgnoreCase);
      var types = await retrieval;
      foreach (var type in types)
      {
        _nameToSongType[type.Name] = type;
      }
    }

    private Task BuildDictionaries(Func<Task<IEnumerable<SongTypeNode>>> retrieval)
    {
      return _buildDictionariesTask ?? (_buildDictionariesTask = BuildDictionariesTask(retrieval()));
    }

    public async Task<IEnumerable<SongTypeNode>> GetAllSongTypes(Func<Task<IEnumerable<SongTypeNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _nameToSongType.Values;
    }

    public async Task<SongTypeNode> GetSongTypeByName(string typeName, Func<Task<IEnumerable<SongTypeNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _nameToSongType.TryGetValue(typeName, out var type) ? type : null;
    }
    public Task Handle(ClearCacheEvent inEvent)
    {
      _buildDictionariesTask = null;
      return Task.CompletedTask;
    }
  }
}
