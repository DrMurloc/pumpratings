﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Models;

namespace PumpRatings.Query.Caches
{
  public sealed class TagCache : ITagCache,
    IEventHandler<ClearCacheEvent>,
    IEventHandler<TagCreatedEvent>
  {
    private IDictionary<string, TagNode> _nameToTags;
    private Task _buildDictionariesTask;

    private async Task BuildDictionariesTask(Task<IEnumerable<TagNode>> retrieval)
    {
      _nameToTags = new Dictionary<string, TagNode>(StringComparer.OrdinalIgnoreCase);
      var tags = await retrieval;
      foreach (var tag in tags)
      {
        _nameToTags[tag.Name] = tag;
      }
    }

    private Task BuildDictionaries(Func<Task<IEnumerable<TagNode>>> retrieval)
    {
      return _buildDictionariesTask ?? (_buildDictionariesTask = BuildDictionariesTask(retrieval()));
    }
    public async Task<IEnumerable<TagNode>> GetAllTags(Func<Task<IEnumerable<TagNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _nameToTags.Values;
    }

    public async Task<TagNode> GetTagByName(string tagName, Func<Task<IEnumerable<TagNode>>> retrieval)
    {
      await BuildDictionaries(retrieval);
      return _nameToTags.TryGetValue(tagName, out var tag) ? tag : null;
    }
    public Task Handle(ClearCacheEvent inEvent)
    {
      _buildDictionariesTask = null;
      return Task.CompletedTask;
    }

    public async Task Handle(TagCreatedEvent inEvent)
    {
      if (_buildDictionariesTask == null)
      {
        return;
      }

      await _buildDictionariesTask;
      _nameToTags[inEvent.Name] = new TagNode
      {
        Color = inEvent.Color,
        Name = inEvent.Name
      };
    }
  }
}
