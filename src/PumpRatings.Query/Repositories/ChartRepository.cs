﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class ChartRepository : IChartRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public ChartRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<ChartNode>> GetAllCharts()
    {
      using (var scope = _metrics.BeginRepositoryScope<ChartRepository>(nameof(GetAllCharts)))
      {
        var results = (await _connection.QueryAsync<ChartNode>(@"
SELECT c.Id,
  c.Difficulty,
  c.Type AS TypeName,
  c.SongId,
  c.Video,
  c.Players,
  AVG(r.Rating) AverageRating
FROM Charts c
  LEFT OUTER JOIN Ratings r ON c.Id = r.ChartId
GROUP BY c.Id,
  c.Difficulty,
  c.Type,
  c.SongId,
  c.Video,
  c.Players")).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }

    public async Task<double> GetChartAverageRating(string chartId)
    {
      using (var scope = _metrics.BeginRepositoryScope<ChartRepository>(nameof(GetChartAverageRating)))
      {
        var result = await _connection.QueryFirstOrDefaultAsync<double>(@"
SELECT AVG(r.Rating)
FROM Ratings r
WHERE r.ChartId = @chartId", new { chartId });
        scope.ResultCount =1;
        return result;
      }
    }

    public async Task<IEnumerable<ListChartEdge>> GetChartIdsByList(IEnumerable<string> listIds)
    {
      using (var scope = _metrics.BeginRepositoryScope<ChartRepository>(nameof(GetChartIdsByList)))
      {
        var result = (await _connection.QueryAsync<ListChartEdge>(@"
SELECT ListId,
  ChartId
FROM ListCharts WHERE ListId IN @listIds", new {listIds})).ToArray();
        scope.ResultCount = result.Length;
        return result;
      }
    }
  }
}
