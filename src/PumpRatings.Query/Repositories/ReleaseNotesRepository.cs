﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class ReleaseNotesRepository : IReleaseNotesRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public ReleaseNotesRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<ReleaseNotesNode>> GetAllReleaseNotes()
    {
      using (var scope = _metrics.BeginRepositoryScope<ReleaseNotesRepository>(nameof(GetAllReleaseNotes)))
      {
        var results = (await _connection.QueryAsync<ReleaseNotesNode>(@"
SELECT Id,
  UserId,
  ReleaseDate,
  Notes
FROM ReleaseNotes")).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
