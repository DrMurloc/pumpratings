﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class SongRepository : ISongRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public SongRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<SongNode>> GetAllSongs()
    {
      using (var scope = _metrics.BeginRepositoryScope<SongRepository>(nameof(GetAllSongs)))
      {
        var results = (await _connection.QueryAsync<SongNode>(@"
SELECT Id,
  Name,
  Artist,
  Bpm,
  Image,
  MixId,
  Type AS TypeName
FROM Songs")).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
