﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class RoleRepository :IRoleRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;

    public RoleRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<RoleNode>> GetAllRoles()
    {
      using (var scope = _metrics.BeginRepositoryScope<RoleRepository>(nameof(GetAllRoles)))
      {
        var results = (await _connection.QueryAsync<RoleNode>(@"
SELECT Name
FROM Roles")).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
