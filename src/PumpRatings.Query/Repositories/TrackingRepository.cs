﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MoreLinq;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class TrackingRepository : ITrackingRepository
  {
    private readonly IMetricsService _metrics;
    private readonly IDbConnection _connection;
    public TrackingRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<TrackingNode>> SearchTracking(int page, int count)
    {
      using (var scope = _metrics.BeginRepositoryScope<TrackingRepository>(nameof(SearchTracking)))
      {
        var offset = (page - 1) * count;
        var results = (await _connection.QueryAsync<TrackingNode>(@"
SELECT Id,
  ChangeDate,
  UserId,
  Summary,
  Change
FROM Tracking
ORDER BY ChangeDate DESC
OFFSET @offset ROWS
FETCH NEXT @count ROWS ONLY", new {offset, count})).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }

    public async Task<IEnumerable<TrackingNode>> GetTrackingByUserIds(IEnumerable<string> userIds)
    {
      using (var scope = _metrics.BeginRepositoryScope<TrackingRepository>(nameof(GetTrackingByUserIds)))
      {
        var results = (await Task.WhenAll(userIds.Batch(2000).Select(batch => _connection.QueryAsync<TrackingNode>(@"
SELECT Id,
  ChangeDate,
  UserId,
  Summary,
  Change
FROM Tracking
WHERE UserId IN @userIds
ORDER BY ChangeDate DESC", new {userIds = batch}))))
          .SelectMany(_ => _)
          .ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
