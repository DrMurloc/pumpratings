﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class MixRepository : IMixRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public MixRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<MixNode>> GetAllMixes()
    {
      using (var scope = _metrics.BeginRepositoryScope<MixRepository>(nameof(GetAllMixes)))
      {
        var result = (await _connection.QueryAsync<MixNode>(@"
SELECT Id,
  Name,
  Image,
  DisplayOrder
FROM Mixes")).ToArray();
        scope.ResultCount = result.Length;
        return result;
      }
    }
  }
}
