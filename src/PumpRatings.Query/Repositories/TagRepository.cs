﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class TagRepository : ITagRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public TagRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<TagNode>> GetAllTags()
    {
      using (var scope = _metrics.BeginRepositoryScope<TagRepository>(nameof(GetAllTags)))
      {
        var results = (await _connection.QueryAsync<TagNode>(@"
SELECT Name,
  Color
FROM Tags")).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
