﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class SongTypeRepository : ISongTypeRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public SongTypeRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<SongTypeNode>> GetAllSongTypes()
    {
      using (var scope = _metrics.BeginRepositoryScope<SongTypeRepository>(nameof(GetAllSongTypes)))
      {
        var results = (await _connection.QueryAsync<SongTypeNode>(@"
SELECT Type AS Name
FROM SongTypes")).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
