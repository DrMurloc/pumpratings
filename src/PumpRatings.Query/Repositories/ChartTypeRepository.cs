﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class ChartTypeRepository : IChartTypeRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public ChartTypeRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<ChartTypeNode>> GetAllChartTypes()
    {
      using (var scope = _metrics.BeginRepositoryScope<ChartTypeRepository>(nameof(GetAllChartTypes)))
      {
        var results = (await _connection.QueryAsync<ChartTypeNode>(@"
SELECT Type AS Name
FROM ChartTypes")).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
