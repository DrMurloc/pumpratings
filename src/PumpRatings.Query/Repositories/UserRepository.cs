﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class UserRepository : IUserRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public UserRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<UserNode>> GetAllUsers()
    {
      using (var scope = _metrics.BeginRepositoryScope<UserRepository>(nameof(GetAllUsers)))
      {
        var results = (await _connection.QueryAsync<UserNode>(@"
SELECT Id,
  Name,
  Role AS RoleName,
  DarkTheme,
  CurrentRole AS CurrentRoleName
FROM Users")).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
