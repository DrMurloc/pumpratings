﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MoreLinq;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class CommentRepository : ICommentRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public CommentRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<CommentNode>> GetCommentsByUserIds(IEnumerable<string> userIds)
    {
      using (var scope = _metrics.BeginRepositoryScope<CommentRepository>(nameof(GetCommentsByUserIds)))
      {
        var results = (await Task.WhenAll(userIds.Batch(2000).Select(batch => _connection.QueryAsync<CommentNode>(@"
SELECT UserId,
  ChartId,
  CreatedDate,
  Message
FROM Comments
WHERE UserId IN @userIds", new {userIds = batch}))))
          .SelectMany(_=>_)
          .ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }

    public async Task<IEnumerable<CommentNode>> GetCommentsByChartIds(IEnumerable<string> chartIds)
    {
      using (var scope = _metrics.BeginRepositoryScope<CommentRepository>(nameof(GetCommentsByChartIds)))
      {
        var results = (await Task.WhenAll(chartIds.Batch(2000).Select(batch => _connection.QueryAsync<CommentNode>(@"
SELECT UserId,
  ChartId,
  CreatedDate,
  Message
FROM Comments
WHERE ChartId IN @chartIds", new {chartIds = batch}))))
          .SelectMany(_ => _)
          .ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
