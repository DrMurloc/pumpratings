﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class ListRepository : IListRepository
  {
    private readonly IMetricsService _metrics;
    private readonly IDbConnection _connection;
    public ListRepository(IMetricsService metrics,
      IDbConnection connection)
    {
      _metrics = metrics;
      _connection = connection;
    }
    public async Task<IEnumerable<ListNode>> GetListsByUserIds(IEnumerable<string> userIds)
    {
      using (var scope = _metrics.BeginRepositoryScope<ListRepository>(nameof(GetListsByUserIds)))
      {
        var results = (await _connection.QueryAsync<ListNode>(@"
SELECT Id, UserId, Name, IsFavorite IsFavorites
FROM Lists
WHERE UserId IN @userIds", new {userIds})).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
