﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MoreLinq;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class RatingRepository : IRatingRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public RatingRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<RatingNode>> GetRatingsByChartIdsAndUserId(IEnumerable<string> chartIds, string userId)
    {
      using (var scope = _metrics.BeginRepositoryScope<RatingRepository>(nameof(GetRatingsByChartIdsAndUserId)))
      {
        var results = (await Task.WhenAll(chartIds.Batch(2000).Select(batch => _connection.QueryAsync<RatingNode>(@"
SELECT ChartId,
  UserId,
  Rating
FROM Ratings
WHERE ChartId IN @chartIds
  AND UserId = @userId", new {chartIds = batch, userId}))))
          .SelectMany(_ => _)
          .ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }

    public async Task<IEnumerable<RatingNode>> GetRatingsByChartIds(IEnumerable<string> chartIds)
    {
      using (var scope = _metrics.BeginRepositoryScope<RatingRepository>(nameof(GetRatingsByChartIds)))
      {
        var results = (await Task.WhenAll(chartIds.Batch(2000).Select(batch => _connection.QueryAsync<RatingNode>(@"
SELECT ChartId,
  UserId,
  Rating
FROM Ratings
WHERE ChartId IN @chartIds", new {chartIds = batch}))))
          .SelectMany(_ => _)
          .ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }

    public async Task<IEnumerable<RatingNode>> GetRatingsByUserIds(IEnumerable<string> userIds)
    {
      using (var scope = _metrics.BeginRepositoryScope<RatingRepository>(nameof(GetRatingsByUserIds)))
      {
        var results = (await Task.WhenAll(userIds.Batch(2000).Select(batch => _connection.QueryAsync<RatingNode>(@"
SELECT ChartId,
  UserId,
  Rating
FROM Ratings
WHERE UserId IN @userIds", new {userIds = batch}))))
          .SelectMany(_ => _)
          .ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
