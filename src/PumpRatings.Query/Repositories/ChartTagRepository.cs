﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using PumpRatings.Metrics.Abstractions.Services;
using PumpRatings.Query.Abstractions.Models;
using PumpRatings.Query.Abstractions.Repositories;

namespace PumpRatings.Query.Repositories
{
  public sealed class ChartTagRepository : IChartTagRepository
  {
    private readonly IDbConnection _connection;
    private readonly IMetricsService _metrics;
    public ChartTagRepository(IDbConnection connection,
      IMetricsService metrics)
    {
      _connection = connection;
      _metrics = metrics;
    }
    public async Task<IEnumerable<ChartTagEdge>> GetAllChartTags()
    {
      using (var scope = _metrics.BeginRepositoryScope<ChartTagRepository>(nameof(GetAllChartTags)))
      {
        var results = (await _connection.QueryAsync<ChartTagEdge>(@"
SELECT ChartId,
  Tag AS TagName
FROM ChartTag")).ToArray();
        scope.ResultCount = results.Length;
        return results;
      }
    }
  }
}
