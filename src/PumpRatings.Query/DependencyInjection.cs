﻿
using GraphiQl;
using GraphQL;
using GraphQL.DataLoader;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Query.Abstractions.Caches;
using PumpRatings.Query.Abstractions.Repositories;
using PumpRatings.Query.Abstractions.Services;
using PumpRatings.Query.Caches;
using PumpRatings.Query.GraphTypes;
using PumpRatings.Query.QuerySchema;
using PumpRatings.Query.Repositories;
using PumpRatings.Query.Services;

namespace PumpRatings.Query.NetCore
{
  public static class DependencyInjection
  {
    public static IApplicationBuilder AddPumpRatingsGraphiQl(this IApplicationBuilder builder)
    {
      return builder.UseGraphiQl("/query");
    }
    public static IServiceCollection AddPumpRatingsGraphQl(this IServiceCollection serviceCollection)
    {
      return serviceCollection
        .AddDataLayer()
        .AddCaches()
        .AddServices()
        .AddGraphTypes()
        .AddSchema()
        .AddController()
        .AddEventHandlers()
        .AddSingleton<IDataLoaderContextAccessor, DataLoaderContextAccessor>()
        .AddSingleton<DataLoaderDocumentListener>(); ;
    }


    private static IServiceCollection AddEventHandlers(this IServiceCollection builder)
    {
      return builder.AddEventHandler<ClearCacheEvent, ChartCache>()
        .AddEventHandler<ClearCacheEvent, ChartTagCache>()
        .AddEventHandler<ClearCacheEvent, ChartTypeCache>()
        .AddEventHandler<ClearCacheEvent, MixCache>()
        .AddEventHandler<ClearCacheEvent, ReleaseNotesCache>()
        .AddEventHandler<ClearCacheEvent, RoleCache>()
        .AddEventHandler<ClearCacheEvent, SongCache>()
        .AddEventHandler<ClearCacheEvent, SongTypeCache>()
        .AddEventHandler<ClearCacheEvent, TagCache>()
        .AddEventHandler<ClearCacheEvent, UserCache>()
        .AddEventHandler<ChartCreatedEvent, ChartCache>()
        .AddEventHandler<MixCreatedEvent,MixCache>()
        .AddEventHandler<ReleaseNotesCreatedEvent,ReleaseNotesCache>()
        .AddEventHandler<SongCreatedEvent,SongCache>()
        .AddEventHandler<TagCreatedEvent,TagCache>()
        .AddEventHandler<ChartUpdatedEvent,ChartCache>()
        .AddEventHandler<ProfileUpdatedEvent,UserCache>()
        .AddEventHandler<RatingUpdatedEvent,ChartCache>()
        .AddEventHandler<ChartTagsUpdatedEvent,ChartTagCache>()
        .AddEventHandler<ProfileCreatedEvent,UserCache>();
    }

    private static IServiceCollection AddEventHandler<TEvent, THandler>(this IServiceCollection builder)
      where THandler : class, IEventHandler<TEvent>
    {
      return builder.AddTransient<IEventHandler<TEvent>, THandler>(services => services.GetService<THandler>());
    }
    private static IServiceCollection AddController(this IServiceCollection builder)
    {
      return builder
        .AddSingleton<IDocumentExecuter, DocumentExecuter>();
    }

    private static IServiceCollection AddSchema(this IServiceCollection builder)
    {
      return builder
        .AddSingleton<EntryPoints>()
        .AddSingleton<ISchema>(context => new PumpItUpSchema(new FuncDependencyResolver(context.GetService)));
    }

    private static IServiceCollection AddGraphTypes(this IServiceCollection builder)
    {
      return builder
        .AddSingleton<ChartGraphType>()
        .AddSingleton<ChartTypeGraphType>()
        .AddSingleton<CommentGraphType>()
        .AddSingleton<MixGraphType>()
        .AddSingleton<RatingGraphType>()
        .AddSingleton<ReleaseNotesGraphType>()
        .AddSingleton<RoleGraphType>()
        .AddSingleton<SongGraphType>()
        .AddSingleton<SongTypeGraphType>()
        .AddSingleton<TagGraphType>()
        .AddSingleton<TrackingGraphType>()
        .AddSingleton<UserGraphType>()
        .AddSingleton<LoginProviderGraphType>()
        .AddSingleton<ChartListGraphType>();
    }

    private static IServiceCollection AddServices(this IServiceCollection builder)
    {
      return builder
        .AddTransient<IChartService,ChartService>()
        .AddTransient<IChartTypeService,ChartTypeService>()
        .AddTransient<ICommentService,CommentService>()
        .AddTransient<IMixService,MixService>()
        .AddTransient<IRatingService,RatingService>()
        .AddTransient<IReleaseNotesService,ReleaseNotesService>()
        .AddTransient<IRoleService,RoleService>()
        .AddTransient<ISongService,SongService>()
        .AddTransient<ISongTypeService,SongTypeService>()
        .AddTransient<ITagService,TagService>()
        .AddTransient<ITrackingService,TrackingService>()
        .AddTransient<IUserService,UserService>()
        .AddTransient<ILoginProviderService,LoginProviderService>()
        .AddTransient<IListService,ListService>();
    }

    private static IServiceCollection AddDataLayer(this IServiceCollection builder)
    {
      return builder
        .AddTransient<IChartRepository,ChartRepository>()
        .AddTransient<IChartTagRepository,ChartTagRepository>()
        .AddTransient<IChartTypeRepository,ChartTypeRepository>()
        .AddTransient<ICommentRepository,CommentRepository>()
        .AddTransient<IMixRepository,MixRepository>()
        .AddTransient<IRatingRepository,RatingRepository>()
        .AddTransient<IReleaseNotesRepository,ReleaseNotesRepository>()
        .AddTransient<IRoleRepository,RoleRepository>()
        .AddTransient<ISongRepository,SongRepository>()
        .AddTransient<ISongTypeRepository,SongTypeRepository>()
        .AddTransient<ITagRepository,TagRepository>()
        .AddTransient<ITrackingRepository,TrackingRepository>()
        .AddTransient<IUserRepository,UserRepository>()
        .AddTransient<IListRepository,ListRepository>();
    }

    private static IServiceCollection AddCaches(this IServiceCollection builder)
    {
      return builder
        .AddCache<IChartCache,ChartCache>()
        .AddCache<IChartTagCache,ChartTagCache>()
        .AddCache<IChartTypeCache,ChartTypeCache>()
        .AddCache<IMixCache,MixCache>()
        .AddCache<IReleaseNotesCache,ReleaseNotesCache>()
        .AddCache<IRoleCache,RoleCache>()
        .AddCache<ISongCache,SongCache>()
        .AddCache<ISongTypeCache,SongTypeCache>()
        .AddCache<ITagCache,TagCache>()
        .AddCache<IUserCache,UserCache>();
    }

    private static IServiceCollection AddCache<TCacheInterface, TCache>(this IServiceCollection builder)
      where TCache : class, TCacheInterface
      where TCacheInterface : class
    {
      return builder
        .AddSingleton<TCache>()
        .AddSingleton<TCacheInterface>(services=>services.GetService<TCache>());
    }
  }
}
