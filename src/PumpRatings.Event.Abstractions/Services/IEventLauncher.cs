﻿using System.Threading.Tasks;
using PumpRatings.Abstractions.Models;
using PumpRatings.Event.Abstractions.Events;

namespace PumpRatings.Event.Abstractions.Services
{
  public interface IEventLauncher
  {
    void Launch<TEvent>(UserProfile user, TEvent inEvent);
    void Launch<TEvent>(GenericEvent<TEvent> inEvent);
  }
}
