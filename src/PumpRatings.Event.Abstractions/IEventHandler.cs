﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PumpRatings.Event.Abstractions
{
  public interface IEventHandler<in TEvent>
  {
    Task Handle(TEvent inEvent);
  }
}
