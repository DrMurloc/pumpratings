﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Updated Profile")]
  public sealed class ProfileUpdatedEvent
  {
    public string UserId { get; set; }
    public string Name { get; set; }
    public string CurrentRole { get; set; }
    public bool DarkTheme { get; set; }
  }
}
