﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Chart Removed from List")]
  public sealed class ChartRemovedFromListEvent
  {
    public string ChartId { get; set; }
    public string ListId { get; set; }
  }
}
