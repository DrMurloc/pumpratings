﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Created Song")]
  public sealed class SongCreatedEvent
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public string Artist { get; set; }
    public string Bpm { get; set; }
    public string Image { get; set; }
    public string MixId { get; set; }
    public string Type { get; set; }
  }
}
