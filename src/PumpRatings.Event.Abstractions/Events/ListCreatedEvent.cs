﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("List Created")]
  public sealed class ListCreatedEvent
  {
    public string Id { get; set; }
    public string UserId { get; set; }
    public string Name { get; set; }
    public bool IsFavorites { get; set; }
  }
}
