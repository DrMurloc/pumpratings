﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Created Comment")]
  public sealed class CommentCreatedEvent
  {
    public string UserId { get; set; }
    public string UserName { get; set; }
    public string ChartId { get; set; }
    public string Message { get; set; }
  }
}
