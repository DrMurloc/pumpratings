﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Created Release Notes")]
  public sealed class ReleaseNotesCreatedEvent
  {
    public string UserId { get; set; }
    public string UserName { get; set; }
    public string Notes { get; set; }
    public DateTimeOffset ReleaseDate { get; set; }
  }
}
