﻿using PumpRatings.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  public sealed class UserOriginatedEvent<TEvent> : GenericEvent<TEvent>
  {
    public UserProfile User { get; set; }
  }
}
