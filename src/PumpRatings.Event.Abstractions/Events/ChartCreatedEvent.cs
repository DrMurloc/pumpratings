﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Created Chart")]
  public sealed class ChartCreatedEvent
  {
    public string Id { get; set; }
    public int? Difficulty { get; set; }
    public string Type { get; set; }
    public string SongId { get; set; }
    public string Video { get; set; }
    public int Players { get; set; }
  }
}
