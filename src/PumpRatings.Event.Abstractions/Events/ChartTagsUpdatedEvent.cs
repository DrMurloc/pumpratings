﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Updated Chart Tags")]
  public sealed class ChartTagsUpdatedEvent
  {
    public string ChartId { get; set; }
    public IEnumerable<string> TagNames { get; set; }
  }
}
