﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Created Tag")]
  public sealed class TagCreatedEvent
  {
    public string Name { get; set; }
    public string Color { get; set; }
  }
}
