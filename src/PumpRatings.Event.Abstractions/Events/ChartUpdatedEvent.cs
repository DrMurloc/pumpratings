﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Updated Chart")]
  public sealed class ChartUpdatedEvent
  {
    public string ChartId { get; set; }
    public string Video { get; set; }
  }
}
