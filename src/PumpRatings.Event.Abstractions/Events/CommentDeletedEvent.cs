﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Deleted Comment")]
  public sealed class CommentDeletedEvent
  {
    public string UserId { get; set; }
    public string ChartId { get; set; }
  }
}
