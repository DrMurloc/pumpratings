﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Updated User Role")]
  public sealed class UserRoleUpdatedEvent
  {
    public string UserId { get; set; }
    public string Role { get; set; }
  }
}
