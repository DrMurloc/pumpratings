﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Chart Added to List")]
  public sealed class ChartAddedToListEvent
  {
    public string ChartId { get; set; }
    public string ListId { get; set; }
  }
}
