﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Updated Rating")]
  public sealed class RatingUpdatedEvent
  {
    public string UserId { get; set; }
    public string ChartId { get; set; }
    public double Rating { get; set; }
    public double NewAverageRating { get; set; }
  }
}
