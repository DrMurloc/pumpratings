﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  public abstract class GenericEvent
  {

  }
  public abstract class GenericEvent<TEvent> : GenericEvent
  {
    public TEvent Data { get; set; }
  }
}
