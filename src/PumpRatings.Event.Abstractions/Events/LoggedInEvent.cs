﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Logged In")]
  public sealed class LoggedInEvent
  {
    public string UserId { get; set; }
    public string Name { get; set; }
    public string Role { get; set; }
    public bool DarkTheme { get; set; }
    public string CurrentRole { get; set; }
  }
}
