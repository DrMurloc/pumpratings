﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace PumpRatings.Event.Abstractions.Events
{
  [Description("Created Mix")]
  public sealed class MixCreatedEvent
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public string Image { get; set; }
    public int DisplayOrder { get; set; }
  }
}
