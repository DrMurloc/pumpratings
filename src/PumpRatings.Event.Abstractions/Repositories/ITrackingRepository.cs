﻿using PumpRatings.Event.Abstractions.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PumpRatings.Event.Abstractions.Repositories
{
  public interface ITrackingRepository
  {
    Task TrackEvent<TEvent>(GenericEvent<TEvent> inEvent);
  }
}
