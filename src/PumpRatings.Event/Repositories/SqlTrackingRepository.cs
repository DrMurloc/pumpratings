﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Abstractions.Models;
using PumpRatings.Abstractions.Security;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Repositories;

namespace PumpRatings.Event.Repositories
{
  public sealed class SqlTrackingRepository : ITrackingRepository
  {
    private readonly ISqlConnectionFactory _connectionFactory;
    public SqlTrackingRepository(ISqlConnectionFactory connectionFactory)
    {
      _connectionFactory = connectionFactory;
    }
    public async Task TrackEvent<TEvent>(GenericEvent<TEvent> inEvent)
    {
      var eventType = typeof(TEvent);
      var description = eventType.GetCustomAttribute<DescriptionAttribute>();
      var eventName = description?.Description ?? eventType.Name;
      switch (inEvent)
      {
        case UserOriginatedEvent<TEvent> userOriginatedEvent:
          var user = userOriginatedEvent.User;
          using (var connection = await _connectionFactory.ConnectAsync())
          {
            await connection.ExecuteAsync(@"
INSERT INTO Tracking
(ChangeDate, UserId, Summary, Change)
VALUES
(@ChangeDate, @UserId, @Summary, @Change)",
              new
              {
                ChangeDate = DateTimeOffset.Now,
                UserId = user.Id,
                Summary = eventName,
                Change = JsonConvert.SerializeObject(inEvent.Data, Formatting.Indented)
              });
          }
          break;
      }
    }
  }
}
