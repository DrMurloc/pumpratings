﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PumpRatings.Abstractions.Models;
using PumpRatings.Abstractions.Security;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Repositories;

namespace PumpRatings.Event.Repositories
{
  public sealed class LoggerTrackingRepository : ITrackingRepository
  {
    private readonly ILogger<LoggerTrackingRepository> _logger;
    public LoggerTrackingRepository(ILogger<LoggerTrackingRepository> logger)
    {
      _logger = logger;
    }
    public Task TrackEvent<TEvent>(GenericEvent<TEvent> inEvent)
    {
      return Task.Run(() =>
      {
        var eventType = typeof(TEvent);
        var description = eventType.GetCustomAttribute<DescriptionAttribute>();
        var eventName = description?.Description ?? eventType.Name;
        switch (inEvent)
        {
          case UserOriginatedEvent<TEvent> userOriginatedEvent:
            var user = userOriginatedEvent.User;
            _logger.LogInformation($"{user.Name} - {eventName}", new
            {
              UserId = user.Id,
              UserName = user.Name,
              Event = inEvent.Data
            });
            break;
          default:
            _logger.LogInformation($"{eventName}", new
            {
              Event = inEvent.Data
            });
            break;

        }
      });
    }
  }
}
