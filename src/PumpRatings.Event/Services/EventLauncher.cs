﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PumpRatings.Abstractions.Infrastructure;
using PumpRatings.Abstractions.Models;
using PumpRatings.Abstractions.Security;
using PumpRatings.Event.Abstractions;
using PumpRatings.Event.Abstractions.Events;
using PumpRatings.Event.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Services;

namespace PumpRatings.Event.Services
{
  public sealed class EventLauncher : IEventLauncher
  {

    private readonly IServiceResolver _serviceResolver;
    public EventLauncher(IServiceResolver serviceResolver)
    {
      _serviceResolver = serviceResolver;
    }
    public async void Launch<TEvent>(GenericEvent<TEvent> inEvent)
    {
      var profile = _serviceResolver.GetService<IUserProfileAccessor>().UserProfile;
      var handlers = _serviceResolver.GetService<IEnumerable<IEventHandler<TEvent>>>();
      var trackingRepos = _serviceResolver.GetService<IEnumerable<ITrackingRepository>>();
      await Task.WhenAll(handlers.Select(handler => handler.Handle(inEvent.Data)));
      await Task.WhenAll(trackingRepos.Select(repo => repo.TrackEvent(inEvent)));
    }

    public void Launch<TEvent>(UserProfile user, TEvent inEvent)
    {
      Launch(new UserOriginatedEvent<TEvent>
      {
        User=user,
        Data=inEvent
      });
    }
  }
}
