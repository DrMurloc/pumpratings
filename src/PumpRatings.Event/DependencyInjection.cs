﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using PumpRatings.Event.Abstractions.Repositories;
using PumpRatings.Event.Abstractions.Services;
using PumpRatings.Event.Repositories;
using PumpRatings.Event.Services;

namespace PumpRatings.Event
{
  public static class DependencyInjection
  {
    public static IServiceCollection AddPumpRatingEvents(this IServiceCollection builder)
    {
      return builder.AddSingleton<ITrackingRepository, LoggerTrackingRepository>()
        .AddSingleton<ITrackingRepository, SqlTrackingRepository>()
        .AddSingleton<IEventLauncher,EventLauncher>();
    }
  }
}
